//
//  BlockDetailVC.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/07.
//

import UIKit


enum BlockDetailCellTypes{
	case info
	case blockNumber
	case selectRange
	
}

protocol BlockDetailVCDelegate:class {
	func updateModel()
}

class BlockDetailVC: UIViewController {

	@IBOutlet weak var tableView: UITableView!

	weak var delegate: BlockDetailVCDelegate?

	var cellArray:[BlockDetailCellTypes] = [.blockNumber, .selectRange, .info]
	var model:BlockNumberModel? = nil
//	{
//		willSet{
//			if let model = newValue{
//				self.settedBlockNumber = model.blockNumber
//				self.settedSuffixCount = model.suffixCount
//			}
//		}
//	}
	
//	var settedBlockNumber:String = ""
//	var settedSuffixCount:Int = 2
	var expectResult:String = ""
	
	@IBOutlet weak var numberPadView: NumberPadView!
	var vcTitle:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

//		self.setNavigationBar()
		self.setTableView()
		self.setData()
//		self.setCellArray()
		self.registerNotification()
		self.setNavigationBar()
		self.setUI()
		self.setNumberPad()
		self.setNavigationRightBtn()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
//		self.tabBarController?.tabBar.isHidden = true
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		Toast.makeToast(message: "방향키를 이용하여 차단 범위를 지정하세요", controller: self)
	}
	
	deinit {
		self.model = nil
		print(debug: "", self, isSimple: true)
		self.unregisterNotification()
	}
}

//MARK: - UI
extension BlockDetailVC{
	func setUI(){
		
		
	}
	
	func setNavigationRightBtn(){
//		guard let model = self.model else { return }
//		if !model.isDefault{
//			self.navigationItem.rightBarButtonItem?.isEnabled = !model.blockNumber.isEmpty
//		}
//		if let isDefault = self.model?.isDefault{
//			self.navigationItem.rightBarButtonItem?.isEnabled = !isDefault
//		}
		
	}
	
}
//MARK: - NavigationBar
extension BlockDetailVC: UIGestureRecognizerDelegate{
	func setNavigationBar(){
		
//		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
//		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
		self.navigationController?.navigationBar.prefersLargeTitles = false
		
		
		// set right button
//		let rightImage = UIImage(systemName: "checkmark")//?.withRenderingMode(.alwaysOriginal)
//		let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(image: rightImage, style:.plain, target: self, action: #selector(rightBtnTapped))
		let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(title: "저장", style:.plain, target: self, action: #selector(rightBtnTapped))
		
		self.navigationItem.rightBarButtonItem = rightBarButtonItem
		
		// title
		self.title = self.vcTitle
		
		
//		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexString: ColorInfo.TOPBAR)]
//        self.navigationController?.navigationBar.barTintColor = UIColor(ColorInfo.MAIN)
		
	}
	
	@objc func leftBtnTapped(){
		
	}
	
	@objc func rightBtnTapped(){
		self.view.endEditing(true)
		
		guard let model = self.model else { return }
		
		if let connected = model.isConnected{
			if connected{
				self.saveRealmWithCallkit(msg: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다")
				
				self.delegate?.updateModel()
			}
			else{
				CallKitManager.shared.openSettingAlert()
			}
		}
		
		
		
	}
}

//MARK: - Data
extension BlockDetailVC{
	func setData(){
		
//		self.tableView.reloadData()
	}
	
	func checkIsConnected(){
		guard let model = self.model else { return }
		if let bundle = CallKitBundles(rawValue: model.callKitBundle.rawValue){
			CallKitManager.shared.checkCallkitEnable(bundle: bundle, success: {
				self.model?.isConnected = true
			
			}, failure: { status in
				switch status{
					
					case .unknown:
						self.model?.isConnected = false
					case .disabled:
						self.model?.isConnected = false
					case .enabled:
						self.model?.isConnected = true
					@unknown default:
						self.model?.isConnected = false
				}
				
				
			})
		}
	}
	
}


//MARK: - TableView
extension BlockDetailVC: UITableViewDelegate, UITableViewDataSource{
	func setTableView(){
		/* 커스텀 버튼 딜레이제거 */
		self.tableView.delaysContentTouches = false
		for case let subview as UIScrollView in self.tableView.subviews {
			subview.delaysContentTouches = false
		}
		/* 커스텀 버튼 딜레이제거 */
		
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.separatorStyle = .none
		self.tableView.keyboardDismissMode = .onDrag
		self.tableView.tableFooterView = UIView()
		self.tableView.alwaysBounceVertical = false
		self.tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
		self.registerCell()
		
	}
	
	@objc func handleTap(sender: UITapGestureRecognizer) {
		 if sender.state == .ended {
			 // todo...
			self.view.endEditing(true)
		 }
		 sender.cancelsTouchesInView = false
	}
	
	func registerCell(){

		self.tableView.register(UINib(nibName: String.init(describing: BlockDetailPreviewCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: BlockDetailPreviewCell.self))
		
		self.tableView.register(UINib(nibName: String.init(describing: BlockDetailInputCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: BlockDetailInputCell.self))
		self.tableView.register(UINib(nibName: String.init(describing: BlockDetailSelectRangeCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: BlockDetailSelectRangeCell.self))
		
		
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.cellArray.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension//BlockListCell.cellHeight()
		
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let model = self.model else {
			return UITableViewCell()
		}
		let cell: UITableViewCell
		
		
		switch self.cellArray[indexPath.row] {
			
			case .info:
				cell = UITableViewCell()
								
			case .blockNumber:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: BlockDetailInputCell.self)) as! BlockDetailInputCell
				customCell.fullNumberLabelText = model.blockNumber
				cell = customCell
			case .selectRange:
				let customCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: BlockDetailSelectRangeCell.self)) as! BlockDetailSelectRangeCell
				customCell.preViewLabel.text = nil
				customCell.preViewLabel.attributedText = nil
				customCell.blockNumber = model.blockNumber
				customCell.currentSuffix = model.suffixCount
				customCell.delegate = self
				cell = customCell
				
			
		}
		
		cell.selectionStyle = .none
		
		return cell
	}
	
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let view = UIView()
		return view
	}
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
		
		
	}

	func reloadCell(indexPath: IndexPath){
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			if self.tableView.isCellVisible(indexPath: indexPath){
				self.tableView.reloadRows(at: [indexPath], with: .none)
			}
		}
		
	}
}

//MARK: - Didbecome
extension BlockDetailVC{
	@objc func applicationDidBecomeActive(){
		self.checkIsConnected()
	}
}


//MARK: - Notification
extension BlockDetailVC{
	func registerNotification(){
		NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive),
											   name: UIApplication.didBecomeActiveNotification,
											   object: nil)
	}
	func unregisterNotification(){
		NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
		
	}

}

//MARK: - Method
extension BlockDetailVC{
	func resetNumber(){
		guard let model = self.model else { return }
		if model.blockNumber.isEmpty{
			return
		}
		
		let confirmAction = UIAlertAction(title: TextInfo.alert_action_confirm, style: .default, handler: { (action) in
			self.model?.blockNumber = ""
			self.model?.localNumber = ""
			self.model?.middleNumber = ""
			self.model?.lastNumber = ""
			self.saveRealmWithCallkit(msg: "기존 패턴차단이 해제되었습니다")
			self.reloadTableView()
			self.delegate?.updateModel()
		})
		
		let cancelAction = UIAlertAction(title: TextInfo.alert_cancel, style: .cancel, handler: nil)
		UIAlertController.showAlert(title: title, message: "기존 차단 설정이 삭제됩니다\n이대로 진행할까요?", actions: [confirmAction, cancelAction])
	}
	
	
	func saveRealmWithCallkit(msg:String){
		guard let model = self.model else { return }
		
		let blockNumber = model.blockNumber
		let replaced = self.expectResult
		let splitArray:[String] = replaced.components(separatedBy: "-")
		var localNumber:String = ""
		var middleNumber:String = ""
		var lastNumber:String = ""
		for (idx, i) in splitArray.enumerated(){
			let item = i.replacingOccurrences(of: "X", with: "")
			if idx == 0{
				localNumber = item
			}
			if idx == 1{
				middleNumber = item
			}
			if idx == 2{
				lastNumber = item
			}
		}
		self.model?.suffixCount = replaced.filter({ $0 == "X"}).count
		self.model?.localNumber = localNumber
		self.model?.middleNumber = middleNumber
		self.model?.lastNumber = lastNumber
		RealmManager.shared.updateNumber(seq: model.seq, blockNumber: blockNumber, localNumber: localNumber, middleNumber: middleNumber, lastNumber: lastNumber, suffixCount: model.suffixCount, completion: { (success) in
			print(debug: "callkit load success", self, isSimple: true)
			if success{
				Toast.makeToast(message: msg, controller: self)
				CallKitManager.shared.saveCallkitData(value: model.expectNumber, bundle: model.callKitBundle, suffixCount: model.suffixCount, success: {
					if success{
//						self.settedBlockNumber = model.blockNumber
//						self.settedSuffixCount = model.suffixCount
						print(debug: "update number success", self, isSimple: true)

						DispatchQueue.main.async { [weak self] in
							guard let self = self else { return }
							self.reloadTableView()
						}
					}
					else{
						print(debug: "saveCallkitData failed", self, isSimple: true)
					}
					
				}, failure: {
					print(debug: "callkit load failed", self, isSimple: true)
				})
			}
			else{
//				self.model?.blockNumber = self.settedBlockNumber
//				self.model?.suffixCount = self.settedSuffixCount
				self.reloadTableView()
			}
			
		})
	}
	
	func showIsDefaultAlert(){
		let confirmAction = UIAlertAction(title: "이동하기", style: .default, handler: { (action) in
			CallKitManager.shared.moveToSetting()
		})
		let cancelAction = UIAlertAction(title: TextInfo.alert_cancel, style: .destructive, handler: nil)
		
		UIAlertController.showAlert(title: title, message: "기본제공되는 차단패턴은 범위만 수정할 수 있습니다\n차단해제는 설정을 꺼주세요", actions: [confirmAction, cancelAction])
	}
	
}


//MARK: - Number Pad dependency
extension BlockDetailVC: NumberPadViewDelegate{
	func setNumberPad(){
		self.numberPadView.delegate = self
	}
	
	func selectedNumber(value: String) {
		
		
		guard let model = self.model else { return }
		if model.isDefault{
			self.showIsDefaultAlert()
			return
		}
		
		
		var current:[String] = model.blockNumber.map { String($0) }
		
		if let eventType:NumberPadEvents = NumberPadEvents(rawValue: value){
			switch eventType {
				
				case .paste:
					//클립보드에 복사된 문자열을 가져오기
					if let theString = UIPasteboard.general.string {
						let matched = theString.matches(for: "[0-9]").joined()
						self.model?.blockNumber = matched.setPhoneNumber()
						self.reloadTableView()
					}
				case .reset:
					if let isConnected = model.isConnected{
						if isConnected{
							self.resetNumber()
						}
						else{
							CallKitManager.shared.openSettingAlert()
						}
						
					}
					
			}
		}
		else{
			if value == ""{
				// 삭제
				current = current.dropLast()
			}
			else{
				// 추가
				current.append(value)
			}
			
			let result = current.joined()
			self.model?.blockNumber = result.setPhoneNumber()
		
			self.reloadTableView()
		}
		
		
	}
}

extension BlockDetailVC: BlockDetailSelectRangeCellDelegate{
	func expectResult(value: String) {
		
		self.expectResult = value
	}
}

extension BlockDetailVC{
	func reloadTableView(){
		self.tableView.reloadData()
		self.setNavigationRightBtn()
	}
}
