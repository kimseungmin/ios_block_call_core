//
//  NumberPadView.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/11.
//

import Foundation
import UIKit


enum NumberPadEvents:String{
	case paste = "paste"
	case reset = "reset"
}


protocol NumberPadViewDelegate:class {
	func selectedNumber(value:String)
}

class NumberButtonModel: NSObject{
    var number:String = ""
    var tag:Int       = 0
	
	init(number:String, tag:Int) {
        self.number = number
        self.tag    = tag
		
	}
}

class NumberPadView:UIView{
	@IBOutlet weak var contentView: UIView!
	
	
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var numberBtn1: UIButton!
	@IBOutlet weak var numberBtn2: UIButton!
	@IBOutlet weak var numberBtn3: UIButton!
	@IBOutlet weak var numberBtn4: UIButton!
	@IBOutlet weak var numberBtn5: UIButton!
	@IBOutlet weak var numberBtn6: UIButton!
	@IBOutlet weak var numberBtn7: UIButton!
	@IBOutlet weak var numberBtn8: UIButton!
	@IBOutlet weak var numberBtn9: UIButton!
	@IBOutlet weak var numberBtn0: UIButton!
	
	var numberBtnArray:[NumberButtonModel] = []
	
	@IBOutlet weak var resetBtn: UIButton!
	var isConnected:Bool = true{
		willSet{
			self.resetBtn.isEnabled = newValue
		}
		
	}
	
	weak var delegate: NumberPadViewDelegate?
	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.commonInit()
	}
	
	private func commonInit(){
		
		let view = UINib.loadNib(self)
		view.frame = self.bounds
		self.addSubview(view)
		view.addSubview(contentView)
		
		contentView.frame = self.bounds
		contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			self.setUI()
		}
		
	}
	private func setButtonArray(){
		let titleArr = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "초기화", "0", ""]
		for i in titleArr{
			if let num = Int(i){
				self.numberBtnArray.append(NumberButtonModel.init(number: i, tag: num))
			}
			else{
				if i == titleArr.last{
					self.numberBtnArray.append(NumberButtonModel.init(number: i, tag: -1))
				}
				else{
					self.numberBtnArray.append(NumberButtonModel.init(number: i, tag: 1000))
				}
				
			}
			
		}
		self.collectionView.reloadData()
		
//		self.numberBtnArray = [self.numberBtn1, self.numberBtn2, self.numberBtn3, self.numberBtn4, self.numberBtn5, self.numberBtn6, self.numberBtn7, self.numberBtn8, self.numberBtn9, self.numberBtn0]
	}
	
	private func setUI(){
		
		self.registerCell()
		self.setButtonArray()
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
		self.collectionView.backgroundColor = .clear
		self.collectionView.alwaysBounceVertical = false
		self.collectionView.alwaysBounceHorizontal = false
		self.collectionView.isScrollEnabled = false
		
	}
	
}

extension NumberPadView: UICollectionViewDelegate, UICollectionViewDataSource {
	
	// register Cell
	private func registerCell() {
		
		self.collectionView.register(UINib.init(nibName: String.init(describing: NumberPadViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: NumberPadViewCell.self))
	}
	
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 1
	}
	
	// item
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		return self.numberBtnArray.count
	}
	
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: NumberPadViewCell.self), for: indexPath) as! NumberPadViewCell
		let row = self.numberBtnArray[indexPath.row]
		
		cell.numberBtn.setTitle(row.number, for: .normal)
		cell.numberBtn.tag = row.tag
		
		
		
		if row.number.count == 1{
			// 숫자
			cell.numberBtn.titleLabel?.font = UIFont.systemFont(ofSize: 30)
		}
		else{
			// 초기화
			cell.numberBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
			
		}
		
		if row.tag == -1{
			cell.numberBtn.setImage(UIImage.init(systemName: "delete.left.fill"), for: .normal)
		}
		
		
		
		cell.delegate = self
		
		return cell
	}
	
	
}

extension NumberPadView: UICollectionViewDelegateFlowLayout{
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

		let leftRightPadding = self.frame.width * 0.13
		let interSpacing = self.frame.width * 0.1

		let cellWidth = (self.frame.width - 2 * leftRightPadding - 2 * interSpacing) / 3

		return .init(width: cellWidth, height: cellWidth)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 16
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

		// some basic math/geometry

		let leftRightPadding = self.frame.width * 0.15
//        let interSpacing = view.frame.width * 0.1

//        let cellWidth =.....


		return .init(top: 16, left: leftRightPadding, bottom: 16, right: leftRightPadding)
	}
	
		
	
}
extension NumberPadView: NumberPadViewCellDelegate {
	func numberTapped(tag: Int) {
		switch tag {
			case 0...9:
				self.delegate?.selectedNumber(value: "\(tag)")
				break
			case -1:
				self.delegate?.selectedNumber(value: "")
				break
				
			case 1000:
				// 초기화
				self.delegate?.selectedNumber(value: NumberPadEvents.reset.rawValue)
				break
				
			default:
				break
		}
	}
}
extension NumberPadView {
	
	@IBAction func copyBtnTapped(sender: UIButton){
		self.delegate?.selectedNumber(value: NumberPadEvents.paste.rawValue)
	}
	

}
