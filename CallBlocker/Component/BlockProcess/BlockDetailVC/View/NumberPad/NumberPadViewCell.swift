//
//  NumberPadViewCell.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/12.
//

import UIKit

protocol NumberPadViewCellDelegate: AnyObject {
	func numberTapped(tag:Int)
}

class NumberPadViewCell: UICollectionViewCell {

	@IBOutlet weak var numberBtn: UIButton!
	
	weak var delegate:NumberPadViewCellDelegate?
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		

    }
	
	override func draw(_ rect: CGRect) {
		self.numberBtn.toCornerRound(radius: self.frame.height / 2)
		self.numberBtn.setTitleColor(UIColor.setColorSet(.textColor), for: .normal)
		self.numberBtn.tintColor = UIColor.setColorSet(.textColor)
		self.numberBtn.setBackgroundColor(UIColor.setColorSet(.numberPadBgColor), for: .normal)
		
		if self.numberBtn.titleLabel?.text?.count ?? 1 > 1{
			self.numberBtn.setTitleColor(UIColor.setColorSet(.tabbarTintColor), for: .normal)
		}
	}

}

extension NumberPadViewCell{
	@IBAction func btnTapped(sender: UIButton){
		self.delegate?.numberTapped(tag: self.numberBtn.tag)
	}
}
