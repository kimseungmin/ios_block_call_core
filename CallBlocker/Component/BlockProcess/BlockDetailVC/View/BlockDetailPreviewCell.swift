//
//  BlockDetailPreviewCell.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/09.
//

import UIKit

class BlockDetailPreviewCell: UITableViewCell {

	@IBOutlet weak var rangeLabel: UILabel!
	@IBOutlet weak var expectRangeLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
