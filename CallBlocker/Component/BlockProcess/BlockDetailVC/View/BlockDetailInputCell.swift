//
//  BlockDetailInputCell.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/11.
//

import UIKit

class BlockDetailInputCell: UITableViewCell {

//	let localNumberArray:[String] = ["02", "031", "032", "033", "041", "042", "043", "044", "051", "052", "054", "053", "054", "055", "061", "062", "063", "064", "070", "080", "050"]
	

	@IBOutlet weak var fullNumberLabel: UILabel!
	
	@IBOutlet weak var descriptionLabel: UILabel!
	var fullNumberLabelText:String = ""{
		willSet{
//			self.descriptionLabel.isHidden = (newValue == "")
			if newValue == ""{
				self.fullNumberLabel.font = UIFont.systemFont(ofSize: 25.0, weight: .medium)
				self.fullNumberLabel.textColor = .lightGray
				self.fullNumberLabel.text = "차단할 번호를 끝까지 입력하세요\n예시 : 070-7079-5443"
				self.descriptionLabel.text = "(최근 기록에서 붙여넣을 수 있습니다)"
			}
			else{
				self.fullNumberLabel.font = UIFont.systemFont(ofSize: 40.0, weight: .medium)
				self.fullNumberLabel.textColor = UIColor.setColorSet(.navigationTitleColor)
				self.fullNumberLabel.text = newValue
				self.descriptionLabel.text = "(차단 번호)"
			}
			self.fullNumberLabel.sizeToFit()
			self.layoutIfNeeded()
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
		self.fullNumberLabel.textAlignment = .center
		self.descriptionLabel.font = UIFont.systemFont(ofSize: 13.0)
		self.fullNumberLabel.textColor = .lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


