//
//  BlockDetailSelectRangeCell.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/11.
//

import UIKit


protocol BlockDetailSelectRangeCellDelegate:class{
	func expectResult(value:String)
}
class BlockDetailSelectRangeCell: UITableViewCell {

	@IBOutlet weak var preViewLabel: UILabel!
	
	@IBOutlet weak var increaseBtn: UIButton!
	@IBOutlet weak var decreaseBtn: UIButton!
	
	@IBOutlet weak var rangeLabel: UILabel!
	
	
	weak var delegate:BlockDetailSelectRangeCellDelegate?
	var blockNumber:String = ""
	var currentSuffix:Int = 2{
		willSet{
			self.setBtnEnable(newValue: newValue)
			var newSuffix = newValue
			if (newSuffix < self.minSuffix) || newSuffix > self.maxSuffix{
				newSuffix = self.minSuffix
				self.currentSuffix = self.minSuffix
			}
			let current = self.blockNumber.replacingOccurrences(of: "-", with: "")
//			print(debug: current, self, isSimple: false)
			var currentArr:[String] = current.map { String($0) }.reduce([],{ [$1] + $0 })
//			print(debug: currentArr, self, isSimple: false)
			for (idx, _ ) in currentArr.enumerated(){
				if idx <= (newSuffix - 1){
					currentArr[idx] = "X"
				}
			}
			
			let result = currentArr.reduce([],{ [$1] + $0 }).joined()
//			print(debug: result, self, isSimple: false)
			
			self.fullNumberLabelText = result.setPhoneNumber()
			self.delegate?.expectResult(value: result.setPhoneNumber())
		}
	}
	
	
	let minSuffix:Int = 2
	let maxSuffix:Int = 6
	
	var fullNumberLabelText:String = ""{
		willSet{
			
			
			if newValue == ""{
				
				self.preViewLabel.text = newValue
				self.rangeLabel.text = "차단범위가 없습니다"
			}
			else{
				let start = newValue.replacingOccurrences(of: "X", with: "0")
				let end = newValue.replacingOccurrences(of: "X", with: "9")
				
				
				if let i = newValue.firstIndex(of: "X") {
					self.preViewLabel.attributedText = nil
					let index: Int = newValue.distance(from: newValue.startIndex, to: i)
					let first = String(newValue.prefix(index))
					let second = String(newValue.suffix(newValue.count - index))
					
					let attributes1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25.0, weight: .medium),
									   NSAttributedString.Key.foregroundColor: UIColor.setColorSet(.tabbarTintColor),
									   NSAttributedString.Key.backgroundColor: UIColor.clear]
					
					
					let attributes2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25.0, weight: .light),
									   NSAttributedString.Key.backgroundColor: UIColor.setColorSet(.tabbarTintColor).withAlphaComponent(0.3)]
					
					
					let prefixText = NSAttributedString(string: first, attributes: attributes1)
					let suffixText = NSAttributedString(string: second, attributes: attributes2)
					let combination = NSMutableAttributedString()
					combination.append(prefixText)
					combination.append(suffixText)
					self.preViewLabel.attributedText = combination
					
					
				}
//				self.preViewLabel.text = newValue
				let startAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0, weight: .light),
									   NSAttributedString.Key.foregroundColor: UIColor.gray]
				let descriptionText = NSAttributedString(string: "차단 범위\n", attributes: startAttributes)
				
				
				let endAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0, weight: .light)]
				let endText = NSAttributedString(string: start + "~" + end, attributes: endAttributes)
				
				
				let combination = NSMutableAttributedString()
				combination.append(descriptionText)
				combination.append(endText)
				self.rangeLabel.attributedText = combination
			}
			self.layoutIfNeeded()
			
		}
	}
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		

		self.currentSuffix = 2
		
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
	override func draw(_ rect: CGRect) {
		
		
		self.increaseBtn.tintColor = UIColor.setColorSet(.textColor)
	
		self.decreaseBtn.tintColor = UIColor.setColorSet(.textColor)
	}
	
	func setBtnEnable(newValue: Int){
		self.increaseBtn.isEnabled = !(newValue <= self.minSuffix)
		self.decreaseBtn.isEnabled = !(newValue >= self.maxSuffix)
	}
}

extension BlockDetailSelectRangeCell{
	@IBAction func increaseBtnTapped(sender: UIButton){
		let value = self.currentSuffix + 1
		self.setBtnEnable(newValue: value)
		if value < self.minSuffix || value > self.maxSuffix {
			return
		}
		
		self.currentSuffix += 1
		
		
	}
	
	@IBAction func decreaseBtnTapped(sender: UIButton){
		let value = self.currentSuffix - 1
		self.setBtnEnable(newValue: value)
		if value < self.minSuffix || value > self.maxSuffix {
			return
		}
		self.currentSuffix -= 1
	}
}
