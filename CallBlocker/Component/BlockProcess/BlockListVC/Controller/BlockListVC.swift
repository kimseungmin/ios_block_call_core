//
//  BlockListVC.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/07.
//

import UIKit

class BlockListVC: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	
	@IBOutlet weak var searchBarView: SearchBarView!
	var models:[BlockNumberModel] = []
	var currentModels:[BlockNumberModel] = []
	override func viewDidLoad() {
        super.viewDidLoad()

		self.setNavigationBar()
		self.setTableView()
		self.setSearchView()
		self.registerNotification()
		self.setData()
		
		IAPManager.shared.verifyRecipt(withProgress: false)
//		RealmManager.shared.deleteAllNumbers(completion: { (success) in
			
//		})
		
		
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
//		self.registerNotification()
		
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		
		if let tabbarController = self.tabBarController{
			if tabbarController.tabBar.isHidden == true{
				UIView.transition(with: tabbarController.tabBar, duration:0.3, options: .transitionCrossDissolve, animations: {
					
					tabbarController.tabBar.isHidden = false
				}, completion: nil)
			}
			
		}
		
		
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
//		self.unregisterNotification()
		if let tabbarController = self.tabBarController{
			if tabbarController.selectedIndex == 0{
				UIView.transition(with: tabbarController.tabBar, duration:0.1, options: .transitionCrossDissolve, animations: {
					tabbarController.tabBar.isHidden = true
				}, completion: nil)
			}
			
		}

	}
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)

		if let tabbarController = self.tabBarController{
			if tabbarController.selectedIndex != 0{
				UIView.transition(with: tabbarController.tabBar, duration:0.0, options: .transitionCrossDissolve, animations: {
					tabbarController.tabBar.isHidden = false
				}, completion: nil)
			}
			
		}
	}
	
	
	deinit {
		print(debug: "", self, isSimple: true)
		self.unregisterNotification()
	}
}

//MARK: - UI
extension BlockListVC{
	func setUI(){
		
	}
	
	func reloadCell(indexPath: IndexPath){
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			if self.tableView.isCellVisible(indexPath: indexPath){
				self.tableView.reloadRows(at: [indexPath], with: .none)
			}
		}
	}
	
}

//MARK: - NavigationBar
extension BlockListVC{
	func setNavigationBar(){
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.barTintColor = UIColor.setColorSet(.navigationBgColor)
		self.navigationController?.navigationBar.prefersLargeTitles = false
		self.navigationController?.navigationBar.tintColor = UIColor.setColorSet(.navigationTitleColor)
		
//        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(ColorInfo.MAIN) as Any]
		
		// 네비 하단바 삭제
//		self.navigationController?.navigationBar.shadowImage = UIImage()
		
		// set left button

		let leftImage = UIImage(systemName: "arrow.clockwise")//?.withRenderingMode(.alwaysOriginal)
		let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(image: leftImage, style:.plain, target: self, action: #selector(leftBtnTapped))
		self.navigationItem.leftBarButtonItem = leftBarButtonItem
		
		// set back button
		let backBarButtonItem = UIBarButtonItem(title: "뒤로", style: .plain, target: nil, action: nil)
		self.navigationItem.backBarButtonItem = backBarButtonItem

		// set right button
		let rightImage = UIImage(systemName: "gift.circle")//?.withRenderingMode(.alwaysOriginal)
		let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(image: rightImage, style:.plain, target: self, action: #selector(rightBtnTapped))
		
		self.navigationItem.rightBarButtonItem = rightBarButtonItem
		
		// title
		self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.setColorSet(.navigationTitleColor)]

		

//		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexString: ColorInfo.TOPBAR)]
//        self.navigationController?.navigationBar.barTintColor = UIColor(ColorInfo.MAIN)
		
	}
	
	@objc func leftBtnTapped(){
		let confirmAction = UIAlertAction(title: "시작", style: .destructive, handler: { (action) in
			for i in self.models{
				guard let connected = i.isConnected else { return }
				guard let isAct = i.isAct else { return }
				if connected && isAct{
					let blockNumber = i.expectNumber
					
					CallKitManager.shared.saveCallkitData(value: blockNumber, bundle: i.callKitBundle, suffixCount: i.suffixCount, success: {
						
					}, failure: {
						
					})
				}
				
				
			}
		})
		
		let cancelAction = UIAlertAction(title: TextInfo.alert_cancel, style: .cancel, handler: nil)
		
		UIAlertController.showAlert(title: "전체 동기화", message: "설정 되어있는 모든 패턴이 차단 시도 됩니다\n완료 시 푸시 알림이 발송됩니다", actions: [confirmAction, cancelAction])
		
	}
	
	@objc func rightBtnTapped(){
		RouterManager.shared.dispatcher(.inAppPurchase)
	}
}

//MARK: - Data
extension BlockListVC{
	func setData(){
		var numbers = RealmManager.shared.getAllNumbers()
		if numbers.isEmpty{
			RealmManager.shared.createNumbers()
			numbers = RealmManager.shared.getAllNumbers()
		}
		
		self.models = numbers
		self.currentModels = numbers
		
		self.checkAllConnected()
		
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
//			self.tableView.reloadData()
			self.tableView.reloadSections([0], with: .fade)
		}
	}
	
	func checkAllConnected(){
		var syncCount = 0
		for i in self.models{
			if let bundle = CallKitBundles(rawValue: i.callKitBundle.rawValue){
				CallKitManager.shared.checkCallkitEnable(bundle: bundle, success: {
					syncCount += 1
					i.isConnected = true
					if let connected = i.isConnected{
						let isLoaded = UserInfoManager.shared.getIsCallkitSetted()
						if connected && !isLoaded{
							let okAction = UIAlertAction(title: TextInfo.alert_action_confirm, style: .default, handler: { (action) in
								self.leftBtnTapped()
							})
							
							UIAlertController.showAlert(title: TextInfo.alert_title, message: "설정 변경이 감지되었습니다", actions: [okAction])
							
							UserInfoManager.shared.setIsCallkitSetted(status: true)
						}
					}
					
					DispatchQueue.main.async { [weak self] in
						guard let self = self else { return }
						if let firstIndex = self.models.firstIndex(where: { $0.callKitBundle == bundle }){
							let indexPath:IndexPath = IndexPath(row: firstIndex, section: 0)
							self.reloadCell(indexPath: indexPath)
						}
						
						let models = (self.models.filter({ $0.isConnected != nil }).count == self.models.count)
						if models && self.models.filter({ $0.isConnected == true }).isEmpty{
							if let _ = self.presentedViewController as? UIAlertController{
								return
							}
							CallKitManager.shared.openSettingAlert()
						}
						
							
					}
					
					
				}, failure: { status in
					syncCount += 1
					switch status{
						
						case .unknown:
							i.isConnected = false
						case .disabled:
							i.isConnected = false
						case .enabled:
							i.isConnected = true
						@unknown default:
							i.isConnected = false
					}
					DispatchQueue.main.async { [weak self] in
						guard let self = self else { return }
						if let firstIndex = self.models.firstIndex(where: { $0.callKitBundle == bundle }){
							let indexPath:IndexPath = IndexPath(row: firstIndex, section: 0)
							self.reloadCell(indexPath: indexPath)
							
						}
						let models = (self.models.filter({ $0.isConnected != nil }).count == self.models.count)
						if models && self.models.filter({ $0.isConnected == true }).isEmpty{
							if let _ = self.presentedViewController as? UIAlertController{
								return
							}
							CallKitManager.shared.openSettingAlert()
						}
					}

				})
			}
			else{
				
				print(debug: "고장난 애다", self, isSimple: false)
				
			}
			
		}
	}
	
}


//MARK: - TableView
extension BlockListVC: UITableViewDelegate, UITableViewDataSource{
	func setTableView(){
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.keyboardDismissMode = .onDrag
		self.registerCell()
//		self.tableView.backgroundColor = UIColor.setColorSet(.navigationBgColor)
//		self.tableView.estimatedSectionHeaderHeight = 50
//		self.tableView.sectionHeaderHeight = UITableView.automaticDimension
	}
	
	func registerCell(){
		self.tableView.register(UINib(nibName: String.init(describing: BlockListCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: BlockListCell.self))
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 0
	}
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.models.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension//BlockListCell.cellHeight()
		
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: BlockListCell.self)) as! BlockListCell
		let row = self.models[indexPath.row]
		
		
		var numberText = ""
		var rangeText = "-"
		var countText = "-"
		// Call kit 연동 여부
		
		numberText = row.localNumber + row.middleNumber + row.lastNumber
		
		let prefixString = String(repeating: "X", count: row.suffixCount)
		
		numberText.append(prefixString)
		numberText = numberText.setPhoneNumber()
		
		if row.blockNumber.count == 0{
			numberText = TextInfo.text_need_number
			cell.numberLabel.text = TextInfo.text_need_number
			cell.numberLabel.textColor = UIColor.systemRed
		}
		else{
			cell.numberLabel.textColor = UIColor.systemGreen
		}
		
		if let isAct = row.isAct{
			if isAct{
				cell.numberLabel.textColor = UIColor.systemGreen
			}
			else{
				countText = "미 구매 상품"
				cell.numberLabel.textColor = UIColor.systemRed
			}
		}
		else{
			countText = "확인 중"
		}
	
		
		if let isConnected = row.isConnected{
			if isConnected{
				
				
				let firstRange = numberText.replacingOccurrences(of: "X", with: "0")
				let secondRange = numberText.replacingOccurrences(of: "X", with: "9")
				
				if let isAct = row.isAct{
					if isAct{
						cell.numberLabel.textColor = UIColor.systemGreen
						
						let prefixStringValue = prefixString.replacingOccurrences(of: "X", with: "9")
						
						var expectCount = 0
						if let countInt = Int(prefixStringValue), !row.blockNumber.isEmpty{
							expectCount = (countInt + 1)
							
							if let countString = expectCount.decimalComma{
								countText = countString + "건"
								rangeText = firstRange + " ~ " + secondRange
							}
							else{
								countText = "확인 실패"
							}
							
						}
					}
				}
				
				
			}
			else{
				
				countText = "설정이 꺼져있습니다"
				cell.numberLabel.textColor = UIColor.systemRed
				
			}
		}
		else{
			
			countText = "설정을 확인중입니다"
			cell.numberLabel.textColor = UIColor.systemGray
			

		}
		
		cell.numberLabel.text = numberText
		cell.rangeLabel.text = rangeText
		cell.countLabel.text = countText
	
			
		if row.isDefault{
			cell.typeLabel.text = "기본 070 " + "\(indexPath.row + 1)"
		}
		else{
			let count = self.models.filter({$0.isDefault == true}).count
			cell.typeLabel.text = "사용자 지정 " + "\((indexPath.row + 1) - count)"
		}
		
		return cell
	}
	
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let view = UIView()
		
		return view
	}
	
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let row = self.models[indexPath.row]
		
		var vcTitle = ""
		if row.isDefault{
			vcTitle = "기본 070"
		}
		else{
			let count = self.models.filter({$0.isDefault == true}).count
			vcTitle = "사용자 지정 " + "\((indexPath.row + 1) - count)"
		}
		
		if let isConnected = row.isConnected{
			if isConnected{
				
				if let isAct = row.isAct{
					if isAct{
						let vc = BlockDetailVC()
						vc.delegate = self
						vc.model = row.copy() as? BlockNumberModel
						
						let prefixString = String(repeating: "X", count: row.suffixCount)

						let result = row.localNumber + row.middleNumber + row.lastNumber + prefixString
						vc.expectResult = result.setPhoneNumber()

						vc.vcTitle = vcTitle
	
						
						self.navigationController?.pushViewController(vc, animated: true)
					}
					else{
						let confirmAction = UIAlertAction(title: "구매", style: .default, handler: { (action) in
							RouterManager.shared.dispatcher(.inAppPurchase)
						})

						let cancelAction = UIAlertAction(title: TextInfo.alert_cancel, style: .cancel, handler: nil)

						UIAlertController.showAlert(title: TextInfo.alert_title, message: "상품 구매가 필요합니다\n상품 복원은 더보기에서 확인할 수 있습니다", actions: [confirmAction, cancelAction])
					}
				}
				else{
					Toast.makeToast(message: IAPTextInfo.msg_checking_purchase, controller: self)
				}

				
			}
			else{
				CallKitManager.shared.openSettingAlert()
			}
		}
		else{
			UIAlertController.showMessage("설정을 확인 중입니다\n잠시만 기다려주세요")
		}
		
		
		if let index = self.tableView.indexPathForSelectedRow{
			self.tableView.deselectRow(at: index, animated: false)
		}
		
	}

}

//MARK: - Didbecome
extension BlockListVC{
	@objc func applicationDidBecomeActive(){
		self.checkAllConnected()
	}
}


//MARK: - Notification
extension BlockListVC{
	func registerNotification(){
		NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive),
											   name: UIApplication.didBecomeActiveNotification,
											   object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(resetData), name: Notification.Name.refreshInAppPurchase, object: nil)
		
	}
	
	func unregisterNotification(){
		NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
		
	}
	
	@objc func resetData(){
		
		self.setData()
		for i in self.models{
			if let bundle = CallKitBundles(rawValue: i.callKitBundle.rawValue){
				CallKitManager.shared.checkCallkitEnable(bundle: bundle, success: {
					guard let connected = i.isConnected else { return }
					guard let isAct = i.isAct else { return }
					
					if !isAct && connected{
						CallKitManager.shared.saveCallkitData(value: "", bundle: i.callKitBundle, suffixCount: 1, success: {
							
						}, failure: {
							
						})
					}
					
					DispatchQueue.main.async { [weak self] in
						guard let self = self else { return }
						if let firstIndex = self.models.firstIndex(where: { $0.callKitBundle == bundle }){
							let indexPath:IndexPath = IndexPath(row: firstIndex, section: 0)
							self.reloadCell(indexPath: indexPath)
						}
							
					}
					
				}, failure: { status in
//					print(debug: "not yet", self, isSimple: true)
				})
			}
			else{
				
				print(debug: "고장난 애다", self, isSimple: true)
				
			}
			
			
		}
		
		
	}

}


//MARK: - BlockDetailVCDelegate
extension BlockListVC: BlockDetailVCDelegate{
	func updateModel() {
		self.setData()
	}
}

//MARK: - SearchBarViewDelegate
extension BlockListVC: SearchBarViewDelegate{
	func isSearchMode(status: Bool) {
		var title = ""
		if status{
			title = "닫기"
		}
		else{
			title = "조회"
		}
		self.searchBarView.cancelSearchBtn.setTitle(title, for: .normal)
//		print(debug: status, self, isSimple: false)
	}
	
	func setSearchView(){
		self.searchBarView.backgroundColor = .clear
		self.searchBarView.delegate = self
		self.searchBarView.isSearchMode = false
		self.searchBarView.textField.keyboardType = .decimalPad
		self.searchBarView.textField.placeholder = "검색할 번호를 입력하세요"
		self.searchBarView.cancelSearchBtn.setTitle("조회", for: .normal)
	}
	
	func textFieldChanged(text: String) {
		searchData(searchText: text)
	}
	
	/* 검색 로직 */
	func searchData(searchText: String) {
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			self.models = self.currentModels
			if searchText == ""{
				self.models = self.currentModels
				
			}else{
				self.models = self.models.filter({$0.blockNumber.replacingOccurrences(of: "-", with: "").contains(find: searchText)})
			}
			
			self.tableView.reloadData()
		}
		

	}
	/* 검색 로직 */
}
