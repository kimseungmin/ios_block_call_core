//
//  BlockListCell.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/07.
//

import UIKit

class BlockListCell: UITableViewCell {

	@IBOutlet weak var typeLabel: UILabel!
	@IBOutlet weak var numberLabel: UILabel!
	@IBOutlet weak var rangeLabel: UILabel!
	@IBOutlet weak var countLabel: UILabel!
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.typeLabel.font = UIFont.boldSystemFont(ofSize: self.typeLabel.font.pointSize)
//		self.backgroundColor = UIColor.setColorSet(.navigationBgColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//extension BlockListCell{
//	class func cellHeight() -> CGFloat{
//		return 120
//	}
//}

