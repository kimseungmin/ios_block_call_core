//
//  SearchBarView.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/06.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
import UIKit


/**
need set class to file's owner nib
UIView => File's Owner xib
headerView => View xib
*/
protocol SearchBarViewDelegate:class {
	func textFieldChanged(text:String)
	func isSearchMode(status:Bool)
	
}

class SearchBarView: UIView {

	
	
	@IBOutlet weak var contentView: UIView!
	@IBOutlet weak var cancelSearchBtn: UIButton!
	@IBOutlet weak var textField: UITextField!
	@IBOutlet weak var searchBGView: UIView!
	weak var delegate: SearchBarViewDelegate?
	    
	var isSearchMode:Bool = false{
		willSet{
			self.delegate?.isSearchMode(status: newValue)

		}
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.commonInit()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.commonInit()
	}
	
	override func draw(_ rect: CGRect) {
		self.cancelSearchBtn.setTitleColor(UIColor.setColorSet(.navigationTitleColor), for: .normal)
		self.searchBGView.backgroundColor = UIColor.setColorSet(.navigationBgColor)
	}
	private func commonInit(){
		let view = UINib.loadNib(self)
		view.frame = self.bounds
		self.addSubview(view)
		view.backgroundColor = .clear
		view.addSubview(contentView)
		
		contentView.frame = self.bounds
		contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//		contentView.backgroundColor = .white
		
		DispatchQueue.main.async { [weak self] in
			self?.setUI()
		}
	}
	
	
	func setUI() {
		
		self.searchBGView.toCornerRound(radius: 8.0)
        self.textField.delegate = self
        self.isSearchMode       = false
		
    }
	
	

}

extension SearchBarView: UITextFieldDelegate{
	@IBAction func textFieldEditingChanged(_ textField: UITextField) {
		self.delegate?.textFieldChanged(text: self.textField.text ?? "")
	}

	func textFieldDidBeginEditing(_ textField: UITextField) {
		self.isSearchMode = true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
//		self.isSearchMode = false
	}
}

//MARK: - Button
extension SearchBarView{
	@IBAction func cancelBtnTapped(sender: UIButton){
		if self.isSearchMode{
			self.endEditing(true)
			self.textField.text = ""
			self.isSearchMode = false
			self.delegate?.textFieldChanged(text: self.textField.text ?? "")
		}
		else{
			self.isSearchMode = true
			self.textField.becomeFirstResponder()
		}
		
	}
}
