//
//  BlockNumberModel.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/07.
//

import Foundation


class BlockNumberModel: NSObject, NSCopying {
    var seq: Int                     = 0
    var blockNumber:String           = ""
    var localNumber:String           = ""
    var middleNumber:String          = ""
    var lastNumber:String            = ""
    var callKitBundle:CallKitBundles = .bundle1
    var isConnected:Bool?            = nil
    var isAct:Bool?                  = nil
    var isDefault:Bool               = false
    var suffixCount:Int				 = 2
	
	
	init(seq:Int,blockNumber:String , localNumber:String, middleNumber:String, lastNumber:String, callKitBundle:CallKitBundles, suffixCount:Int) {
		
		self.suffixCount   = suffixCount
        self.seq           = seq
        self.blockNumber   = blockNumber
        self.localNumber   = localNumber
        self.middleNumber  = middleNumber
        self.lastNumber    = lastNumber
        self.callKitBundle = callKitBundle
		
	}
	
//	var suffixCount:Int {
//		let blockNumberCount = self.blockNumber.filter({$0 != "X"}).filter({ $0 != "-"}).count
//		var suffixCount = blockNumberCount - localNumber.count - middleNumber.count - lastNumber.count
//		if suffixCount <= 0{
//			suffixCount = 1
//		}
//
//
//		return suffixCount
//	}
	
	var expectNumber:String {
		let blockNumberCount = self.blockNumber.filter({$0 != "X"}).filter({ $0 != "-"}).count
		var suffixCount = blockNumberCount - localNumber.count - middleNumber.count - lastNumber.count
		if suffixCount <= 0{
			suffixCount = 1
		}
		
		let number = localNumber + middleNumber + lastNumber
		return number
	}
	
	func copy(with zone: NSZone? = nil) -> Any {
		let copy = BlockNumberModel(seq: seq, blockNumber: blockNumber, localNumber: localNumber, middleNumber: middleNumber, lastNumber: lastNumber, callKitBundle: callKitBundle, suffixCount: suffixCount)
        copy.isConnected = isConnected
        copy.isAct       = isAct
        copy.isDefault   = isDefault
        copy.suffixCount = suffixCount
		
		return copy
	}
	
}
