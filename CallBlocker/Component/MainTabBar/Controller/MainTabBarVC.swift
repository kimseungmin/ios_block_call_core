//
//  MainTabBarVC.swift
//  twoNumSender
//
//  Created by seungmin kim on 07/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import SVProgressHUD

class MainTabBarVC: UITabBarController {

    var newLoad:Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
		self.view.backgroundColor = .clear
        self.drawTabBar()
		self.setNavigationBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
			self.applicationDidBecomeActive()

		}


		UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge], completionHandler: {
			didAllow, Error in
			if let error = Error{
				// Error 처리
				print(debug: error.localizedDescription, self, isSimple: false)
			}
			else{
				if didAllow{
					// 푸시 설정 완료
				}
				else{
					// 차단 완료 여부 확인을 위해 푸시 알림설정을 허용 해 주세요
				}
			}
		})
    }
	
	
    deinit {
        print(debug: "", self)
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//		super.viewDidAppear(animated)
//        
//    }
    

    @objc func applicationDidBecomeActive(){
        // 화면 복귀 시 재 체크
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
        }
    }

    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        let indexOfTab = tabBar.items?.firstIndex(of: item)
//        let userContacts = UserDefaults.standard.string(forKey: .userContacts)
//        if indexOfTab == 0{
//            self.title = userContacts
//
//        }else{
//            self.title = item.title
//        }
        
    }
}
//MARK: private
extension MainTabBarVC {
   
    
    private func drawTabBar() {
        // font
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)], for: .normal)
		
		self.tabBar.barTintColor = UIColor.setColorSet(.tabbarBgColor)
		self.tabBar.tintColor = UIColor.setColorSet(.tabbarTintColor)
        self.tabBar.unselectedItemTintColor = UIColor.gray
        
        // background
//        self.tabBar.isTranslucent = false
//		self.tabBar.barTintColor = UIColor(hexString: ColorInfo.MAIN)
        
        // 연락처

		let blockVC = BlockListVC()
		blockVC.title = TextInfo.vc_block_title
		let blockNavVC = UINavigationController(rootViewController: blockVC)
		blockNavVC.tabBarItem = UITabBarItem(title: TextInfo.vc_block_title, image: UIImage(named: "img_contacts"), selectedImage: nil)
		blockNavVC.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: 0.0)
        
        
        // 설정
        let settingVC = SettingVC()
		settingVC.title = TextInfo.vc_more_title
		let settingNavVC = UINavigationController(rootViewController: settingVC)
		settingNavVC.tabBarItem = UITabBarItem(title: TextInfo.vc_more_title, image: UIImage(named: "img_setting"), selectedImage: nil)
		settingNavVC.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: 0.0)
        
        
        let controllers = [blockNavVC ,settingNavVC]
        self.viewControllers = controllers

		
    }
    

}

//MARK: - Navigation
extension MainTabBarVC{
	func setNavigationBar(){
		
	}
}
