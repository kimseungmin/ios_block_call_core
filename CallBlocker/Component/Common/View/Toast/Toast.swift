//
//  ToastUtils.swift
//  KFHHumidifier
//
//  Created by vincent_kim on 2020/01/28.
//  Copyright © 2020 vincent.kim. All rights reserved.
//

import Foundation
import UIKit

class Toast {
    static var toastShowing:Bool = false
    static func makeToast(message: String, controller: UIViewController) {
        if Toast.toastShowing == true{
            return
        }
        let toastContainer = UIView(frame: CGRect())
		toastContainer.backgroundColor = UIColor.setColorSet(.toastBgColor).withAlphaComponent(0.8)
        
//        toastContainer.alpha = 0.0
//        toastContainer.layer.cornerRadius = 25;
//        toastContainer.clipsToBounds  =  true
        
        let toastLabel = UILabel(frame: CGRect())
        toastLabel.textColor = UIColor.setColorSet(.toastTextColor)
        toastLabel.textAlignment = .center
		toastLabel.font = UIFont.systemFont(ofSize: 14.0)
        toastLabel.text = message
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        
        toastContainer.addSubview(toastLabel)
        controller.view.addSubview(toastContainer)
        
        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastContainer.translatesAutoresizingMaskIntoConstraints = false
        
        
//        let bottomConst:CGFloat = -100
//        let device = SystemUtils.shared.getDeviceType()
        
//        let width = controller.view.bounds.size.width
//        let toastHeight:CGFloat = 50
//        let x:CGFloat = 0
//        let y = controller.view.bounds.size.height - toastHeight - bottomConst
////
//        let frame  = CGRect(x: x, y: y, width: width, height: toastHeight)
//        toastContainer.frame = frame
        
        let labelWidth = NSLayoutConstraint(item: toastLabel, attribute: .width, relatedBy: .equal, toItem: toastContainer, attribute: .width, multiplier: 1, constant: 0)
        let labelXposition = NSLayoutConstraint(item: toastLabel, attribute: .centerX, relatedBy: .equal, toItem: toastContainer, attribute: .centerX, multiplier: 1, constant: 0)
        let labelYposition = NSLayoutConstraint(item: toastLabel, attribute: .centerY, relatedBy: .equal, toItem: toastContainer, attribute: .centerY, multiplier: 1, constant: 0)
        toastContainer.addConstraints([labelWidth, labelXposition, labelYposition])
        
        let viewLeftConst = NSLayoutConstraint(item: toastContainer, attribute: .leading, relatedBy: .equal, toItem: controller.view, attribute: .leading, multiplier: 1, constant: 20)
        let viewRightConst = NSLayoutConstraint(item: toastContainer, attribute: .trailing, relatedBy: .equal, toItem: controller.view, attribute: .trailing, multiplier: 1, constant: -20)
		let viewTopConst = NSLayoutConstraint(item: toastContainer, attribute: .top, relatedBy: .equal, toItem: controller.view, attribute: .top, multiplier: 1, constant: 20)
//        let viewBottomConst = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: controller.view, attribute: .bottom, multiplier: 1, constant: bottomConst)
        let viewHeight = NSLayoutConstraint(item: toastContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        controller.view.addConstraints([viewLeftConst, viewRightConst, viewTopConst, viewHeight])
        
        toastContainer.alpha = 0.0
        
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .transitionCrossDissolve, animations: {
            toastContainer.alpha = 1.0
            Toast.toastShowing = true
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 1.0, options: .curveEaseOut, animations: {
                toastContainer.alpha = 0.0
            }, completion: {_ in
                toastContainer.removeFromSuperview()
                Toast.toastShowing = false
            })
        })
    }
}
