//
//  PurchaseMainDescCell.swift
//  twoNumSender
//
//  Created by dkt_mac on 12/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

protocol PurchaseBtnDescCellDelegate: AnyObject{
	func labelTapped(type: PurchaseBtnDescCellTypes)
}

enum PurchaseBtnDescCellTypes{
	case restore
	case terms
	case none
}

class PurchaseBtnDescCell: UITableViewCell {

	var cellType: PurchaseBtnDescCellTypes = .none
	var isEnableTapped:Bool = false{
		willSet{
			self.actionBtn.isEnabled = newValue
		}
	}
    @IBOutlet weak var descLabel: UILabel!
	@IBOutlet weak var actionBtn: UIButton!
	weak var delegate: PurchaseBtnDescCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
		self.backgroundColor = .clear
        
    }
	
	override func prepareForReuse() {
		self.descLabel.text = ""
		self.descLabel.attributedText = nil
		
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PurchaseBtnDescCell{
	@IBAction func actionBtnTapped(sender: UIButton){
		self.delegate?.labelTapped(type: self.cellType)
	}
}
