//
//  PurchaseTitleDescCell.swift
//  YouTubeSearch
//
//  Created by vincent_kim on 2020/12/26.
//

import UIKit

class PurchaseTitleDescCell: UITableViewCell {

	@IBOutlet weak var descLabel: UILabel!
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.backgroundColor = .clear
		self.descLabel.font = UIFont.systemFont(ofSize: 17.0)
		self.descLabel.textColor = .darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
