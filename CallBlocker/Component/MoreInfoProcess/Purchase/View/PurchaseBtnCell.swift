//
//  PurchaseBtnCell.swift
//  twoNumSender
//
//  Created by dkt_mac on 12/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

protocol PurchaseBtnCellDelegate: AnyObject {
	func purchaseBtnTapped(type: RegisteredPurchase)
}

class PurchaseBtnCell: UITableViewCell {
    
	var purchaseType: RegisteredPurchase = .autoRenewableMonthly
	weak var delegate:PurchaseBtnCellDelegate?
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var purchaseTypeLabel: UILabel!
    @IBOutlet weak var purchaseValueLabel: UILabel!
    @IBOutlet weak var purchaseTypeInfoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.backgroundColor = .clear
		self.btnView.toCornerRound(radius: 8.0)
        self.purchaseTypeInfoLabel.text = TextInfo.purchase_info1 + TextInfo.purchase_info2
		self.purchaseTypeInfoLabel.customLabel(string: TextInfo.purchase_info1, withColor: UIColor.setColorSet(.textColor), underLine: false, bold: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PurchaseBtnCell{
	@IBAction func purchaseBtnTapped(){
		self.delegate?.purchaseBtnTapped(type: self.purchaseType)
	}
}

//extension PurchaseBtnCell{
//    class func cellHeight() -> CGFloat {
//        return 62
//    }
//}
