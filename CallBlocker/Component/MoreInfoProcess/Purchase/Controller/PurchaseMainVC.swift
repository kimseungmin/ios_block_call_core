//
//  PurchaseMainVC.swift
//  twoNumSender
//
//  Created by dkt_mac on 12/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit
import SVProgressHUD

enum PurchaseCellTypes{
    case mainDesc
    case background
    case monthBtn
    case yearBtn
    case restore
    case subDesc
    case term
}

class PurchaseMainVC: UITableViewController {
    
    let purchaseCells:[PurchaseCellTypes] = [.mainDesc, .background ,.yearBtn, .monthBtn, .restore, .subDesc, .term]
    
    override func viewDidLoad() {
        super.viewDidLoad()

//		self.view.backgroundColor = .white
        self.drawNavigationBar()
        self.registerCell()
    
    }
	override func viewWillAppear(_ animated: Bool) {

	}
	override func viewWillDisappear(_ animated: Bool) {

	}
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.purchaseCells.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { // row 높이 지정
        let row = self.purchaseCells[indexPath.row]
        var cellHeight:CGFloat
        switch row {
        case .mainDesc, .subDesc:
            cellHeight = UITableView.automaticDimension
        case .monthBtn, .yearBtn:
            cellHeight = UITableView.automaticDimension//PurchaseBtnCell.cellHeight()
        case .background:
            cellHeight = UITableView.automaticDimension
        case .restore:
            cellHeight = UITableView.automaticDimension
        case .term:
            cellHeight = UITableView.automaticDimension
        }
        return cellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell
        
        let row = self.purchaseCells[indexPath.row]
        
        switch row {
        case .mainDesc:
            let mainDescCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseTitleDescCell.self)) as! PurchaseTitleDescCell
			
			let string = TextInfo.purchase_main_desc1 + TextInfo.purchase_main_desc2
			mainDescCell.descLabel.text = string
			mainDescCell.descLabel.customLabel(string: TextInfo.purchase_main_desc1, withColor: UIColor.black, underLine: false, bold: true)
			
            cell = mainDescCell
            
        case .background:
            let bgCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseBackgroundCell.self)) as! PurchaseBackgroundCell
            
            cell = bgCell
            
            
        case .monthBtn:
            let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseBtnCell.self)) as! PurchaseBtnCell
            btnCell.purchaseTypeLabel.text  = TextInfo.purchase_month_btn_status

            btnCell.purchaseValueLabel.text = TextInfo.purchase_month_btn_value

//			btnCell.btnView.backgroundColor = .systemOrange
//            btnCell.btnView.outline(borderWidth: 0, borderColor: UIColor.white, opacity: 1.0)
			
			btnCell.purchaseValueLabel.textColor = .white
			btnCell.purchaseTypeLabel.textColor = .white
			
			
            btnCell.purchaseTypeInfoLabel.text = TextInfo.purchase_info_month
			btnCell.btnView.backgroundColor = UIColor.setColorSet(.btnSelectedColor)
			btnCell.purchaseType = .autoRenewableMonthly
			btnCell.delegate = self
            
            cell = btnCell
        case .yearBtn:
            let btnCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseBtnCell.self)) as! PurchaseBtnCell
			btnCell.purchaseTypeInfoLabel.text = TextInfo.purchase_info1 + TextInfo.purchase_info2
			btnCell.purchaseTypeInfoLabel.customLabel(string: TextInfo.purchase_info1, withColor: UIColor.black, underLine: false, bold: false)
			
            btnCell.purchaseTypeLabel.text  = TextInfo.purchase_year_btn_status

            btnCell.purchaseValueLabel.text = TextInfo.purchase_year_btn_value

//            btnCell.btnView.backgroundColor = UIColor.white
			btnCell.btnView.outline(borderWidth: 1, borderColor: UIColor.setColorSet(.btnSelectedColor), opacity: 1.0)
			
//			btnCell.btnView.backgroundColor = .systemBackground
			btnCell.purchaseValueLabel.textColor = .black
			btnCell.purchaseTypeLabel.textColor = .black
			
            btnCell.purchaseTypeInfoLabel.isHidden = false
            
			
			btnCell.purchaseType = .autoRenewableYearly
			btnCell.delegate = self
			
            
            cell = btnCell
        
        case .restore:
            let restoreCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseBtnDescCell.self)) as! PurchaseBtnDescCell
            restoreCell.descLabel.text = TextInfo.purchase_restore
            restoreCell.descLabel.customLabel(string: "여기", withColor: UIColor.blue, underLine: true, bold: false)
            restoreCell.descLabel.font = UIFont.systemFont(ofSize: 16)
			
			restoreCell.cellType = .restore
			restoreCell.delegate = self
			
            cell = restoreCell
            
        case .subDesc:
            let subDescCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseSubDescCell.self)) as! PurchaseSubDescCell
            subDescCell.descLabel.text = TextInfo.purchase_sub_desc
            
            cell = subDescCell
            
        case .term:
            let termLinkCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PurchaseBtnDescCell.self)) as! PurchaseBtnDescCell
            
			termLinkCell.descLabel.text = "서비스 이용약관"
            termLinkCell.descLabel.customLabel(string: termLinkCell.descLabel.text!, withColor: UIColor.blue, underLine: true, bold: false)
            termLinkCell.descLabel.font = UIFont.systemFont(ofSize: 14)
			
			termLinkCell.cellType = .terms
			termLinkCell.delegate = self
            cell = termLinkCell
            
        }
        

        // Configure the cell...

        return cell
    }
	
    
    
}
extension PurchaseMainVC{
    private func registerCell() {
        self.tableView.allowsSelection = false
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 90
		
		self.tableView.register(UINib(nibName: String.init(describing: PurchaseTitleDescCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseTitleDescCell.self))
        self.tableView.register(UINib(nibName: String.init(describing: PurchaseBtnDescCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseBtnDescCell.self))
        
        self.tableView.register(UINib(nibName: String.init(describing: PurchaseSubDescCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseSubDescCell.self))
        
        self.tableView.register(UINib(nibName: String.init(describing: PurchaseBtnCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseBtnCell.self))
        
        self.tableView.register(UINib(nibName: String.init(describing: PurchaseBackgroundCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: PurchaseBackgroundCell.self))
        
        
    }
    
}

extension PurchaseMainVC{
    private func drawNavigationBar() {
        
        self.navigationController?.navigationBar.isTranslucent = false          // 투명도 설정
		self.navigationController?.navigationBar.barTintColor = .white
		self.navigationController?.navigationBar.tintColor = .black
        
        self.navigationController?.navigationBar.shadowImage = UIImage()        // 네비 하단바 삭제 ?
        
        
        self.title = "프리미엄 상품보기"
//		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: ColorInfo.MainTint]
        
        // set right bar - 메인화면으로 이동
        let rightBarImage = UIImage(systemName: "multiply")//UIImage(named: "error")?.withRenderingMode(UIImage.RenderingMode.automatic)
        let rightBarButtonItem =  UIBarButtonItem(image: rightBarImage,
                                                  style: UIBarButtonItem.Style.done,
                                                  target: self,
                                                  action: #selector(actionPop))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
//		self.navigationItem.rightBarButtonItem?.tintColor = ColorInfo.MainTint

    }
    
    
}

// @objc
extension PurchaseMainVC{
    
    func selectTermType(){
		let alert = UIAlertController(title: "서비스 이용약관", message: "약관 유형을 선택하세요", preferredStyle: .actionSheet)
        
		alert.addAction(UIAlertAction(title: "서비스 이용약관", style: .default) { action in
            self.moveToTerm()
        })
		
		alert.addAction(UIAlertAction(title: "개인정보 처리방침", style: .default) { action in
			self.moveToPolicy()
		})
		
		alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .cancel) { action in
            
        })
        
        self.present(alert, animated: true, completion: nil)
    }

}

// move Page
extension PurchaseMainVC{
    func moveToTerm(){

		let values:[String:Any] = ["landingUrl": URLInfo.local_terms_of_service_address,
								   "vcTitle":"서비스 이용약관"
		]
		RouterManager.shared.dispatcher(.commonWebview, values: values)

    }
	func moveToPolicy(){
		
		let values:[String:Any] = ["landingUrl": URLInfo.local_policy_address,
								   "vcTitle":"개인정보 처리방침"
		]
		RouterManager.shared.dispatcher(.commonWebview, values: values)
		
	}
	
    @objc func actionPop() {
        //        self.navigationController?.popViewController(animated: true)
        self.navigationController?.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: Notification.Name.refreshInAppPurchase, object: nil, userInfo: nil)
    }
}


extension PurchaseMainVC: PurchaseBtnDescCellDelegate{
	func labelTapped(type: PurchaseBtnDescCellTypes) {
		switch type {
			
			case .restore:
				IAPManager.shared.restore()
			case .terms:
				self.selectTermType()
			case .none:
				break
		}
	}
	
}

extension PurchaseMainVC: PurchaseBtnCellDelegate{
	func purchaseBtnTapped(type: RegisteredPurchase) {
		switch type {
			
			case .autoRenewableMonthly:
				IAPManager.shared.purchase(.autoRenewableMonthly, atomically: true)
			case .autoRenewableYearly:
				IAPManager.shared.purchase(.autoRenewableYearly, atomically: true)
		}
	}
	
}


