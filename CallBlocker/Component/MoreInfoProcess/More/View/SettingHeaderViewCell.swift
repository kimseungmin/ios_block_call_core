//
//  SettingHeaderViewCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 06/06/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class SettingHeaderViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var bgView: UIView!
	
	@IBOutlet weak var symbolImageView: UIImageView!
	
	override func prepareForReuse() {
	}
	
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
		DispatchQueue.main.async { [weak self] in
			guard let self = self else {
				return
			}
			self.setUI()
			
		}
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
		DispatchQueue.main.async { [weak self] in
			guard let self = self else {
				return
			}
			self.setUI()
			
		}
    }
    
}

//MARK: - UI
extension SettingHeaderViewCell{
	
	private func setUI(){
		self.contentView.backgroundColor = .clear
		let backgroundView = UIView(frame: self.bounds)
		backgroundView.backgroundColor = .clear
		
		self.backgroundView = backgroundView
		
		
		
		self.titleLabel.font = UIFont.fontWithName(type: .bold, size: 15.0)
//		self.titleLabel.textColor = .white
		
		
	}
	
}


extension SettingHeaderViewCell {
    class func cellHeight() -> CGFloat {
        return 32
    }
}
