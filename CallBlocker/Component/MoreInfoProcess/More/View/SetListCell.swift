//
//  SetListCell.swift
//  twoNumSender
//
//  Created by seungmin kim on 26/05/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import UIKit

class SetListCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var setListCheckBoxView: UIImageView!
	@IBOutlet weak var subTitleLabel: UILabel!
	
	
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.setUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
	override func prepareForReuse() {
		self.titleLabel.text = ""
		self.subTitleLabel.text = ""
	}
}

extension SetListCell{
	func setUI(){
//		self.backgroundColor = .clear
//		self.contentBgView.backgroundColor = ColorInfo.MainSub
//		self.contentBgView.toCornerRound(radius: 10.0)
		
//		self.rankLabel.font = UIFont.fontWithName(type: .bold, size: 20.0)
//		self.rankLabel.textColor = .white
		
		
//		self.titleLabel.textColor = .white
		self.titleLabel.font = UIFont.fontWithName(type: .regular, size: 15.0)
//		self.subTitleLabel.textColor = .green
		self.subTitleLabel.font = UIFont.fontWithName(type: .regular, size: 16.0)
		
		
		
	}
}
