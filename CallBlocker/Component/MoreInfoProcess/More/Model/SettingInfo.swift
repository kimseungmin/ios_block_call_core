//
//  SettingInfo.swift
//  YouTubeSearch
//
//  Created by vincent_kim on 2020/12/29.
//

import Foundation
//MARK: - Section



enum SettingSectionTypes{
	struct SettingSectionStruct {
		var title: String
		
		init (title: String) {
			self.title = title
		}
	}
	
    
	case purchase
    case support
    case share
    case info
	
	func info() -> SettingSectionStruct{
		switch self {
			
			case .purchase:
				return SettingSectionStruct(title: "구매 관리")
			case .support:
				return SettingSectionStruct(title: "지원")
			case .share:
				return SettingSectionStruct(title: "기타")
			case .info:
				return SettingSectionStruct(title: "정보")
		}
	}
}


enum Helps{
	struct HelpsStruct {
		var title: String
		
		init (title: String) {
			self.title = title
		}
	}
    case notice
    case contact
    case guide
	
	func info() -> HelpsStruct{
		switch self{
			case .notice : return HelpsStruct(title: "공지 / 자주 묻는 질문")
			case .contact : return HelpsStruct(title: "문의하기")
			case .guide : return HelpsStruct(title: "설명서")
		}
	}
	

}

enum Infos{
	struct InfosStruct {
		var title: String
		
		init (title: String) {
			self.title = title
		}
	}
	
	case update
	case privacy
	case service
	
	func info() -> InfosStruct{
		switch self{
			case .privacy : return InfosStruct(title: "개인정보 처리방침")
			case .service : return InfosStruct(title: "서비스 이용약관")
			case .update : return InfosStruct(title: "앱 버전")
		}
	}
	
}


enum Shares{
	
	struct SharesStruct {
		var title: String
		
		init (title: String) {
			self.title = title
		}
	}
	
	case share
	case review
	
	func info() -> SharesStruct{
		switch self{
			case .share : return SharesStruct(title: "친구에게 공유하기")
			case .review : return SharesStruct(title: "리뷰하기")
			
		}
	}
	
}

enum Purchases{
	
	struct PurchasesStruct {
		var title: String
		
		init (title: String) {
			self.title = title
		}
	}
	
	case goToPremium
	case restore
	
	func info() -> PurchasesStruct{
		switch self{
			case .goToPremium : return PurchasesStruct(title: "프리미엄 상품보기")
			case .restore : return PurchasesStruct(title: "구매 상품 복원하기")
			
		}
	}
	
}
