//
//  SettingVC.swift
//  twoNumSender
//
//  Created by vincent_kim on 2020/10/12.
//  Copyright © 2020 remain. All rights reserved.
//

import UIKit
import MessageUI
import SVProgressHUD
import StoreKit
import CallKit
import SwiftyJSON
import CloudKit


protocol SettingVCDelegate:AnyObject {
	func clearPlayList()

}


class SettingVC: UIViewController, MFMessageComposeViewControllerDelegate {
	var adminCount = 0
	
//    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
//    let managedContext = CoreDataManager.shared.managedObjectContext
	
    var sectionArray :[SettingSectionTypes]     = []


	var helpArray : [Helps]                     = [.contact]//[.notice, .contact, .guide]
    var infoArray : [Infos]                     = [.privacy, .service, .update]
    var shareArray : [Shares]                   = [.share, .review]
    var purchaseArray : [Purchases]             = [.goToPremium, .restore]
	
	var product:SKProduct?
	
	let purchaseStatus = UserDefaults.standard.string(forKey: .premium)
	
	
	@IBOutlet weak var tableView: UITableView!
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.setUI()
		self.setTableView()
		self.setNavigationBar()
		self.registerNotification()
		
		
	}
	deinit {
		self.unregisterNotification()
		print(debug: "", self)
	}
	
	override func viewWillAppear(_ animated: Bool) {
        self.setSectionArray()

	}
	
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
		self.dismiss(animated: true, completion: nil)
	}
	
	
	
}

//MARK: - UI
extension SettingVC{
	private func setUI(){
		
	}
	
}


// table view del
extension SettingVC: UITableViewDelegate, UITableViewDataSource{
	
	@objc func setSectionArray(){
		

		self.sectionArray = [.purchase, .support, .share, .info]
		
		
			DispatchQueue.main.async { [weak self] in
				guard let self = self else { return }
				UIView.transition(with: self.tableView, duration: 0.5, options: .transitionCrossDissolve, animations: {
					self.tableView.reloadData()
				}, completion: nil)
			}
			
		
		
	}
	
	func setTableView(){
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.tableFooterView = UIView()
		self.tableView.alwaysBounceVertical = false
		self.registerCell()
		self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
		
		
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return SettingHeaderViewCell.cellHeight()
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return self.sectionArray.count
	}
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String.init(describing: SettingHeaderViewCell.self)) as! SettingHeaderViewCell
		let sectionType = self.sectionArray[section]
		
		headerView.titleLabel.text = sectionType.info().title
		
		return headerView
		
	}
	
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		var count = 0
		let sectionType = self.sectionArray[section]
		switch sectionType {
		
		case .support:
			count = self.helpArray.count
		case .share:
			count = self.shareArray.count
		case .info:
			count = self.infoArray.count
		case .purchase:
			count = self.purchaseArray.count
		
		}
		
		return count
		
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell: UITableViewCell
		let setListCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: SetListCell.self)) as! SetListCell
		setListCell.setListCheckBoxView.isHidden = true
		setListCell.accessoryType = .none
		
		setListCell.subTitleLabel.text = ""
		var labelText = ""
		
		let section = self.sectionArray[indexPath.section]
		
		switch section {
		case .support:
			
			let row = self.helpArray[indexPath.row]
			labelText = row.info().title
			
			setListCell.titleLabel.text = labelText

			
		case .info:
			
			let row = self.infoArray[indexPath.row]
			labelText = row.info().title
			switch row{
			case .update:
				let appVersion = SystemUtils.shared.appVersion()
				labelText = row.info().title + " " + appVersion
				if !SystemUtils.shared.isUpdateAvailable(){
					labelText += " (최신 버전입니다)"
				}
				else{
					labelText += " (새로운 버전이 있습니다)"
				}
			
			case .privacy:
				break
			case .service:
				break
			}
			setListCell.titleLabel.text = labelText
			
			
		case .share:
			let row = self.shareArray[indexPath.row]
			labelText = row.info().title
			setListCell.titleLabel.text = labelText
//			cell = setListCell
			
		case .purchase:
			
			let row = self.purchaseArray[indexPath.row]
			labelText = row.info().title
			setListCell.titleLabel.text = labelText
//			cell = setListCell
		
		}
		
		
		cell = setListCell
		cell.selectionStyle = .default

		return cell
		
	}
	
	
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if let index = self.tableView.indexPathForSelectedRow{
			self.tableView.deselectRow(at: index, animated: true)
		}

		
		let sectionType = self.sectionArray[indexPath.section]
		switch sectionType {
		
		// 도움말
		case .support:
			let row = self.helpArray[indexPath.row]
			switch row{
			case .notice:
				
//				let values:[String:Any] = ["landingUrl": URLInfo.facebook_page,
//										   "vcTitle":row.info().title]
//
//				RouterManager.shared.dispatcher(.commonWebview, values: values)
				break
			case .contact:
				self.showContactAlert()
				
			case .guide:
				break
				
//				let values:[String:Any] = ["tutorialType": TutorialStatus.tutorial]
//				RouterManager.shared.dispatcher(.guide, values: values)
							
			}
		// 정보
		case .info:
			let row = self.infoArray[indexPath.row]
			switch row{

			case .update:
				let title = "앱 스토어로 이동하기"
				let msg = "앱 스토어로 이동할까요?"
				let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
				
				alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .cancel) { action in
					
				})
				
				alert.addAction(UIAlertAction(title: TextInfo.alert_action_confirm, style: .default) { action in
					
					let appLinkUrl = "itms-apps://itunes.apple.com/app/id\(AppInfo.appId)"
					
					let URL = NSURL(string : appLinkUrl)!
					UIApplication.shared.open(URL as URL, options: [:], completionHandler: nil)
					
				})
				
				self.present(alert, animated: true, completion: nil)
				case .privacy:
					
					let values:[String:Any] = ["landingUrl": URLInfo.local_policy_address,
											   "vcTitle":"개인정보 처리방침"
					]
					RouterManager.shared.dispatcher(.commonWebview, values: values)
					
					
				case .service:
					let values:[String:Any] = ["landingUrl": URLInfo.local_terms_of_service_address,
											   "vcTitle":"서비스 이용약관"
					]
					RouterManager.shared.dispatcher(.commonWebview, values: values)
			}
		
		case .share:
			let row = self.shareArray[indexPath.row]
			switch row{
			
			case .share:
				
				let downloadLink = "itunes.apple.com/app/id\(AppInfo.appId)"
				
				//문자열을 클립보드에 복사
				UIPasteboard.general.string = downloadLink
				UIAlertController.showMessage("클립보드에 다운로드 링크가 복사되었습니다")
				 
//				//클립보드에 복사된 문자열을 가져오기
//				if let theString = UIPasteboard.general.string {
//
//				}

				
				
			case .review:

				self.showReviewAlert()

				
			}
		case .purchase:
			let row = self.purchaseArray[indexPath.row]
			switch row {
				case .goToPremium:
					RouterManager.shared.dispatcher(.inAppPurchase)
				case .restore:
					IAPManager.shared.restore()
				
			}
		}
		
	   
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//		tableView.cellForRow(at: indexPath as IndexPath)?.selectionStyle = .blue
//		tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
		
	}
}
// SET UI
extension SettingVC{
	private func registerCell() {
		self.tableView.register(UINib(nibName: String.init(describing: SetListCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: SetListCell.self))
		self.tableView.register(UINib(nibName: String.init(describing: SettingHeaderViewCell.self), bundle: nil), forHeaderFooterViewReuseIdentifier: String.init(describing: SettingHeaderViewCell.self))
	}

}

// event bus
extension SettingVC{
	private func registerNotification() {
		NotificationCenter.default.addObserver(self, selector: #selector(setSectionArray), name: Notification.Name.refreshInAppPurchase, object: nil)
		
	}
	
	// unregister notification
	private func unregisterNotification() {
		NotificationCenter.default.removeObserver(self, name: Notification.Name.refreshInAppPurchase, object: nil)

	}
}


//MARK: - Navigation Bar
extension SettingVC{
	func setNavigationBar(){
//		self.navigationController?.navigationBar.isTranslucent = false
//		self.navigationController?.navigationBar.barTintColor = UIColor.white
		self.navigationController?.navigationBar.prefersLargeTitles = false
//        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(ColorInfo.MAIN) as Any]
		
		// 네비 하단바 삭제
		
//		self.navigationController?.navigationBar.shadowImage = UIImage()
		
		// set left button

//		let leftImage = UIImage(systemName: "questionmark.circle.fill")//?.withRenderingMode(.alwaysOriginal)
//		let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(image: leftImage, style:.plain, target: self, action: #selector(leftBtnTapped))
//		self.navigationItem.leftBarButtonItem = leftBarButtonItem

		// set right button
//		let rightImage = UIImage(systemName: nil)//?.withRenderingMode(.alwaysOriginal)
		let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(title: "   ", style: .plain, target: self, action: #selector(rightBtnTapped))
		self.navigationItem.rightBarButtonItem = rightBarButtonItem
		
		
		// title
//		self.title = "더 보기"
		
		
		
//		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexString: ColorInfo.TOPBAR)]
//        self.navigationController?.navigationBar.barTintColor = UIColor(ColorInfo.MAIN)
		
	}
	
	
	@objc func leftBtnTapped() {
		
	}

	@objc func rightBtnTapped() {
		self.adminCount += 1
		
		if self.adminCount == 5{
			// debug for vincent
			self.adminCount = 0
			self.vincentAlert()
		}
	}
	
}


//MARK: - Alert
extension SettingVC{
	func showReviewAlert(){
		let title = "앱 리뷰하기"
		let msg = String(format: "%@를 이용해 주셔서 감사합니다\n여러분의 소중한 리뷰가 힘이됩니다\n앱스토어로 이동할까요?", SystemUtils.shared.appName())
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .cancel) { action in
			
		})
		
		alert.addAction(UIAlertAction(title: TextInfo.alert_action_confirm, style: .default) { action in

			let appLinkUrl = "itms-apps://itunes.apple.com/app/id\(AppInfo.appId)?action=write-review"

			let URL = NSURL(string : appLinkUrl)!
			UIApplication.shared.open(URL as URL, options: [:], completionHandler: nil)
			
		})
		
		
		self.present(alert, animated: true, completion: nil)
	}
	/*vincent alert*/
	func vincentAlert(){
		let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
		alert.addTextField()
		alert.addAction(UIAlertAction(title: TextInfo.alert_action_confirm, style: .default) { action in
			let text = alert.textFields?[0].text
			if text == IAPManager.adminString{
				
				UserDefaults.standard.set(IAPManager.adminString, forKey: .premium)
				IAPManager.shared.verifyRecipt(withProgress: true)

			}
			if text == "off"{
				
				UserDefaults.standard.set("", forKey: .premium)
				IAPManager.shared.verifyRecipt(withProgress: true)

			}
			
			
			
		})
		
		self.present(alert, animated: true, completion: nil)
	}
	
	func showContactAlert(){
		let alert = UIAlertController(title: "문의하기", message: "문의 방법을 선택하세요", preferredStyle: .alert)
//		alert.addAction(UIAlertAction(title: "페이스북 메신저로 문의하기", style: .default) { action in
//			let values:[String:Any] = ["landingUrl": URLInfo.facebook_page,
//									   "vcTitle": Helps.contact.info().title]
//			RouterManager.shared.dispatcher(.commonWebview, values: values)
//
//		})
		
		alert.addAction(UIAlertAction(title: "메일로 문의하기 (로그 파일을 첨부합니다)", style: .default) { action in
			
			self.logFileAlert()
			
		})
		
		alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .cancel) { action in
			
		})
		self.present(alert, animated: true, completion: nil)
	}
	
	
	/*오류 제보 Alert*/
	func logFileAlert(){
		
		var title = ""
		var msg = ""
		title = "E-mail로 문의하기"
		msg = "회신받을 E-mail 주소를 입력해 주세요"
		
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		let okAction = UIAlertAction(title: TextInfo.alert_action_confirm, style: .default) { action in
			let text = alert.textFields?[0].text
			if text == ""{
				return
			}else{
				self.sendEmailForLogFile(email: text ?? "")
			}
		}
		okAction.isEnabled = false
		alert.addTextField { (textField) in
			
			// Observe the UITextFieldTextDidChange notification to be notified in the below block when text is changed
			NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main, using:
				{_ in
					// Being in this block means that something fired the UITextFieldTextDidChange notification.
					
					// Access the textField object from alertController.addTextField(configurationHandler:) above and get the character count of its non whitespace characters
					let textCount = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0
					let textIsNotEmpty = textCount > 0
					
					// If the text contains non whitespace characters, enable the OK Button
					okAction.isEnabled = textIsNotEmpty
				
			})
		}
		
		alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .destructive, handler: nil))
		alert.addAction(okAction)
		
		self.present(alert, animated: true, completion: nil)
	}
	
	// send Email - log file
	private func sendEmailForLogFile(email:String) {
		
		if MFMailComposeViewController.canSendMail() {

			let mail = MFMailComposeViewController()
			mail.mailComposeDelegate = self
			mail.setToRecipients([ReportEmailInfo.recipients_address])
			mail.setCcRecipients([ReportEmailInfo.recipients_cc_address])
			mail.setSubject(ReportEmailInfo.email_subject)
			
			LogManager.shared.removeFileData()
			LogManager.shared.write("\n[사용 환경]\nOS 버전 : \(SystemUtils.shared.osVersion())\nApp 버전 : \(SystemUtils.shared.appVersion())\n사용기기 : \(UIDevice.current.deviceType.displayName)", self)
			IAPManager.shared.verifyRecipt(withProgress: true)
			SVProgressHUD.show()
			SystemUtils.shared.delay(delay: 5){
				// 본문 채우기
				var messageBody = String.init(format: "*[필수] 회신받을 이메일 주소 : %@\n\n오류내용을 적어주세요:\n\n\n\n\n\n\n\n[정보]\nOS 버전 : %@\n앱 버전 : %@\n사용 기기 : %@", email, SystemUtils.shared.osVersion(), SystemUtils.shared.appVersion(), UIDevice.current.deviceType.displayName)
				
				
				if let logData = LogManager.shared.getFileData(){
					mail.addAttachmentData(logData, mimeType: "text/txt", fileName: "\(Bundle.appName())_Log.txt")
				}
				else{
					messageBody += "\n - 데이터 수집을 실패했습니다"
				}
				
				mail.setMessageBody(messageBody, isHTML: false)
				
				
				
				self.present(mail, animated: true) {
					SVProgressHUD.dismiss()
				}
			}
			
		}
		else {
			let alertController = UIAlertController(title: nil, message: ReportEmailInfo.alert_send_email_fail, preferredStyle: UIAlertController.Style.alert)

			let okAction = UIAlertAction(title: TextInfo.alert_action_confirm, style: UIAlertAction.Style.destructive)
			alertController.addAction(okAction)
			self.present(alertController, animated: true, completion: nil)
		}
	}
	
}
// MARK: MFMailComposeViewController Delegate
extension SettingVC: MFMailComposeViewControllerDelegate {
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		switch result.rawValue {
		case MFMailComposeResult.cancelled.rawValue:
			print(debug: "Cancelled", self)
		case MFMailComposeResult.saved.rawValue:
			print(debug: "Saved", self)
		case MFMailComposeResult.sent.rawValue:
			print(debug: "Sent", self)
		case MFMailComposeResult.failed.rawValue:
			print(debug: "Error: \(String(describing: error?.localizedDescription))", self)
		default:
			break
		}
		controller.dismiss(animated: true, completion: nil)
	}
	
}
