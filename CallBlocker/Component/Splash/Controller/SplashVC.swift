//
//  SplashVC.swift
//

import UIKit
import AuthenticationServices

class SplashVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		print(debug: "", self, isSimple: true)

    }
	
//	override var preferredStatusBarStyle: UIStatusBarStyle{
//		return .lightContent
//	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		SystemUtils.shared.delay(delay: 0.5){
			self.updateChecker()
		}
		
	}
    
    deinit {
        print(debug: "", self)
    }


}
//MARK:- 업데이트 체크
extension SplashVC{
	
	func updateChecker(){

		let needUpdate = SystemUtils.shared.isUpdateAvailable()
		
		if needUpdate == true{
			self.showUpdateAlert(alertType: .normal)
			
		}else{
			// 영수증 검증 -> 메인에서 처리
//			IAPManager.shared.verifyRecipt()

			DispatchQueue.main.async { () -> Void in
				RouterManager.shared.dispatcher(.main)
			}
			
		}
	}
}

//MARK: - Update Alert

extension SplashVC{
	enum AlertType {
		case normal
		case force
	}
	
	func showUpdateAlert(alertType: AlertType) {
		let alert = UIAlertController(title: AppInfo.alert_update_title, message: AppInfo.alert_update_message, preferredStyle: .alert)
		let okAction = UIAlertAction(title: AppInfo.alert_ok, style: .default, handler: { Void in
			guard let url = URL(string: "itms-apps://itunes.apple.com/app/id\(AppInfo.appId)") else { return }
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		})
		
		
		alert.addAction(okAction)
		if alertType == .normal {
			alert.addAction(UIAlertAction(title: AppInfo.alert_cancel, style: .destructive) { action in
//				 영수증 검증 -> 메인에서 처리
//				IAPManager.shared.verifyRecipt()

				DispatchQueue.main.async { () -> Void in
					RouterManager.shared.dispatcher(.main)
				}
			})
			
		}
		//        UIApplication.shared.keyWindow?.rootViewController
		self.present(alert, animated: true, completion: nil)
	}
}


extension SplashVC{
	@IBAction func test(sender: UIButton){


	}
	
}
