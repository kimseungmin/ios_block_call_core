//
//  SystemUtils.swift
//  ParkingManagement
//
//  Created by rex.kim on 19/02/2019.
//  Copyright © 2019 김강훈. All rights reserved.
//

import UIKit


final class SystemUtils {
    static let shared = SystemUtils()
	private var needUpdate:Bool?
    private init() {
        
    }
	
	
	func rootViewController() -> UIViewController?{
		return UIWindow.key?.rootViewController
	}
	
	
	/**
	Disable print "[LayoutConstraints] Unable to simultaneously satisfy constraints."
	*/
	func setConstraintsLog(_ isOn:Bool){
		UserDefaults.standard.setValue(isOn, forKey:"_UIConstraintBasedLayoutLogUnsatisfiable")

	}
	
}



//MARK: - Nib & File
extension SystemUtils{
	static func getNibBundle(vc: AnyClass) -> Bundle {
		return Bundle(for: vc.self)
//        return Bundle(for: SelectNetworkVC.self)
		// print(Utility.getNibBundl1e)
	}
	
	static func getNibName(name: String) -> String {
		return name
//        return "SelectNetworkVC"
		// print(Utility.getNibName)
	}
	
	func fileExists(path: String) -> Bool {
		return FileManager.default.fileExists(atPath: path)
		 
	}
	
	func bundleURL(_ name: String, withExtension: String) -> URL? {
		return Bundle.main.url(forResource: name, withExtension: withExtension)
	}
	
}


//MARK: - 디바이스
extension SystemUtils{
	
	enum deviceType {
		case iPhoneSE
		case iPhone6to8
		case iPhone6to8plus
		case iPhoneX
		case iPhoneXsMax
		case iPhoneXr
		case unknown
	}
	
	func getDeviceType() -> deviceType {
		
		
		if UIDevice().userInterfaceIdiom == .phone {
			switch UIScreen.main.nativeBounds.height {
			case 1136:
				return .iPhoneSE
			case 1334:
				return .iPhone6to8
				
			case 1920, 2208:
				return .iPhone6to8plus
				
			case 2436:
				return .iPhoneX
				
			case 2688:
				return .iPhoneXsMax
				
			case 1792:
				return .iPhoneXr
				
			default:
				return .unknown
				
			}
		}else{
			// not iPhone
			return .unknown
		}
		
	}
}

//MARK: - UI
extension SystemUtils{
	func statusBarHeight() -> CGFloat {
		
		if #available(iOS 13.0, *) {
			let height = UIWindow.key?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
			return height
		} else {
			// Fallback on earlier versions
			return UIApplication.shared.statusBarFrame.height
		}

	}
	
	func getTopSafeAreaHeight() -> CGFloat{
		var topSafeAreaHeight: CGFloat = 0
		if #available(iOS 11.0, *) {
			let window = UIApplication.shared.windows[0]
			let safeFrame = window.safeAreaLayoutGuide.layoutFrame
			topSafeAreaHeight = safeFrame.minY
		}
		return topSafeAreaHeight
	}
	
	func delay(delay: Double, closure: @escaping () -> ()) {
		
		DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
			closure()
		}
	}
}

//MARK:- 버전관리
extension SystemUtils{
	
	func appVersion() -> String {
		return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
	}
	func appVersionInt() -> Int {
		return self.appVersion().replacingOccurrences(of: ".", with: "").toInt()
	}
	
	func appName() -> String{
		guard Bundle.main.infoDictionary != nil else {
			return ""
		}
		
//		return Bundle.main.infoDictionary!["CFBundleName"] as! String
		return Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String
		
		
	}

	func appstoreVersion() -> String {
		let url = URL(string: "https://itunes.apple.com/kr/lookup?id=\(AppInfo.appId)")!
		do {
			let data = try Data(contentsOf: url)
			let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
			let results = json?["results"] as! [[String: Any]]

			let appStoreVersion: String
			if results.count > 0 {
				appStoreVersion = results[0]["version"] as! String
			}
			else {
				appStoreVersion = ""
			}
			return appStoreVersion
		}
		catch {
			print(debug: "\(error.localizedDescription)", self)
		}

		return ""
	}
	func osVersion() -> String {
		return ProcessInfo().operatingSystemVersionString
	}
	
	// 앱스토어 버전과 앱 버전 비교하여 업데이트 가능 여부체크
	func isUpdateAvailable() -> Bool {
		
		if let needUpdate = self.needUpdate{
			return needUpdate
		}
		else{
			let appVersion = self.appVersion()
			let appStoreVersion = self.appstoreVersion()
			let needUpdate = appVersion.compare(appStoreVersion, options: .numeric)
			
			if needUpdate == .orderedSame {
				// same version
	//			print(debug: "버전 같음 (DEBUG)", self)
				self.needUpdate = false
				return false
			} else if needUpdate == .orderedAscending {
	//			print(debug: "기 설치 버전이 더 작은 경우", self)
				self.needUpdate = true
				return true
			} else if needUpdate == .orderedDescending {
	//			print(debug: "기 설치 버전이 더 큰 경우", self)
				self.needUpdate = false
				return false
			}
			return false
		}
		
	}
	
	
	
	
}

//MARK: - 날짜
extension SystemUtils{
	func getCurrentTimeZone() -> String{

	   return TimeZone.current.identifier

	}
	func getTimeStamp() -> Int{
		let nowDouble = NSDate().timeIntervalSince1970
		
		return Int(nowDouble*1000)
	}
	
	
	func getWeekCountInMonth(year: Int, month: Int) -> Int{
//		let dateComponents = DateComponents.init(year: year, month: month)
//		let monthCurrentDayFirst = Calendar.current.date(from: dateComponents) ?? Date()
//		let monthNextDayFirst = Calendar.current.date(byAdding: .month, value: 1, to: monthCurrentDayFirst) ?? Date()
//		let monthCurrentDayLast = Calendar.current.date(byAdding: .day, value: -1, to: monthNextDayFirst) ?? Date()
//		let numberOfWeeks = Calendar.current.component(.weekOfMonth, from: monthCurrentDayLast)
//
//		return numberOfWeeks
		
		let dateComponents = DateComponents.init(year: year, month: month)
		let date = Calendar.current.date(from: dateComponents) ?? Date()
		var calendar = Calendar(identifier: .gregorian)
		calendar.firstWeekday = 1
		let weekRange = calendar.range(of: .weekOfMonth,
									   in: .month,
									   for: date)
		return weekRange?.count ?? 0
		
	}
	
	func getCurrentYear() -> Int{
		return self.getCurrentDate().0
	}
	
	func getCurrentMonth() -> Int{
		return self.getCurrentDate().1
	}
	
	func getCurrentDay() -> Int{
		return self.getCurrentDate().2
	}
	
	func getCurrentDate() -> (Int,Int,Int){
		let date = Date()
		
		let calendar = Calendar.current
		let y = calendar.component(.year, from: date)
		let m = calendar.component(.month, from: date)
		let d = calendar.component(.day, from: date)
		return(y, m, d)
	}
	
	func getCurrentWeekNumber(byMonth: Bool) -> Int{
		if byMonth{
			return Calendar.current.component(.weekOfMonth, from: Date())
		}
		else{
			return Calendar.current.component(.weekOfYear, from: Date())
		}
	}
	
}


