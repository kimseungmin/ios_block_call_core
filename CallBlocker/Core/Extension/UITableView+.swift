//
//  UITableView+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

//MARK:- UITableView
extension UITableView {
	
	func reloadWithoutAnimation() {
		let lastScrollOffset = contentOffset
		beginUpdates()
		endUpdates()
		layer.removeAllAnimations()
		setContentOffset(lastScrollOffset, animated: false)
	}
	
	func isCellVisible(indexPath: IndexPath) -> Bool {
		guard let indexes = self.indexPathsForVisibleRows else {
			return false
		}
		return indexes.contains(indexPath)
	}

}

