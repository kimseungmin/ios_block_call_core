//
//  UIButton+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

//MARK: - UIButton Extension (UIControl Extension - add action)
//for  UIButton AddAction Every where
extension UIControl: Actionable {}

protocol Actionable {
	associatedtype T = Self
	func addAction(for controlEvent: UIControl.Event, action: ((T) -> Void)?)
}

private class ClosureSleeve<T> {
	let closure: ((T) -> Void)?
	let sender: T

	init (sender: T, _ closure: ((T) -> Void)?) {
		self.closure = closure
		self.sender = sender
	}

	@objc func invoke() {
		closure?(sender)
	}
}

extension Actionable where Self: UIControl {
	/**
	actionBtn.addAction(for: .touchUpInside, action: { _ in
		self.btnHandler?(item.type, item.value)
	})
	*/
	
	func addAction(for controlEvent: UIControl.Event, action: ((Self) -> Void)?) {
		
		let previousSleeve = objc_getAssociatedObject(self, String(controlEvent.rawValue))
		objc_removeAssociatedObjects(previousSleeve as Any)
		removeTarget(previousSleeve, action: nil, for: controlEvent)

		let sleeve = ClosureSleeve(sender: self, action)
		addTarget(sleeve, action: #selector(ClosureSleeve<Self>.invoke), for: controlEvent)
		objc_setAssociatedObject(self, String(controlEvent.rawValue), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
	}
}


extension UIButton {
	func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
		UIGraphicsBeginImageContext(CGSize(width: 1.0, height: 1.0))
		guard let context = UIGraphicsGetCurrentContext() else { return }
		context.setFillColor(color.cgColor)
		context.fill(CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0))
		
		let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		 
		self.setBackgroundImage(backgroundImage, for: state)
	}
}
