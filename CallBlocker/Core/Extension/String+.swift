//
//  String+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

//MARK:- String Extension (HTML Dependency)
extension String {

	var convertSpecialCharacters: String {
		var newString = self
		let char_dictionary = [
			"&amp;" : "&",
			"&lt;" : "<",
			"&gt;" : ">",
			"&quot;" : "\"",
			"&apos;" : "'"
		];
		for (escaped_char, unescaped_char) in char_dictionary {
			newString = newString.replacingOccurrences(of: escaped_char, with: unescaped_char, options: NSString.CompareOptions.literal, range: nil)
		}
		return newString
	}
	
	// Html To Attributed String When Parse Data
	var htmlToAttributed: NSAttributedString? {
		guard let data = data(using: .utf8) else { return NSAttributedString() }
		do {
			return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html.rawValue, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
				
		} catch {
			return NSAttributedString()
		}
	}
	var htmlToString: String {
		return htmlToAttributed?.string ?? ""
	}

	func capturedGroups(withRegex pattern: String) -> [String] {
		var results = [String]()

		var regex: NSRegularExpression
		do {
			regex = try NSRegularExpression(pattern: pattern, options: [])
		} catch {
			return results
		}

		let matches = regex.matches(in: self, options: [], range: NSRange(location:0, length: self.count))

		guard let match = matches.first else { return results }

		let lastRangeIndex = match.numberOfRanges - 1
		guard lastRangeIndex >= 1 else { return results }

		for i in 1...lastRangeIndex {
			let capturedGroupIndex = match.range(at: i)
			let matchedString = (self as NSString).substring(with: capturedGroupIndex)
			results.append(matchedString)
		}

		return results
	}
	
	func matches(for regex: String) -> [String] {

		do {
			let regex = try NSRegularExpression(pattern: regex)
			let results = regex.matches(in: self,
										range: NSRange(self.startIndex..., in: self))
			return results.map {
				String(self[Range($0.range, in: self)!])
			}
		} catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return []
		}
	}
	
	/**
	get all params = "[^&?]+=[^&?]+"
	
	*/
	
	func matchingKeywords(regexPattern: String) -> [String] {
		if self.isEmpty {
			return []
		}
		
		do {
			let regex = try NSRegularExpression(pattern: regexPattern)
			let results = regex.matches(in: self, range: NSRange(location: 0, length: self.count))
			let keywords = results
				.compactMap { Range($0.range, in: self) }
				.map { String(self[$0]) }
			return keywords
		} catch {
			return []
		}
	}
		
}



//MARK:- String Extension (size Dependency)
extension String{
	func widthOfString(usingFont font: UIFont) -> CGFloat {
		let fontAttributes = [NSAttributedString.Key.font: font]
		let size = self.size(withAttributes: fontAttributes)
		return size.width
	}

	func heightOfString(usingFont font: UIFont) -> CGFloat {
		let fontAttributes = [NSAttributedString.Key.font: font]
		let size = self.size(withAttributes: fontAttributes)
		return size.height
	}

	func sizeOfString(usingFont font: UIFont) -> CGSize {
		let fontAttributes = [NSAttributedString.Key.font: font]
		return self.size(withAttributes: fontAttributes)
	}
}

// MARK:- String Extension (Utils Dependency)
extension String {
	
	init(bytes: [UInt8]) {
		self.init()
		self  = bytes.reduce("", { $0 + String(format: "%c", $1)})
	}
	
	var localized: String {
		return NSLocalizedString(self, comment: "")
	}
	
	func contains(find: String) -> Bool{
		return self.range(of: find) != nil
	}
	
	func containsIgnoringCase(find: String) -> Bool{
		return self.range(of: find, options: .caseInsensitive) != nil
	}
	
	
	func split(by length: Int) -> [String] {
		var startIndex = self.startIndex
		var results = [Substring]()
		
		while startIndex < self.endIndex {
			let endIndex = self.index(startIndex, offsetBy: length, limitedBy: self.endIndex) ?? self.endIndex
			results.append(self[startIndex..<endIndex])
			startIndex = endIndex
		}
		
		return results.map { String($0) }
	}
	
	/// random
	static func random(length: Int = 20) -> String {
		let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		var randomString: String = ""
		
		for _ in 0..<length {
			let randomValue = arc4random_uniform(UInt32(base.count))
			randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
		}
		return randomString
	}
	
	/// toBool
	func toBool() -> Bool {
		switch self {
		case "True", "true", "yes", "1":
			return true
		case "False", "false", "no", "0":
			return false
		default:
			return false
		}
	}
	
	/// toInt
	func toInt() -> Int{
		let newString = self.replacingOccurrences(of: ".", with: "")
		let value: Int = Int(newString) ?? 0
		
		return value
		
	}
	/// toDict
	func toDict() -> [String: Any]? {
		if let data = self.data(using: .utf8) {
			do {
				return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
			} catch {
				print(error.localizedDescription)
			}
		}
		return nil
	}
	
	
}


extension String {

	init?(htmlEncodedString: String) {

		guard let data = htmlEncodedString.data(using: .utf8) else {
			return nil
		}

		let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
			.documentType: NSAttributedString.DocumentType.html,
			.characterEncoding: String.Encoding.utf8.rawValue
		]

		guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
			return nil
		}

		self.init(attributedString.string)

	}

}


extension String {

	func fromBase64() -> String? {

		guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {

			return nil

		}

		return String(data: data as Data, encoding: String.Encoding.utf8)

	}

	

	func toBase64() -> String? {

		guard let data = self.data(using: String.Encoding.utf8) else {

			return nil

		}

		

		return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))

	}

}

extension String {

	func toDate(withFormat format: String = "yyyy-MM-dd'T'HH:mm:ssZ")-> Date?{

		let dateFormatter = DateFormatter()
		dateFormatter.timeZone = TimeZone.current
		//dateFormatter.locale = Locale(identifier: "fa-IR")
		dateFormatter.calendar = Calendar(identifier: .gregorian)
		dateFormatter.dateFormat = format
		let date = dateFormatter.date(from: self)

		return date

	}
}

// xxx-xxxx-xxxx 설정
extension String {
	func setPhoneNumber() -> String {
		let localNumberArray:[String] = ["02", "031", "032", "033", "041", "042", "043", "044", "051", "052", "054", "053", "054", "055", "061", "062", "063", "064", "070", "080", "050", "010"]
		var validCount = 0

		
		let textValue = self.replacingOccurrences(of: "-", with: "")

		var textValueArray:[String] = textValue.map { String($0) }
		
		if localNumberArray.contains(String(textValue.prefix(2))){
			// 02
			if textValue.count > 8{
				
				if textValue.count >= 10{
					// 지우는 경우
					textValueArray.insert("-", at: 6)
				}
				else{
					// 작성중인 경우
					textValueArray.insert("-", at: 5)
				}
			}
			else if textValue.count > 5{
				textValueArray.insert("-", at: 5)
			}
			
			
			// 02 다음에 끼워넣기
			if textValue.count > 2{
				textValueArray.insert("-", at: 2)
			}
			validCount = 10
			
			
		}
		else if localNumberArray.contains(String(textValue.prefix(3))){
			// 031 등
			if textValue.count > 8{
				if textValue.count >= 11{
					// 지우는 경우
					textValueArray.insert("-", at: 7)
				}
				else{
					// 작성중인 경우
					textValueArray.insert("-", at: 6)
				}
				
			}
			else if textValue.count > 6{
				textValueArray.insert("-", at: 6)
			}
			
			if textValue.count > 3{
				textValueArray.insert("-", at: 3)
			}
			
			
			validCount = 11
			
			
		}
		else{
			// 1588
			if textValue.count > 4{
				textValueArray.insert("-", at: 4)
			}
			validCount = 8
		}
		
		
		if textValue.count > validCount{
			textValueArray = textValueArray.dropLast()
		}
		
		return textValueArray.joined()

		
//		let _str = self.replacingOccurrences(of: "-", with: "")
//		// 하이픈 모두 빼준다
////		let arr = Array(_str)
//		let arr:[String] = _str.map { String($0) }
//
//		if arr.count >= 3 {
//			let prefix = String(format: "%@%@", String(arr[0]), String(arr[1]))
//			if prefix == "02" {
//				// 서울지역은 02번호
//				if let regex = try? NSRegularExpression(pattern: "([0-9]{2})([0-9]{3,4})([0-9]{4})", options: .caseInsensitive) {
//					let modString = regex.stringByReplacingMatches(in: _str, options: [], range: NSRange(_str.startIndex..., in: _str), withTemplate: "$1-$2-$3")
//					return modString
//
//				}
//
//			} else if prefix == "15" || prefix == "16" || prefix == "18" {
//				// 썩을 지능망...
//				if let regex = try? NSRegularExpression(pattern: "([0-9]{4})([0-9]{4})", options: .caseInsensitive) {
//					let modString = regex.stringByReplacingMatches(in: _str, options: [], range: NSRange(_str.startIndex..., in: _str), withTemplate: "$1-$2")
//					return modString
//
//				}
//
//			} else {
//				// 나머지는 휴대폰번호 (010-xxxx-xxxx, 031-xxx-xxxx, 061-xxxx-xxxx 식이라 상관무)
//				if let regex = try? NSRegularExpression(pattern: "([0-9]{3})([0-9]{3,4})([0-9]{4})", options: .caseInsensitive) {
//					let modString = regex.stringByReplacingMatches(in: _str, options: [], range: NSRange(_str.startIndex..., in: _str), withTemplate: "$1-$2-$3")
//					return modString
//
//				}
//
//			}
//
//		}
//
//		return self
		
	}
	
	

}

extension String {
	/**
	let string = "Hello,World!"
	string.substring(from: 1, to: 7)gets you: ello,Wo

	string.substring(to: 7)gets you: Hello,Wo

	string.substring(from: 3)gets you: lo,World!

	string.substring(from: 1, length: 4)gets you: ello

	string.substring(length: 4, to: 7)gets you: o,Wo
	*/
	func substring(from: Int?, to: Int?) -> String {
		if let start = from {
			guard start < self.count else {
				return ""
			}
		}

		if let end = to {
			guard end >= 0 else {
				return ""
			}
		}

		if let start = from, let end = to {
			guard end - start >= 0 else {
				return ""
			}
		}

		let startIndex: String.Index
		if let start = from, start >= 0 {
			startIndex = self.index(self.startIndex, offsetBy: start)
		} else {
			startIndex = self.startIndex
		}

		let endIndex: String.Index
		if let end = to, end >= 0, end < self.count {
			endIndex = self.index(self.startIndex, offsetBy: end + 1)
		} else {
			endIndex = self.endIndex
		}

		return String(self[startIndex ..< endIndex])
	}

	func substring(from: Int) -> String {
		return self.substring(from: from, to: nil)
	}

	func substring(to: Int) -> String {
		return self.substring(from: nil, to: to)
	}

	func substring(from: Int?, length: Int) -> String {
		guard length > 0 else {
			return ""
		}

		let end: Int
		if let start = from, start > 0 {
			end = start + length - 1
		} else {
			end = length - 1
		}

		return self.substring(from: from, to: end)
	}

	func substring(length: Int, to: Int?) -> String {
		guard let end = to, end > 0, length > 0 else {
			return ""
		}

		let start: Int
		if let end = to, end - length > 0 {
			start = end - length + 1
		} else {
			start = 0
		}

		return self.substring(from: start, to: to)
	}
}


extension StringProtocol {
	/**
	let str = "abcde"
	if let index = str.index(of: "cd") {
		let substring = str[..<index]   // ab
		let string = String(substring)
		print(string)  // "ab\n"
	}
	
	let str = "Hello, playground, playground, playground"
	str.index(of: "play")      // 7
	str.endIndex(of: "play")   // 11
	str.indices(of: "play")    // [7, 19, 31]
	str.ranges(of: "play")     // [{lowerBound 7, upperBound 11}, {lowerBound 19, upperBound 23}, {lowerBound 31, upperBound 35}]
	
	*/
	func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
		range(of: string, options: options)?.lowerBound
	}
	func endIndex<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
		range(of: string, options: options)?.upperBound
	}
	func indices<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Index] {
		ranges(of: string, options: options).map(\.lowerBound)
	}
	func ranges<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Range<Index>] {
		var result: [Range<Index>] = []
		var startIndex = self.startIndex
		while startIndex < endIndex,
			let range = self[startIndex...]
				.range(of: string, options: options) {
				result.append(range)
				startIndex = range.lowerBound < range.upperBound ? range.upperBound :
					index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
		}
		return result
	}
}

