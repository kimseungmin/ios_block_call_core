//
//  UIAlertController+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit


extension UIAlertController{
	
	static func commonDebugAlert(_ errorMsg: String) {
		let keyWindow = UIWindow.key
		
		
		let title = "DEBUG Error"
		let msg = errorMsg
		let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "confirm", style: .default) { action in
			
		})
		if let navigationController = keyWindow?.rootViewController as? UINavigationController, let presenting = navigationController.topViewController {
			presenting.present(alert, animated: true, completion: nil)
		}
		
//		if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController, let presenting = navigationController.topViewController {
//			presenting.present(alert, animated: true, completion: nil)
//		}
		
		
	}
	static func showMessage(_ message: String) {
		showAlert(title: "", message: message, actions: [UIAlertAction(title: "OK", style: .cancel, handler: nil)])
	}
	
	/**
	let alertAction = UIAlertAction(title: "ok", style: .cancel)
	UIAlertController.showAlert(title: "Notice", message: "Save Draft Btn Tapped", actions: [alertAction])
	*/
	static func showAlert(title: String?, message: String?, actions: [UIAlertAction], style: UIAlertController.Style = .alert) {
		DispatchQueue.main.async {
			let alert = UIAlertController(title: title, message: message, preferredStyle: style)
			for action in actions {
				alert.addAction(action)
			}
			
			if let topVC = UIApplication.topViewController() {
				topVC.present(alert, animated: true, completion: nil)
			}
			
//            if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController, let presenting = navigationController.topViewController {
//                presenting.present(alert, animated: true, completion: nil)
//            }
		}
	}


}
