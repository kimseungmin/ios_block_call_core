//
//  UITextField+.swift
//  YouTubeSearch
//
//  Created by vincent_kim on 2020/12/30.
//

import Foundation
import UIKit

extension UITextField {
	var clearButton: UIButton? {
		return value(forKey: "clearButton") as? UIButton
	}

	var clearButtonTintColor: UIColor? {
		get {
			return clearButton?.tintColor
		}
		set {
			let image =  clearButton?.imageView?.image?.withRenderingMode(.alwaysTemplate)
			clearButton?.setImage(image, for: .normal)
			clearButton?.tintColor = newValue
		}
	}
}
