//
//  AVAsset+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit
import Photos
import AVFoundation
import AVKit

extension AVAsset {
	var screenSize: CGSize? {
		if let track = tracks(withMediaType: .video).first {
			let size = __CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform)
			return CGSize(width: abs(size.width), height: abs(size.height))
		}
		return nil
	}
	
}

