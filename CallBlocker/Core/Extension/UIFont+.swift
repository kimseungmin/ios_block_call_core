//
//  UIFont+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

//MARK: - UIFont
extension UIFont {
	var checkIsBold: Bool {
		return fontDescriptor.symbolicTraits.contains(.traitBold)
	}

	var checkIsItalic: Bool {
		return fontDescriptor.symbolicTraits.contains(.traitItalic)
	}
	
	class func systemFont(ofSize fontSize: CGFloat, symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont? {
		return UIFont.systemFont(ofSize: fontSize).including(symbolicTraits: symbolicTraits)
	}

	func including(symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont? {
		var _symbolicTraits = self.fontDescriptor.symbolicTraits
		_symbolicTraits.update(with: symbolicTraits)
		return withOnly(symbolicTraits: _symbolicTraits)
	}

	func excluding(symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont? {
		var _symbolicTraits = self.fontDescriptor.symbolicTraits
		_symbolicTraits.remove(symbolicTraits)
		return withOnly(symbolicTraits: _symbolicTraits)
	}

	func withOnly(symbolicTraits: UIFontDescriptor.SymbolicTraits) -> UIFont? {
		guard let fontDescriptor = fontDescriptor.withSymbolicTraits(symbolicTraits) else { return nil }
		return .init(descriptor: fontDescriptor, size: pointSize)
	}


}

enum FontType { case regular, bold, medium, light, semibold }
extension UIFont {
	static func fontWithName(type: FontType, size: CGFloat) -> UIFont {
		var fontName = ""
		switch type {
			case .regular: fontName = "AppleSDGothicNeo-Regular"
			case .light: fontName = "AppleSDGothicNeo-Light"
			case .medium: fontName = "AppleSDGothicNeo-Medium"
			case .semibold: fontName = "AppleSDGothicNeo-SemiBold"
			case .bold: fontName = "AppleSDGothicNeo-Bold"
				
		}
		return UIFont(name: fontName, size: size) ?? UIFont.systemFont(ofSize: size)
		
	}
	
}

