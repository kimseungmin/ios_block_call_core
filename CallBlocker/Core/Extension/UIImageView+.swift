//
//  UIImageView+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

extension UIImageView {
	/// to use
//		guard let confettiImageView = UIImageView.fromGif(frame: view.frame, resourceName: "confetti") else { return }
//		view.addSubview(confettiImageView)
//		confettiImageView.startAnimating()
	
	/// Repeat and duration customizations using UIImageView APIs.
	//	confettiImageView.animationDuration = 3
	//	confettiImageView.animationRepeatCount = 1
	
	/// When you are done animating the gif and want to release the memory.
	// confettiImageView.animationImages = nil
	static func fromGif(frame: CGRect, resourceName: String) -> UIImageView? {
		guard let path = Bundle.main.path(forResource: resourceName, ofType: "gif") else {
			print("Gif does not exist at that path")
			return nil
		}
		let url = URL(fileURLWithPath: path)
		guard let gifData = try? Data(contentsOf: url),
			let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return nil }
		var images = [UIImage]()
		let imageCount = CGImageSourceGetCount(source)
		for i in 0 ..< imageCount {
			if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
				images.append(UIImage(cgImage: image))
			}
		}
		let gifImageView = UIImageView(frame: frame)
		gifImageView.animationImages = images
		return gifImageView
	}
	
	func load(url: URL) {
		DispatchQueue.global().async { [weak self] in
			if let data = try? Data(contentsOf: url) {
				if let image = UIImage(data: data) {
					DispatchQueue.main.async {
						self?.image = image
					}
				}
			}
		}
	}
	
	func addOverlay(color: UIColor = .black, alpha: CGFloat, w: CGFloat, h: CGFloat) {
	

		let overlay = UIView(frame: CGRect(x: 0, y: 0, width: w, height: h))
		
		overlay.backgroundColor = UIColor.black.withAlphaComponent(alpha)
		overlay.tag = 1
		
		
		if self.viewWithTag(1) != nil {
			self.viewWithTag(1)?.removeFromSuperview()
			
		}
		self.addSubview(overlay)
		
	}
	
	func addBlurEffect(){
		let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
		let blurEffectView = UIVisualEffectView(effect: blurEffect)
		blurEffectView.alpha = 0.7
		blurEffectView.frame = self.bounds

		blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
		self.addSubview(blurEffectView)
	}
	
	func removeBlurEffect() {
		let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
		blurredEffectViews.forEach{ blurView in
			blurView.removeFromSuperview()
			
		}
	}
	

}

//MARK: - UIImage
extension UIImage {

	public enum DataUnits: String {
		case byte, kilobyte, megabyte, gigabyte
	}

	func getSizeIn(_ type: DataUnits)-> Float {

		guard let data = self.jpegData(compressionQuality: 0.9) else {
			return 0.00
		}

		var size: Double = 0.0

		switch type {
		case .byte:
			size = Double(data.count)
		case .kilobyte:
			size = Double(data.count) / 1000
		case .megabyte:
			size = Double(data.count) / 1000 / 1000
		case .gigabyte:
			size = Double(data.count) / 1000 / 1000 / 1000
		}
		let string = String(format: "%.2f", size)
		let floatValue = Float(NSString(string: string).floatValue)
		return floatValue
	}
	
//    func maskWithColor(color: UIColor) -> UIImage? {
//        let maskImage = cgImage!
//
//        let width = size.width
//        let height = size.height
//        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
//
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
//        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
//
//        context.clip(to: bounds, mask: maskImage)
//        context.setFillColor(color.cgColor)
//        context.fill(bounds)
//
//        if let cgImage = context.makeImage() {
//            let coloredImage = UIImage(cgImage: cgImage)
//            return coloredImage
//        }
//        else {
//            return nil
//        }
//    }
}

extension UIImageView {

	func tint(color: UIColor) {
		self.image = self.image?.withRenderingMode(.alwaysTemplate)
		self.tintColor = color
	}
}
