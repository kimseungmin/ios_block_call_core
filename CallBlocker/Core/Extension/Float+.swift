//
//  Float+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

extension Float{
	/// toMB
	func mbFloatToString() -> String{
		let value = String(format: "%.2f",  self)
		return value
	}
	func floatToMbFloat() -> Float{
		let string = self.mbFloatToString()
		
		return (string as NSString).floatValue
		
	}
	
	func floatMBtoByte() -> Int{
		return Int(self * 1000 * 1000)
	}
}

