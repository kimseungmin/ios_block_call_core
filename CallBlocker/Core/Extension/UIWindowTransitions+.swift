import Foundation
import UIKit

public extension UIWindow {
    
    /// Transition Options
    struct TransitionOptions {
        
        /// Curve of animation
        ///
        /// - linear: linear
        /// - easeIn: ease in
        /// - easeOut: ease out
        /// - easeInOut: ease in - ease out
        public enum Curve {
            case linear
            case easeIn
            case easeOut
            case easeInOut
            
            /// Return the media timing function associated with curve
            internal var function: CAMediaTimingFunction {
                let key: String!
                switch self {
                case .linear:        key = CAMediaTimingFunctionName.linear.rawValue
                case .easeIn:        key = CAMediaTimingFunctionName.easeIn.rawValue
                case .easeOut:        key = CAMediaTimingFunctionName.easeOut.rawValue
                case .easeInOut:    key = CAMediaTimingFunctionName.easeInEaseOut.rawValue
                }
                return CAMediaTimingFunction(name: CAMediaTimingFunctionName(rawValue: key!))
            }
        }
        
        /// Direction of the animation
        ///
        /// - fade: fade to new controller
        /// - toTop: slide from bottom to top
        /// - toBottom: slide from top to bottom
        /// - toLeft: pop to left
        /// - toRight: push to right
        public enum Direction {
            case fade
            case toTop
            case toBottom
            case toLeft
            case toRight
            
            /// Return the associated transition
            ///
            /// - Returns: transition
            internal func transition() -> CATransition {
                let transition = CATransition()
                transition.type = CATransitionType.push
                switch self {
                case .fade:
                    transition.type = CATransitionType.fade
                    transition.subtype = nil
                case .toLeft:
                    transition.subtype = CATransitionSubtype.fromLeft
                case .toRight:
                    transition.subtype = CATransitionSubtype.fromRight
                case .toTop:
                    transition.subtype = CATransitionSubtype.fromTop
                case .toBottom:
                    transition.subtype = CATransitionSubtype.fromBottom
                }
                return transition
            }
        }
        
        /// Background of the transition
        ///
        /// - solidColor: solid color
        /// - customView: custom view
        public enum Background {
            case solidColor(_: UIColor)
            case customView(_: UIView)
        }
        
        public enum Duration:TimeInterval {
            case main = 0.2
        }
        
        
        /// Direction of the transition (default is `toRight`)
        public var direction: TransitionOptions.Direction = .toRight
        
        /// Style of the transition (default is `linear`)
        public var style: TransitionOptions.Curve = .linear
        
        /// Background of the transition (default is `nil`)
        public var background: TransitionOptions.Background? = nil
        
        /// Duration of the animation (default is 0.20s)
        public var duration: TransitionOptions.Duration = .main
        
        
        /// Initialize a new options object with given direction and curve
        ///
        /// - Parameters:
        ///   - direction: direction
        ///   - style: style
        public init(direction: TransitionOptions.Direction = .toRight,
                    style: TransitionOptions.Curve = .linear,
                    duration: TransitionOptions.Duration = .main) {
            
            self.direction = direction
            self.style = style
            self.duration = duration
        }
        
        public init() { }
        
        /// Return the animation to perform for given options object
        internal var animation: CATransition {
            let transition = self.direction.transition()
            transition.duration = self.duration.rawValue
            transition.timingFunction = self.style.function
            return transition
        }
    }
    
    enum OveralType {
        case fullscreen
        case overal
    }
    
    /// Change the root view controller of the window
    ///
    /// - Parameters:
    ///   - controller: controller to set
    ///   - options: options of the transition
    
	func setRootNavVC(_ controller: UIViewController,setNavHidden:Bool=true , options: TransitionOptions = TransitionOptions()) {
        
        let currentRoot = self.rootViewController

        if currentRoot != nil && options.direction != .fade{
            let width = currentRoot?.view.frame.size.width
            let height = currentRoot?.view.frame.size.height

            var previousFrame:CGRect?
            var nextFrame:CGRect?

            switch options.direction {
            case .toLeft:
                previousFrame = CGRect(x: -width!+1.0, y: 0.0, width: width!, height: height!)
                nextFrame = CGRect(x: width!, y: 0.0, width: width!, height: height!)
            case .toRight:
                previousFrame = CGRect(x: width!-1.0, y: 0.0, width: width!, height: height!)
                nextFrame = CGRect(x: -width!, y: 0.0, width: width!, height: height!)

            case .toTop:
                previousFrame = CGRect(x: 0.0, y: height!-1.0, width: width!, height: height!)
                nextFrame = CGRect(x: 0.0, y: -height!+1.0, width: width!, height: height!)
            case .toBottom:
                previousFrame = CGRect(x: 0.0, y: height!+1.0, width: width!, height: height!)
                nextFrame = CGRect(x: 0.0, y: height!-1.0, width: width!, height: height!)
            case .fade:
                break

            }
            controller.view.frame = previousFrame!
            
            self.addSubview(controller.view)

            UIView.animate(withDuration: options.duration.rawValue,
                animations: { () -> Void in
                controller.view.frame = (currentRoot?.view.frame)!
                    currentRoot?.view.frame = nextFrame!

                })
                { (fihish:Bool) -> Void in
                    let nav = UINavigationController(rootViewController: controller)

                    self.rootViewController = nav
                    controller.navigationController?.setNavigationBarHidden(setNavHidden, animated: false)
                    controller.navigationController?.interactivePopGestureRecognizer!.delegate = controller as? UIGestureRecognizerDelegate
                    controller.navigationController?.interactivePopGestureRecognizer!.isEnabled = true
                    controller.view.clipsToBounds = true

                }
        }else{
             // Make animation
             self.layer.add(options.animation, forKey: kCATransition)

             let nav = UINavigationController(rootViewController: controller)
             self.rootViewController = nav

             controller.navigationController?.setNavigationBarHidden(setNavHidden, animated: true)
             controller.navigationController?.interactivePopGestureRecognizer!.delegate = controller as? UIGestureRecognizerDelegate
             controller.navigationController?.interactivePopGestureRecognizer!.isEnabled = true
             controller.view.clipsToBounds = true


        }
    

    }
    

    func setRootVC(_ controller: UIViewController, options: TransitionOptions = TransitionOptions()) {
        
        // Make animation
        self.layer.add(options.animation, forKey: kCATransition)
        self.rootViewController = controller

    }
    
    /**
     if (overalType == .over) { transitionStyle will set .crossDisolve automatically}
     */
    func presentVcOveral(_ controller: UIViewController, overalType: OveralType = .fullscreen, options: TransitionOptions = TransitionOptions()){
        
        self.layer.add(options.animation, forKey: kCATransition)
        if overalType == .overal{
            controller.definesPresentationContext = false
            controller.modalPresentationStyle = .overCurrentContext
        }
        if options.direction == .fade{
            
            controller.modalTransitionStyle = .crossDissolve
        }
        
        self.rootViewController!.present(controller, animated: true)
        
    }
	func presentVC(_ controller: UIViewController, type: UIModalPresentationStyle){
        controller.modalPresentationStyle = type//.fullScreen
        UIApplication.topViewController()?.present(controller, animated: true, completion: nil)
    }

    
	/**
	fromTop
	- true = TopVIewController
	- false = rootViewController
	*/
	func pushVC(_ controller: UIViewController, fromTop:Bool ,options: TransitionOptions = TransitionOptions()){
        DispatchQueue.main.async {
        // Make animation
		
			var rootVC:UINavigationController? = nil
			if fromTop{
				rootVC = UIApplication.topViewController() as? UINavigationController
			}
			else{
				rootVC = self.rootViewController as? UINavigationController
			}
			guard let rootViewController = rootVC else {
				fatalError("no UINavigationController")
			}
//			let rootViewController = self.rootViewController as? UINavigationController
	//		let rootViewController = UIApplication.topViewController() as? UINavigationController
			controller.navigationController?.interactivePopGestureRecognizer!.delegate = controller as? UIGestureRecognizerDelegate
			if options.direction == .fade{
				controller.modalTransitionStyle = .crossDissolve
				rootViewController.view.layer.add(options.animation, forKey: kCATransition)
				rootViewController.pushViewController(controller, animated: false)
				controller.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
			}else{
				rootViewController.pushViewController(controller, animated: true)
				controller.navigationController?.interactivePopGestureRecognizer!.isEnabled = true
			}
			
        
        }
            
        
    }
    func popDissolve(){
        
        var option = TransitionOptions()
        option.direction = .fade
        option.duration = .main
        
        let rootViewController = self.rootViewController as? UINavigationController
        rootViewController!.view.layer.add(option.animation, forKey: kCATransition)
        rootViewController!.popViewController(animated: false)
    }
    
}
