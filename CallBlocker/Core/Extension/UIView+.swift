//
//  UIView+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

extension UIView {
	/// view.makeSnapshot()
	
//	func makeSnapshot() -> UIImage? {
//		UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
//		drawHierarchy(in: bounds, afterScreenUpdates: true)
//		let image = UIGraphicsGetImageFromCurrentImageContext()
//		UIGraphicsEndImageContext()
//		return image
//	}
	
	func makeSnapshot() -> UIImage {

		// Begin context
		UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)

		// Draw view in that context
		drawHierarchy(in: self.bounds, afterScreenUpdates: true)

		// And finally, get image
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		if (image != nil)
		{
			return image!
		}
		return UIImage()
	}
}

//MARK:- UIView
extension UIView {
	
	private static let lineDashPattern: [NSNumber] = [3, 3]
	private static let lineDashWidth: CGFloat = 1.0

	func makeDashedBorderLine() {
		let path = CGMutablePath()
		let shapeLayer = CAShapeLayer()
		shapeLayer.lineWidth = UIView.lineDashWidth
		shapeLayer.strokeColor = UIColor(red: 33.0 / 255.0, green: 37.0 / 255.0, blue: 41.0 / 255.0, alpha: 0.5).cgColor//UIColor(hexString: "#212529").cgColor
		shapeLayer.lineDashPattern = UIView.lineDashPattern
		path.addLines(between: [CGPoint(x: bounds.minX, y: bounds.height/2),
								CGPoint(x: bounds.maxX, y: bounds.height/2)])
		shapeLayer.path = path
		layer.addSublayer(shapeLayer)
	}
	
	func outline(borderWidth: CGFloat, borderColor: UIColor, opacity: CGFloat = 1.0) {

		self.layer.borderWidth = borderWidth
		self.layer.borderColor = borderColor.withAlphaComponent(opacity).cgColor
	}
	
	
	func toCornerRound(corners: UIRectCorner = .allCorners, radius: CGFloat, borderColor: UIColor = .clear, borderWidth: CGFloat = 0.0) {
		if corners == .allCorners{
			   self.layer.cornerRadius = radius
			   self.layer.masksToBounds = true
			   self.outline(borderWidth: borderWidth, borderColor: borderColor)
		   }else{
			   let path = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))

			   let mask = CAShapeLayer()
			   mask.path = path.cgPath
			   self.layer.mask = mask

			   let borderPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
			   let borderLayer = CAShapeLayer()
			   borderLayer.path = borderPath.cgPath
			   borderLayer.lineWidth = borderWidth
			   borderLayer.strokeColor = borderColor.cgColor
			   borderLayer.fillColor = UIColor.clear.cgColor
			   borderLayer.frame = self.bounds
			   self.layer.addSublayer(borderLayer)
		}
		 
	}
	
	
	func toCircle() {
		self.layer.cornerRadius = self.frame.size.width / 2
		self.layer.masksToBounds = true
	}
	
	func toCircle(borderWidth: CGFloat, borderColor: UIColor, opacity: CGFloat = 1.0) {
		self.toCircle()
		self.outline(borderWidth: borderWidth, borderColor: borderColor, opacity: opacity)
	}
	

	
	func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat, corners: UIRectCorner, fillColor: UIColor = .white, borderColor: UIColor = .clear, borderWidth: CGFloat = 0.0, borderOpacity: CGFloat = 1.0) {
		
		let shadowLayer = CAShapeLayer()
		let size = CGSize(width: cornerRadius, height: cornerRadius)
		let cgPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath //1
		shadowLayer.name = "ShadowLayer"
		shadowLayer.path = cgPath //2
		shadowLayer.fillColor = fillColor.cgColor //3
		shadowLayer.shadowColor = shadowColor.cgColor //4
		shadowLayer.shadowPath = cgPath
		shadowLayer.shadowOffset = offSet //5
		shadowLayer.shadowOpacity = opacity
		shadowLayer.shadowRadius = shadowRadius
		shadowLayer.borderWidth = borderWidth
		shadowLayer.borderColor = borderColor.withAlphaComponent(borderOpacity).cgColor
		self.backgroundColor = .clear
		self.layer.sublayers?.first { $0.name == "ShadowLayer" }?.removeFromSuperlayer()
		self.layer.insertSublayer(shadowLayer, at: 0)
		
	}
	
	
	func findConstraint(layoutAttribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint? {
		if let constraints = superview?.constraints {
			for constraint in constraints where itemMatch(constraint: constraint, layoutAttribute: layoutAttribute) {
				return constraint
			}
		}
		return nil
	}
	
	func itemMatch(constraint: NSLayoutConstraint, layoutAttribute: NSLayoutConstraint.Attribute) -> Bool {
		if let firstItem = constraint.firstItem as? UIView, let secondItem = constraint.secondItem as? UIView {
			let firstItemMatch = firstItem == self && constraint.firstAttribute == layoutAttribute
			let secondItemMatch = secondItem == self && constraint.secondAttribute == layoutAttribute
			return firstItemMatch || secondItemMatch
		}
		return false
	}
	
	func constraint(byIdentifier identifier: String) -> NSLayoutConstraint? {
		return constraints.first(where: { $0.identifier == identifier })
	}
	
	func removeAllSubviews() {
		for view in self.subviews {
			view.removeFromSuperview()
		}
	}
	
	func isScrolling () -> Bool {

	   if let scrollView = self as? UIScrollView {
		   if  (scrollView.isDragging || scrollView.isDecelerating) {
			   return true
		   }
	   }

	   for subview in self.subviews {
		   if ( subview.isScrolling() ) {
			   return true
		   }
	   }
	   return false
	}
	
	@discardableResult
	func topConstraint(equalTo anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.topAnchor.constraint(equalTo:anchor, constant: constant)
		constraint.isActive = true
		return constraint
	}
	
	@discardableResult
	func bottomConstraint(equalTo anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.bottomAnchor.constraint(equalTo:anchor, constant: constant)
		constraint.isActive = true
		return constraint
	}
	
	@discardableResult
	func leadConstraint(equalTo anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.leadingAnchor.constraint(equalTo:anchor, constant: constant)
		constraint.isActive = true
		return constraint
	}
	
	@discardableResult
	func trailConstraint(equalTo anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.trailingAnchor.constraint(equalTo:anchor, constant: constant)
		constraint.isActive = true
		return constraint
	}
	
	@discardableResult
	func centerXConstraint(equalTo anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.centerXAnchor.constraint(equalTo:anchor, constant: constant)
		constraint.isActive = true
		return constraint
	}
	
	@discardableResult
	func centerYConstraint(equalTo anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.centerYAnchor.constraint(equalTo:anchor, constant: constant)
		constraint.isActive = true
		return constraint
	}
	
	@discardableResult
	func widthConstraint(equalToConstant constant: CGFloat) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.widthAnchor.constraint(equalToConstant: constant)
		constraint.isActive = true
		return constraint
	}
	
	@discardableResult
	func widthConstraint(equalTo anchor: NSLayoutDimension, constant: CGFloat = 0) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.widthAnchor.constraint(equalTo: anchor, constant: constant)
		constraint.isActive = true
		return constraint
	}

	@discardableResult
	func heightConstraint(equalToConstant constant: CGFloat) -> NSLayoutConstraint{
		self.translatesAutoresizingMaskIntoConstraints = false
		let constraint = self.heightAnchor.constraint(equalToConstant: constant)
		constraint.isActive = true
		return constraint
	}
	
	// view 절대 위치
	var absoluteFrame: CGRect? {
		let rootView = UIWindow.key?.rootViewController?.view//UIApplication.keyWindow()?.rootViewController?.view
		return self.superview?.convert(self.frame, to: rootView)
	}
	

	func setRotate(angle: CGFloat) {
		let radians = angle / 180.0 * CGFloat.pi
		let rotation = self.transform.rotated(by: radians);
		self.transform = rotation
	}
	
	func setGradientBackground(colorTop: UIColor, colorBottom: UIColor){
		let gradientLayer = CAGradientLayer()
		gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
		gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
		gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
		gradientLayer.locations = [0, 1]
		gradientLayer.frame = bounds

		layer.insertSublayer(gradientLayer, at: 0)
	}
	
}



extension UIView {
	var parentViewController: UIViewController? {
		var parentResponder: UIResponder? = self
		while parentResponder != nil {
			parentResponder = parentResponder!.next
			if let viewController = parentResponder as? UIViewController {
				return viewController
			}
		}
		return nil
	}
}

