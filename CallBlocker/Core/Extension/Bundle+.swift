//
//  Bundle+.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/05.
//

import Foundation

extension Bundle {
	static func appName() -> String {
		guard let dictionary = Bundle.main.infoDictionary else {
			return ""
		}
		if let version : String = dictionary["CFBundleName"] as? String {
			return version
		} else {
			return ""
		}
	}
}

