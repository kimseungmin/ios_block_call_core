//
//  UIApplication+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

extension UIApplication {

	class func topViewController(base: UIViewController? = UIWindow.key?.rootViewController) -> UIViewController? {

		if let nav = base as? UINavigationController {
			return topViewController(base: nav.visibleViewController)

		} else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
			return topViewController(base: selected)

		} else if let presented = base?.presentedViewController {
			return topViewController(base: presented)
		}
		return base
	}
	
//	class func keyWindow() -> UIWindow?{
//
//		if #available(iOS 13.0, *) {
//			let keyWindow = UIApplication.shared.connectedScenes
//					.filter({$0.activationState == .foregroundActive})
//					.map({$0 as? UIWindowScene})
//					.compactMap({$0})
//					.first?.windows
//					.filter({$0.isKeyWindow}).first
//
//			return keyWindow
//		}
//		else{
//			return UIApplication.shared.keyWindow
//		}
//
//
//
//	}
	
}

extension UIWindow {
	static var key: UIWindow? {
		if #available(iOS 13, *) {
			return UIApplication.shared.windows.first { $0.isKeyWindow }
		} else {
			return UIApplication.shared.keyWindow
		}
	}
}

public extension UIApplication {

	func clearLaunchScreenCache() {
		do {
			try FileManager.default.removeItem(atPath: NSHomeDirectory()+"/Library/SplashBoard")
		} catch {
			print("Failed to delete launch screen cache: \(error)")
		}
	}

}
