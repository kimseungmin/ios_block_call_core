//
//  UILabel+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

extension UILabel {
	func setLetterSpacing(spacing: CGFloat, underline: Bool? = false){
		let attributedString = NSMutableAttributedString(string: self.text!)
		attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: self.text!.count))
		
		if underline == true{
			attributedString.addAttribute(NSAttributedString.Key.underlineStyle,
										  value: NSUnderlineStyle.single.rawValue,
										  range: NSRange(location: 0, length: self.text!.count))
		}else{
			
			attributedString.removeAttribute(NSAttributedString.Key.underlineStyle, range: NSRange(location: 0, length: self.text!.count))
		}
		
		self.attributedText = attributedString
	}
}


extension UILabel {
	func customLabel(string: String, withColor: UIColor?, underLine:Bool, bold:Bool) {
		
		let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
		
		attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: withColor!, range: (text! as NSString).range(of:string))
		
		
		if underLine == true{
			attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.thick.rawValue, range: (text! as NSString).range(of:string))
			attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: withColor!, range: (text! as NSString).range(of:string))
		}
		
		if bold == true{

			let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
			let range = (string as NSString).range(of: string)
			attributedString.addAttributes(boldFontAttribute, range: range)
		}
		
		
		
		
		self.attributedText = attributedString
		
	}
}
