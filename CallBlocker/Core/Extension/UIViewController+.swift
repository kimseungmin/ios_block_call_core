//
//  UIViewController.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

extension UIViewController {
	@objc open func showAlert(_ notification: Notification) {
		guard let userInfo = notification.userInfo else {
			return
		}

		let title = userInfo["title"] as! String
		let msg = userInfo["msg"] as! String
		
		let alertAction = UIAlertAction(title: "confirm", style: .default)
		UIAlertController.showAlert(title: title, message: msg, actions: [alertAction])
		
	}
	
	/// modal or present 여부
	var isModal: Bool {
		if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 {
			return false
		} else if presentingViewController != nil {
			return true
		} else if navigationController?.presentingViewController?.presentedViewController == navigationController {
			return true
		} else if tabBarController?.presentingViewController is UITabBarController {
			return true
		} else {
			return false
		}
		/**
		 if self.isModal == true{
			 self.dismiss(animated: true, completion: nil)
		 }else{
			 self.navigationController?.popViewController(animated: true)
		 }
		 */
	}
	
	// dismiss with count
	func dismissVC(withCount: Int, animate:Bool! = true) {
		guard let navigationController = navigationController else {
			for _ in 1...withCount{
				presentingViewController?.dismiss(animated: animate, completion: nil)
			}
			return
		}
		let viewControllers = navigationController.viewControllers
		let index = withCount + 1
		if viewControllers.count >= index {
			navigationController.popToViewController(viewControllers[viewControllers.count - index], animated: animate)
		}
	}
}


extension UIViewController {
	func topMostViewController() -> UIViewController {
		if self.presentedViewController == nil {
			return self
		}
		if let navigation = self.presentedViewController as? UINavigationController {
			return navigation.visibleViewController!.topMostViewController()
		}
		if let tab = self.presentedViewController as? UITabBarController {
			if let selectedTab = tab.selectedViewController {
				return selectedTab.topMostViewController()
			}
			return tab.topMostViewController()
		}
		return self.presentedViewController!.topMostViewController()
	}
}

