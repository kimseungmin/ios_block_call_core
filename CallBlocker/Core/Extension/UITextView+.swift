//
//  UITextView+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

//MARK: - TextView Extension
extension UITextView {
//	func setLetterSpacing(spacing: CGFloat, underline: Bool? = false){
//        let attributedString = NSMutableAttributedString(string: self.text!)
//        attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: self.text!.count))
//
//        if underline == true{
//            attributedString.addAttribute(NSAttributedString.Key.underlineStyle,
//                                          value: NSUnderlineStyle.single.rawValue,
//                                          range: NSRange(location: 0, length: self.text!.count))
//        }else{
//
//            attributedString.removeAttribute(NSAttributedString.Key.underlineStyle, range: NSRange(location: 0, length: self.text!.count))
//        }
//
//        self.attributedText = attributedString
//    }
	
//    func setLetterSpacing(spacing: CGFloat){
//        if let textValue = text, textValue.count > 0 {
//            let attributedString = NSMutableAttributedString(string: textValue)
//			attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: attributedString.length - 1))
//            attributedText = attributedString
//        }
//    }
}
//MARK: TextView Place Holder
extension UITextView :UITextViewDelegate
{

	/// Resize the placeholder when the UITextView bounds change
	override open var bounds: CGRect {
		didSet {
			self.resizePlaceholder()
		}
	}

	/// The UITextView placeholder text
	public var placeholder: (String?, NSTextAlignment?, CGFloat) {
		get {
			var placeholderText: String?
			
			var align : NSTextAlignment?
			
			var size : CGFloat = 16.0
			
			if let placeholderLabel = self.viewWithTag(100) as? UILabel {
				placeholderText = placeholderLabel.text
				size = placeholderLabel.font.pointSize
				align = placeholderLabel.textAlignment
			}
			

			return (placeholderText, align, size)
		}
		set {
			if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
				placeholderLabel.text = newValue.0
				placeholderLabel.textAlignment = newValue.1 ?? .left
//                placeholderLabel.sizeToFit()
				self.resizePlaceholder(newValue.1)
			} else {
				self.addPlaceholder(newValue.0!, align: newValue.1!, size: newValue.2)
			}
			
		}
	}

	/// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
	///
	/// - Parameter textView: The UITextView that got updated
//    public func textViewDidChange(_ textView: UITextView) {
//        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
//            placeholderLabel.isHidden = self.text.count > 0
//			placeholderLabel.isHidden = self.attributedText.length > 0
//        }
//
//		let text = textView.attributedText.attributeToHtml
//		print(debug: text!, self)
//
//    }

	/// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
	private func resizePlaceholder(_ align:NSTextAlignment? = .left) {
		if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
			let labelX = self.textContainer.lineFragmentPadding
			var labelY:CGFloat = self.textContainerInset.top - placeholderLabel.frame.height
			if let font = self.font {
				labelY = font.lineHeight / 2
			}
//			let labelY = self.font?.lineHeight / 4//self.textContainerInset.top - 2
			let labelWidth = self.frame.width// - (labelX * 2)
			let labelHeight = placeholderLabel.frame.height
			DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
				switch align {
					case .left:
						placeholderLabel.textAlignment = .left
						placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
					case .center:
						placeholderLabel.textAlignment = .center
						placeholderLabel.frame = CGRect(x: -labelX, y: labelY, width: labelWidth, height: labelHeight)
						
							placeholderLabel.center.x = self.center.x - labelX*3
						
						
					case .right:
						placeholderLabel.frame = CGRect(x: self.frame.maxX-labelWidth-(labelX*4), y: labelY, width: labelWidth, height: labelHeight)
					default:
						break
				}
			}
			
		}
	}

	/// Adds a placeholder UILabel to this UITextView
	private func addPlaceholder(_ placeholderText: String, align:NSTextAlignment, size: CGFloat) {
		let placeholderLabel = UILabel()

		placeholderLabel.text = placeholderText
		placeholderLabel.font = UIFont.systemFont(ofSize: size)
		placeholderLabel.sizeToFit()
		placeholderLabel.textAlignment = align

		placeholderLabel.font = self.font
		placeholderLabel.textColor = UIColor.lightGray
		placeholderLabel.tag = 100

		placeholderLabel.isHidden = self.text.count > 0
		placeholderLabel.isHidden = self.attributedText.length > 0

		self.addSubview(placeholderLabel)
		self.resizePlaceholder(align)
//        self.delegate = self
	}
}
