//
//  UINib+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit


//MARK: - UINib Loader
fileprivate extension UINib {
	
	static func nib(named nibName: String) -> UINib {
		return UINib(nibName: nibName, bundle: nil)
	}
	
	static func loadSingleView(_ nibName: String, owner: Any?) -> UIView {
		return nib(named: nibName).instantiate(withOwner: owner, options: nil)[0] as! UIView
	}
}

// MARK: App Views
extension UINib {
	class func loadNib(_ owner: AnyObject) -> UIView {
		return loadSingleView(NSStringFromClass(owner.classForCoder).components(separatedBy: ".").last!, owner: owner)
	}
	
//    class func loadPlayerScoreboardMoveEditorView(_ owner: AnyObject) -> UIView {
//        return loadSingleView("PlayerScoreboardMoveEditorView", owner: owner)
//    }
}


