//
//  URL+.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

extension URL {
	var attributes: [FileAttributeKey : Any]? {
		do {
			return try FileManager.default.attributesOfItem(atPath: path)
		} catch let error as NSError {
			print("FileAttribute error: \(error)")
		}
		return nil
	}

	var fileSize: UInt64 {
		return attributes?[.size] as? UInt64 ?? UInt64(0)
	}
	
	var fileSizeFloat: Float {
		let mb = self.fileSizeString
		let float = Float(NSString(string: mb).floatValue) / 1000 / 1000
		return float
	}

	var fileSizeString: String {
		return ByteCountFormatter.string(fromByteCount: Int64(fileSize), countStyle: .file)
	}

	var creationDate: Date? {
		return attributes?[.creationDate] as? Date
	}
	
}

extension URL {

	/**
	if let url:URL = URL(string: item){
		print(url.parameters["v"])
	}
	*/
	public var parameters: [String: Any?] {
		var dic: [String: Any?] = [:]
		
		guard let components = URLComponents(url: self, resolvingAgainstBaseURL: false) else { return dic }
		guard let queryItems = components.queryItems else { return dic }
		
		for item in queryItems {
			dic[item.name] = item.value
		}
		return dic
	}
}
