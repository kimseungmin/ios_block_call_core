

import Foundation

// Debug용 print함수
func print(debug: Any = "", _ target: Any? = nil, isSimple:Bool = false, file: String = #file, line: Int = #line, function: String = #function ) {
	let date = Date()
	let formatter = DateFormatter()
	formatter.dateFormat = "HH:mm:ss.SSSS"
	let timestamp = formatter.string(from: date)
	
	var filename: NSString = file as NSString
	filename = filename.lastPathComponent as NSString
	
	let className: String = (String.init(describing: target).components(separatedBy: ".").last?.components(separatedBy: ":").first ?? "").replacingOccurrences(of: ")", with: "")
	
	
//	let firstLine: String = "|⏱ timestamp: \(timestamp)"
	let secondLine: String = "|🔑 Class: \(className) | 🔨 Function: \(function)"
	
	var title = ""
	var logString: String = ""
	title = "|📌 [\(filename)](\(line)) | ⏱ TimeStamp: \(timestamp)"
	if isSimple{
		logString = "\(debug)"
		if logString == ""{
			logString += "Function: \(function)"
		}
	}
	else{
		logString = "\(secondLine)\n\(debug)"
	}
	
	let endLineString = String(repeating: "—", count: title.count + 3)// "————————————————————————————————————————"
	
//	for _ in 0...(title.count + 3){
//		endLineString += "—"
//	}
#if DEBUG

	if isSimple{
		print(endLineString)
		print(title + " | " + logString)
		print(endLineString)
	}
	else{
		print(endLineString)
		print(title)
		print(logString)
		print(endLineString)
	}
#else
//    MobileReportLibrary.getInstance().addLogData(logString)
#endif
}
