import Foundation

extension Notification.Name {
    static let refreshInAppPurchase = Notification.Name(rawValue: "refreshInAppPurchase")// IAP
    static let refreshPlayList      = Notification.Name(rawValue: "refreshPlayList")// play list 복원
	static let refreshLibrary       = Notification.Name(rawValue: "refreshLibrary")// play list 복원
    static let test                 = Notification.Name(rawValue: "test")
    static let moveToolBar          = Notification.Name(rawValue: "moveToolBar")
    static let moveMoreBar          = Notification.Name(rawValue: "moveMoreBar")
	
    static let updateFeaturedList   = Notification.Name(rawValue: "updateFeaturedList")// [String: FeaturedListModel]
    static let setNewFeaturedList   = Notification.Name(rawValue: "setNewFeaturedList")// [String: FeaturedListModel]
	static let removedFeaturedList  = Notification.Name(rawValue: "removedFeaturedList")// [String: [Int]]
	
    static let updateNewSongsList   = Notification.Name(rawValue: "updateNewSongsList")// [String: FeaturedListModel]
    static let setNewSongsList      = Notification.Name(rawValue: "setNewSongsList")// [String: FeaturedListModel]
	static let removedNewSongsList  = Notification.Name(rawValue: "removedNewSongsList")// [String: [Int]]
	
    static let requestGoogleSearch  = Notification.Name(rawValue: "requestGoogleSearch")// [String: FeaturedListModel]

    static let startToolTip         = Notification.Name(rawValue: "startToolTip")// [String: FeaturedListModel]
}



