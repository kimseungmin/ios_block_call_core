//
//  IAPModel.swift
//  twoNumSender
//
//  Created by seungmin kim on 01/06/2020.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
enum RegisteredPurchase: String {
//    case purchase1
//    case purchase2
//    case nonConsumablePurchase
//    case consumablePurchase
//    case nonRenewingPurchase
//    case autoRenewableWeekly
    case autoRenewableMonthly  = "com.remain.callblock.1month"
    case autoRenewableYearly = "om.remain.callblock.year"
}

enum RestoreType{
    case restore
    case verify
    
    var info: (title: String, msg: String){
        switch self {
        case .restore:
            return ("복원완료", "복원이 완료되었습니다")
        case .verify:
            return ("검증 완료", "기다려주셔서 감사합니다")
        }
    }
    
}

