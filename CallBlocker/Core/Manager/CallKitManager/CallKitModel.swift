//
//  CallKitModel.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/07.
//

import Foundation

enum SaveCallKitDataCycleType:Int{
	case first
	case reload
}
struct CallkitGroupName{
	static let groupName = "group.2E7Y3PH5LW.com.remain.CallBlocker"
}
enum CallKitBundles:String {
	
	case bundle1 = "com.remain.CallBlocker.CallKit-EX"
	case bundle2 = "com.remain.CallBlocker.CallKit-EX2"
	case bundle3 = "com.remain.CallBlocker.CallKit-EX3"
	case bundle4 = "com.remain.CallBlocker.CallKit-EX4"
	case bundle5 = "com.remain.CallBlocker.CallKit-EX5"
	case bundle6 = "com.remain.CallBlocker.CallKit-EX6"
	case bundle7 = "com.remain.CallBlocker.CallKit-EX7"
	case bundle8 = "com.remain.CallBlocker.CallKit-EX8"
	case bundle9 = "com.remain.CallBlocker.CallKit-EX9"
	case bundle10 = "com.remain.CallBlocker.CallKit-EX10"
	case bundle11 = "com.remain.CallBlocker.CallKit-EX11"
	case bundle12 = "com.remain.CallBlocker.CallKit-EX12"
}
