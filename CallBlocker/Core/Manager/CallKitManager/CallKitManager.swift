//
//  CallKitManager.swift
//  twoNumSender
//
//  Created by seungmin kim on 02/06/2020.
//  Copyright © 2020 remain. All rights reserved.
//

import Foundation
import CallKit
import SVProgressHUD
import StoreKit


class CallKitManager: NSObject{
    
    static let shared = CallKitManager()
	
	private let bundleArray:[CallKitBundles] = [.bundle1, .bundle2, .bundle3, .bundle4, .bundle5, .bundle6, .bundle7, .bundle8, .bundle9, .bundle10, .bundle11, .bundle12]
//    private var errorCnt = 0
//    private let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
//    private var saveCallKitDataCycle: SaveCallKitDataCycleType = .first
//    private var isSuccessSync: Bool = false
	
    private override init(){}
	
    
}


//MARK: - Method
extension CallKitManager{
	func getBundleArray() -> [CallKitBundles]{
		return self.bundleArray
	}
	/**
	value : 07071 (070 + 두번쨰 번호 앞 두자리)
	item : bundle name (Callkit 번들명)
	saveCallkitData 에서만 사용
	*/
	private func setCallKitPrefix(value:String, bundle:CallKitBundles, suffixCount:Int, success: @escaping() -> Void, failure: @escaping() -> Void){
		var prefixValue = ""
		let suffixValue = String(repeating: "0", count: suffixCount)//(suffixRange.replacingOccurrences(of: "-", with: "")).replacingOccurrences(of: "X", with: "0")
		
		if value.hasPrefix("0"){
			prefixValue = "82" + value.dropFirst() + suffixValue
		}
		else{
			prefixValue = "82" + value + suffixValue
		}
		
		if let userDefaults = UserDefaults(suiteName: CallkitGroupName.groupName){
			if value == ""{
				let postDict:[String:Int] = ["number":1,
											 "suffix":suffixCount]
				userDefaults.set(postDict, forKey: bundle.rawValue)
				userDefaults.synchronize()
			}
			else{
				if let expectValue = Int(prefixValue){
					let postDict:[String:Int] = ["number":expectValue,
												 "suffix":suffixCount]
					userDefaults.set(postDict, forKey: bundle.rawValue)
					userDefaults.synchronize()
				}
				
			}
			
			success()
		}else{
			print(debug: "cannot create userdefault json", self)
			failure()
			
		}
	}
	
	/**
	value : 07071 (070 + 두번쨰 번호 앞 두자리)
	item : bundle name (Callkit 번들명)
	*/
	public func saveCallkitData(value:String, bundle:CallKitBundles, suffixCount:Int, success: @escaping() -> Void, failure: @escaping() -> Void){
		SVProgressHUD.show()
		
		self.checkCallkitEnable(bundle: bundle, success: {
			self.setCallKitPrefix(value: value, bundle: bundle, suffixCount: suffixCount, success: {
				// user default 저장 완료
				self.reloadCallKitSync(bundle: bundle)
				
				let suffixValue = String(repeating: "0", count: suffixCount)
				let log = "user default 저장 성공 : " + bundle.rawValue + "\n저장 번호" + value + "\nsuffixRange" + suffixValue
				LogManager.shared.write(log, self)
				SVProgressHUD.dismiss()
				success()
			}, failure: {
				// user default 저장 에러
				
				let suffixValue = String(repeating: "0", count: suffixCount)
				let log = "user default 저장 에러 : " + bundle.rawValue + "\n저장 번호" + value + "\nsuffixRange" + suffixValue
				LogManager.shared.write(log, self)
				SVProgressHUD.dismiss()
				failure()
			})
			
		}, failure: { (errType) in
			
			// CALL KIT 권한 체크 FAIL
			print(debug: "CALL KIT 권한 체크 FAIL", self)
			LogManager.shared.write("CALL KIT 권한 체크 FAIL", self)
			let title = TextInfo.alert_title
			var msg = ""
			
			switch errType{
				
				case .unknown:
					msg = "발신번호 표시 기능 설정 중 입니다\n최초 앱 설치 시 최대 1시간까지 소요될 수 있습니다\n잠시 후 다시 시도 해주세요"
				case .disabled:
					msg = "발신번호 표시 권한을 확인해주세요"
				case .enabled:
					break
				@unknown default:
					msg = "발신번호 표시 기능 설정 중 입니다\n최초 앱 설치 시 최대 1시간까지 소요될 수 있습니다\n잠시 후 다시 시도 해주세요"
			}
			
			let okAction = UIAlertAction(title: TextInfo.alert_action_confirm, style: .default, handler: {action in
				self.openSettingAlert()
			})
			UIAlertController.showAlert(title: title, message: msg, actions: [okAction])
			SVProgressHUD.dismiss()
			failure()
		})
	}
	
	/**
	Call kit 허용 여부 체크
	*/
	func checkCallkitEnable(bundle: CallKitBundles, success: @escaping() -> Void, failure: @escaping(CXCallDirectoryManager.EnabledStatus) -> Void){
		// 최초 1회 userdefault 적용
		if let userDefaults = UserDefaults(suiteName: CallkitGroupName.groupName){
			if userDefaults.integer(forKey: bundle.rawValue) == 0{
				userDefaults.set(1, forKey: bundle.rawValue)
				userDefaults.synchronize()
			}
			else{
				
			}
			
		}
		let callDirManager = CXCallDirectoryManager.sharedInstance
		callDirManager.getEnabledStatusForExtension(withIdentifier: bundle.rawValue, completionHandler: { (enableStatus, error) in
			if let error = error{
				
				print(debug: error.localizedDescription, self, isSimple: true)
				LogManager.shared.write("권한체크 성공 : enabled" + bundle.rawValue, self)
				failure(.unknown)
				
			}
			else{
				switch enableStatus{
					case .disabled:
						failure(.disabled)
						LogManager.shared.write("권한체크 실패 : disabled" + bundle.rawValue, self)
						break
					case .enabled:
						success()
						break
					case .unknown:
						failure(.unknown)
						LogManager.shared.write("권한체크 실패 : unknown" + bundle.rawValue, self)
						break
					@unknown default:
						failure(.unknown)
						LogManager.shared.write("권한체크 실패 : unknown" + bundle.rawValue, self)
						fatalError("CallKit 권한체크 unknown 에러")
						break
				}
			}
			
		})
	
    }

	/**
	실제 데이터 저장 처리
	*/
	func reloadCallKitSync(bundle:CallKitBundles){
//		let okAction = UIAlertAction(title: TextInfo.alert_action_confirm, style: .default, handler: {action in
//		})
//		UIAlertController.showAlert(title: TextInfo.alert_title, message: "동기화가 시작되었습니다\n완료 시 푸시알람이 발송됩니다", actions: [okAction])
		
		print(debug: "Save Call Kit Started", self)
        let callDirManager = CXCallDirectoryManager.sharedInstance
		callDirManager.reloadExtension(withIdentifier: bundle.rawValue, completionHandler: { (error) in
			SVProgressHUD.dismiss()
            if let error = error{
                
                let errCode = (error as NSError).code
                LogManager.shared.write(error.localizedDescription , self)

                let title = TextInfo.alert_title
                var msg = "동기화 에러가 발생하였습니다.\n앱 종료 후 다시 시도 해 주세요."
                if errCode == 4097{
                    print(debug: "보조 응용 프로그램과 통신할 수 없습니다.", self)
                }
                let callKitErrCode:CallKitErrorCode = CallKitErrorCode(rawValue: errCode) ?? .unknown
                switch callKitErrCode {
                    
                    case .unknown:
                    msg = "동기화 에러가 발생하였습니다.\n앱 종료 후 다시 시도 해 주세요."
                    case .noExtensionFound:
                    msg = "발신자 표시 기능이 확인되지 않습니다"
                    case .loadingInterrupted:
                    msg = "동기화가 강제종료 되었습니다\n앱 종료 후 다시 시도해 주세요"
                    case .entriesOutOfOrder:
                    msg = "연락처 동기 이상이 발생하여 중지되었습니다"
                    case .duplicateEntries:
                    msg = "중복된 연락처가 존재합니다"
                    case .maximumEntriesExceeded:
                    msg = "최대 등록가능 개수를 초과하였습니다"
                    case .extensionDisabled:
                    msg = "발신자 표시 기능이 비 활성화 되었습니다"
                    case .currentlyLoading:
                    msg = "차단기능 활성화 중 입니다\n사용환경에 따라 최대 1시간까지 소요될 수 있습니다\n앱을 강제종료하는 경우 설정이 정상적으로 이루어지지 않을 수 있습니다"
                    case .unexpectedIncrementalRemoval:
                    msg = "예기치 않은 오류가 발생했습니다"
                }
				
				print(debug: "\(msg) + code : \(errCode)", self)
                let okAction = UIAlertAction(title: TextInfo.alert_cancel, style: .default, handler: {action in
                    LogManager.shared.write(error.localizedDescription + "\n" + msg , self)
                })
                UIAlertController.showAlert(title: title, message: msg, actions: [okAction])

            }else{
//				let okAction = UIAlertAction(title: TextInfo.alert_action_confirm, style: .default, handler: {action in
//				})
//				UIAlertController.showAlert(title: TextInfo.alert_title, message: "설정 완료되었습니다", actions: [okAction])
                print(debug: "Save Call Kit Success", self)
            }
        })
		
		
	}
	
}


//MARK:- Alert
extension CallKitManager{
    /*설정 alert*/
    func openSettingAlert(){
        let title = "설정이 필요합니다"
        let msg = "[설정 -> 전화 -> 전화 차단 및 발신자 확인]에서 설정을 킨 후 전체 동기화를 해주세요"
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
		
		
		if #available(iOS 13.4, *) {
			alert.addAction(UIAlertAction(title: "이동하기", style: .default) { action in
				self.moveToSetting()
			})
			
		}
		
        
//        alert.addAction(UIAlertAction(title: "이동하기", style: .default) { action in
//
//            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
//                return
//            }
//
//            if UIApplication.shared.canOpenURL(settingsUrl) {
//                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                    print(debug: "Settings opened: \(success)", self)
//                })
//            }
//
//        })
        
        alert.addAction(UIAlertAction(title: TextInfo.alert_cancel, style: .destructive) { action in
            
        })
        
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        
    }
	
	func moveToSetting(){
		if #available(iOS 13.4, *) {
			CXCallDirectoryManager.sharedInstance.openSettings(completionHandler: { (error) in
				if let error = error{
					print(debug: "open Setting Error\(error)", self)
				}
			})
		}
	}
}


public enum CallKitErrorCode : Int {

    public typealias _ErrorType = CXErrorCodeCallDirectoryManagerError

    case unknown                       // Code=0

    case noExtensionFound              // Code=1

    case loadingInterrupted            // Code=2

    case entriesOutOfOrder             // Code=3

    case duplicateEntries              // Code=4

    case maximumEntriesExceeded        // Code=5

    case extensionDisabled             // Code=6

    @available(iOS 10.3, *)
    case currentlyLoading              // Code=7

    @available(iOS 11.0, *)
    case unexpectedIncrementalRemoval  // Code=8
}


