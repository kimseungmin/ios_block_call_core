//
//  LogManager.swift
//  twoNumSender
//
//  Created by vincent_kim on 08/10/2019.
//  Copyright © 2019 remain. All rights reserved.
//

import Foundation

class LogManager {
    
    let timeDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy.MM.dd HH:mm:ss"
        
        return formatter
    }()
    
    let fileName: String = "log.txt"
    
    static let shared = LogManager()
    private init() {
        
    }
    
    /// Appends the given string to the stream.
    func write(_ string: String, _ target: Any? = nil, file: String = #file, line: Int = #line, function: String = #function) {
        let userProductInfo = UserDefaults.standard.string(forKey: .premium) ?? " NULL "
        var logString: String = ""
        
        logString += "------------------------------------------------------------------------------\n"
        logString += "userProductInfo = " + userProductInfo + "\n"
        logString += "------------------------------------------------------------------------------\n"
        
        
        var filename: NSString = file as NSString
        filename = filename.lastPathComponent as NSString
        
        let now = Date()
        let timeString = self.timeDateFormatter.string(from: now)
        
        let className = String.init(describing: target).components(separatedBy: ".").last?.components(separatedBy: ":").first ?? ""
        logString += "------------------------------------------------------------------------------\n"
		logString += "|Locale Time: \(timeString) || KR Time: \(now.dateStringKR) \n|Class: \(className)\n|File: \(filename)\n|Line: \(line)\n|Function: \(function)\n|Desc: \(string)\n"
        logString += "------------------------------------------------------------------------------\n"
        
        self.writeToFile(logString)
    }
    
    private func writeToFile(_ string: String) {
        
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let logFile = documentsPath.appendingPathComponent(self.fileName)
        
        do {
            if FileManager.default.fileExists(atPath: logFile.path) {
                let aFileAttributes = try FileManager.default.attributesOfItem(atPath: logFile.path) as [FileAttributeKey:Any]
                let modificationDate = aFileAttributes[FileAttributeKey.modificationDate] as! Date
                let nowDate = Date()
                
                let interval = nowDate.timeIntervalSince(modificationDate)
                let days = Int(interval / 86400)
                if days >= 1 {
                    try FileManager.default.removeItem(at: logFile)
                }
            }
            
            let handle = try FileHandle(forWritingTo: logFile)
            handle.seekToEndOfFile()
            handle.write(string.data(using: .utf8)!)
            handle.closeFile()
        }
        catch {
            do {
                if FileManager.default.fileExists(atPath: logFile.path) {
                    print(debug: error.localizedDescription, self)
                }
                else {
                    try string.data(using: .utf8)?.write(to: logFile)
                }
            }
            catch {
                print(debug: error.localizedDescription, self)
            }
        }
    }
    
    func read() -> String {
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let path = documentsPath.appendingPathComponent(self.fileName).path
        var file:String
        
        if FileManager.default.fileExists(atPath: path) {
            do{
                try file = String.init(contentsOfFile: path, encoding: .utf8)
            }
            catch {
                file = ""
            }
        }
        else {
            file = ""
        }
        
        return file
    }
    
    func getFileData() -> Data? {
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let logFile = documentsPath.appendingPathComponent(self.fileName)
        var dataFile: Data? = nil
        if FileManager.default.fileExists(atPath: logFile.path) {
            
            do {
                dataFile = try Data.init(contentsOf: logFile)
            }
            catch {
                print(debug: error.localizedDescription, self)
            }
        }
        
        return dataFile
    }
    
//    func removeFileData(){
//        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//        let logFile = documentsPath.appendingPathComponent(self.fileName)
//
//        if FileManager.default.fileExists(atPath: logFile.path) {
//            
//            do {
//                try FileManager.default.removeItem(at: logFile)
//                print(debug:"remove File URL: "+logFile.path, self)
//            }
//            catch {
//                print(debug: error.localizedDescription, self)
//            }
//        }
//
//    }
    
    func removeFileData(){
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let path = documentsPath.appendingPathComponent(self.fileName).path
        
        
        if FileManager.default.fileExists(atPath: path) {
            do{
                try FileManager.default.removeItem(atPath: path)
                print(debug:"remove File URL: " + path, self)
            }
            catch {
                
            }
        }
        
        
    }
    
}

