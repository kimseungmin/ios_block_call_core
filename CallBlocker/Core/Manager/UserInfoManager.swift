//
//  UserInfoManager.swift
//  YouTubeSearch
//
//  Created by vincent_kim on 2020/12/27.
//

import Foundation
import UIKit
enum TutorialTypes: String{
	case featured
	case history
	case checkbox
	case list
	case mainPlayer
	case newUser
}
class TutorialModel{
	var type: TutorialTypes
	var status:Bool
	
	init(type: TutorialTypes, status:Bool) {
		self.type = type
		self.status = status
	}
}
public class UserInfoManager {
	let regDateKey = "regDate"
	
	static let shared = UserInfoManager()
	var isLaunchedBefore:Bool = false
	var tutorialInfo: [TutorialModel] = []
	var isNewUser:Bool = false

}

extension UserInfoManager{
	func getTutorialInfos() -> [TutorialModel]{
		return self.tutorialInfo
	}
	
//	func getTurotialIsComplete() -> Bool{
//		var isComplete:Bool = true
//
//		for (key, value) in self.tutorialInfo{
//			if !value{
//				isComplete = false
//			}
//		}
//		return isComplete
//	}
	func setupTutorial(){
//		#warning("지워야함")
//		#if DEBUG
//		return
//		#endif
		
		self.tutorialInfo.append(TutorialModel(type: .featured, status: false))
		self.tutorialInfo.append(TutorialModel(type: .history, status: false))
		self.tutorialInfo.append(TutorialModel(type: .checkbox, status: false))
		self.tutorialInfo.append(TutorialModel(type: .newUser, status: false))
		self.tutorialInfo.append(TutorialModel(type: .list, status: false))
		self.tutorialInfo.append(TutorialModel(type: .mainPlayer, status: false))
			
		
		for value in self.tutorialInfo{
			let userDefault = UserDefaults.standard.bool(forKey: value.type.rawValue)
			value.status = userDefault
		}
		
	}
	
	func setTutorialStatus(type:TutorialTypes, status:Bool){
		self.tutorialInfo.filter( {$0.type == type} ).first?.status = status
		UserDefaults.standard.setValue(status, forKey: type.rawValue)
		
	}
	
	func getTutorialStatus(type:TutorialTypes) -> Bool{
		let type = UserDefaults.standard.bool(forKey: type.rawValue)
		return type
	}
	
	func setIsCallkitSetted(status:Bool){
		UserDefaults.standard.set(status, forKey: .isFirstLoadConnected)
	}
	
	func getIsCallkitSetted() -> Bool{
		return UserDefaults.standard.bool(forKey: .isFirstLoadConnected) ?? false
	}
}

// MARK: - RegDate
extension UserInfoManager{
	func setRegDate(){
		let userDefaults = UserDefaults.standard
		
		#if DEBUG
//		userDefaults.removeObject(forKey: self.regDateKey)
		#endif
		
//		if let purchaseStatus = UserDefaults.standard.string(forKey: .premium){
//			if purchaseStatus == "adstest"{
//				self.isNewUser = false
//				return
//			}
//
//		}
		if let storedDate = userDefaults.object(forKey: self.regDateKey) as? Date {
			print(debug: storedDate.dateStringKR, self, isSimple: false)
			
			let calendar = Calendar(identifier: .gregorian)
			let dayOffset = DateComponents(day: 2)
			if let d3 = calendar.date(byAdding: dayOffset, to: storedDate) {
				print(debug: d3.dateStringKR, self, isSimple: false)
				if d3.isInThePast{
					self.isNewUser = false
				}
				else{
					self.isNewUser = true
					// MARK: Test
//					if storedDate.isInSameDay(as: Date()){
//						self.isNewUser = false
//					}
//					else{
//						self.isNewUser = true
//					}
					
				}
			}
			
		}
		else{
			userDefaults.set(Date(), forKey: self.regDateKey)
			self.isNewUser = true
		}
		
	}
	
	func getIsNewUser() -> Bool{
		return self.isNewUser

	}
	
}
