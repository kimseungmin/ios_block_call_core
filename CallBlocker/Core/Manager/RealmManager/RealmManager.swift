
import Foundation
import Realm
import RealmSwift

extension Results {
    var array: [Element]? {
        return self.count > 0 ? self.map { $0 } : nil
    }

}
extension List {
	var array: [Element]? {
		return self.count > 0 ? self.map { $0 } : nil
	}
	
}

final class RealmManager: RealmUsable {
    static let shared = RealmManager()
    
    func createRealm() -> Realm? {
        do {
            return try Realm(configuration: RealmManager.config)
        } catch let error as NSError {
            print(debug: "realm error: \(error)", self)
//            LogManager.sharedInstance.write("realm error: \(error)", self)
            var config = RealmManager.config
            config.deleteRealmIfMigrationNeeded = true
            return try? Realm(configuration: config)
        }
    }

	
}
//MARK: -
extension RealmManager{
	func getAllNumbers() -> [BlockNumberModel]{
			
		if let realm = self.createRealm(){
			let realmNumberModels = realm.objects(RealmBlockNumberModel.self).sorted(byKeyPath: "seq").array ?? []
			var models:[BlockNumberModel] = []
			for i in realmNumberModels{
				let bundle:CallKitBundles = CallKitBundles(rawValue: i.callKitBundle) ?? .bundle1
				let model = BlockNumberModel(seq: i.seq, blockNumber: i.blockNumber, localNumber: i.localNumber, middleNumber: i.middleNumber, lastNumber: i.lastNumber, callKitBundle: bundle, suffixCount: i.suffixCount)
				model.isDefault = i.isDefault
				
				model.isAct = IAPManager.shared.getPurchaseStatus()
				
				if i.isDefault{
					model.isAct = true
				}
				models.append(model)
			}
			return models
		}else{
			
			return []
		}
		
	}
	
	/**
	최초 로드시에만 체크
	*/
	func createNumbers(){
		if let realm = self.createRealm(){
			let models = self.getAllNumbers()
			// 하나도 없는 경우만 생성
			if models.isEmpty{
				let bundles = CallKitManager.shared.getBundleArray()
//				var newRealmModels:[RealmBlockNumberModel] = []
				for (idx, i) in bundles.enumerated(){
					let realmModel = RealmBlockNumberModel()
					realmModel.seq = realmModel.incrementSeq()
					if idx == 0{
						
						realmModel.blockNumber = "070-7079-5443"
						realmModel.localNumber = "070"
						realmModel.middleNumber = "70"
						realmModel.lastNumber = ""
						realmModel.isDefault = true
						realmModel.suffixCount = 6
					}
					else if idx == 1{
						
						realmModel.blockNumber = "070-7737-0854"
						realmModel.localNumber = "070"
						realmModel.middleNumber = "77"
						realmModel.lastNumber = ""
						realmModel.isDefault = true
						realmModel.suffixCount = 6
					}
					else if idx == 2{
						
						realmModel.blockNumber = "070-7677-0001"
						realmModel.localNumber = "070"
						realmModel.middleNumber = "76"
						realmModel.lastNumber = ""
						realmModel.isDefault = false
						realmModel.suffixCount = 6
					}
					else{
						realmModel.blockNumber = ""
						realmModel.isDefault = false
						realmModel.suffixCount = 2
					}
					realmModel.callKitBundle = i.rawValue
//					newRealmModels.append(realmModel)
					
					do {
						try realm.write{
							realm.add(realmModel)
						}
					}
					catch {
						print(debug: "failed save", self, isSimple: false)
					}
				}
				
				
			}
			
		}
		else{
			print(debug: "realm cannot create", self, isSimple: true)
		}
	}
	
	func getNumber(seq:Int, realm:Realm) -> RealmBlockNumberModel?{
		
		if realm.objects(RealmBlockNumberModel.self).count == 0{
			
			return nil
		}
		
		guard let expectModel:RealmBlockNumberModel = realm.objects(RealmBlockNumberModel.self).filter("seq=\(seq)").first else {
			print(debug: "no content", self)
			
			return nil
		}
		
		return expectModel
	}
	
	func updateNumber(seq: Int, blockNumber:String, localNumber:String, middleNumber:String, lastNumber:String, suffixCount:Int ,completion: @escaping(Bool) -> Void){
		
		guard let realm = self.createRealm() else {
			completion(false)
			return
		}
		
		let prefixNumber:String = localNumber + middleNumber + lastNumber
		let models = self.getAllNumbers()
		for (idx, i) in models.enumerated(){
			let currentPrefix = i.localNumber + i.middleNumber + i.lastNumber
			let startValue = currentPrefix + String(repeating: "0", count: i.suffixCount)
			let endValue = currentPrefix + String(repeating: "9", count: i.suffixCount)
			if let start = Int(startValue), let end = Int(endValue){
				
				let newStartValue = prefixNumber + String(repeating: "0", count: suffixCount)
				let newEndValue = prefixNumber + String(repeating: "0", count: suffixCount)
				if let checkFirstValue = Int(newStartValue), let checkEndValue = Int(newEndValue){
					if ((start == checkFirstValue) || (end == checkEndValue)) && (seq != i.seq){
						var name = ""
						if i.isDefault{
							name = "기본 070 " + "\(idx + 1)번"
						}
						else{
							let count = models.filter({$0.isDefault == true}).count
							name = "사용자 지정 " + "\((idx + 1) - count)번"
						}
					
						let suffixString = String(repeating: "X", count: i.suffixCount)
						let fullText = currentPrefix + suffixString
						
						UIAlertController.showMessage("\(name)에 중복되는 패턴이 저장되어있습니다\n(\(fullText.setPhoneNumber()))\n저장되지 않았습니다")
						completion(false)
						return
					}
					

				}
				
			}
			
		}
		
		
		guard let expectModel = self.getNumber(seq: seq, realm: realm) else {
			completion(false)
			return
		}
		
		realm.beginWrite()
        expectModel.blockNumber  = blockNumber
        expectModel.localNumber  = localNumber
        expectModel.middleNumber = middleNumber
        expectModel.lastNumber   = lastNumber
		expectModel.suffixCount  = suffixCount
		
		do {
			try realm.commitWrite()
			completion(true)
		}
		catch{
			completion(false)
		}
		
	}
	
	func deleteAllNumbers(completion: @escaping (Bool) -> Void){
		do {
			guard let realm = self.createRealm() else {
				completion(false)
				return
			}
			
			let models = realm.objects(RealmBlockNumberModel.self)
			realm.beginWrite()
			
//			for item in models{
//				realm.delete(item)
//			}
			realm.delete(models)
			try realm.commitWrite()
			completion(true)
			
		}
		catch {
			completion(false)
		}
	}
}



//MARK: - Method
extension RealmManager{
	
}


