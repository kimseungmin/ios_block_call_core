
import RealmSwift

enum RealmVersion: UInt64{
	
	/**
	default
	*/
	case v10 = 10
	
	/**
	v11
	+ suffixCount
	- is070Type
	- rangeNumb
	*/
	case v11 = 11
	
	
}

protocol RealmUsable {
    static var config: Realm.Configuration { get }
    func createRealm() -> Realm?
}

extension RealmUsable {
    
    static var config: Realm.Configuration {
        
        // copy file
        let defaultURL = Realm.Configuration.defaultConfiguration.fileURL!
//		print(debug: defaultURL, self)
        if !SystemUtils.shared.fileExists(path: defaultURL.path) {
            if let v0URL = SystemUtils.shared.bundleURL("default", withExtension: "realm") {
                do {
                    try FileManager.default.copyItem(at: v0URL, to: defaultURL)
                    print(debug: "default-v0.realm copy success.", self)
                } catch {
                    print(debug: "default-v0.realm copy failed.\n\(error)", self)
//                    LogManager.sharedInstance.write("default-v0.realm copy failed.\n\(error)", self)
                }
            }
        }
		
		return Realm.Configuration(schemaVersion: RealmVersion.v11.rawValue, migrationBlock: { migration, oldSchemeVersion in
			if (oldSchemeVersion < RealmVersion.v11.rawValue) {
				migration.enumerateObjects(ofType: RealmBlockNumberModel.className()) { oldObject, newObject in

					// copy file
					let defaultURL = Realm.Configuration.defaultConfiguration.fileURL!
					if let v0URL = SystemUtils.shared.bundleURL("default", withExtension: "realm") {
						do {
							try FileManager.default.removeItem(at: defaultURL)
							try FileManager.default.copyItem(at: v0URL, to: defaultURL)
							print(debug: "default-v0.realm copy success.", self)
						} catch {
							print(debug: "default-v0.realm copy failed.\n\(error)", self)

						}
					}
					var suffixCount = 0
				    // combine name fields into a single field
					if (oldObject?["is070Type"] as? Bool ?? true) == true{
						newObject?["suffixCount"] = 6
						suffixCount = 6
						
					}
					else{
						newObject?["suffixCount"] = 2
						suffixCount = 2
					}
					
					if let blockNumber:String = oldObject?["blockNumber"] as? String{
						if !blockNumber.isEmpty{
							let suffixSting = String(repeating: "0", count: suffixCount)
							let result = blockNumber + suffixSting
							newObject?["blockNumber"] = result.setPhoneNumber()
						}
						
					}
				}
//				migration.enumerateObjects(ofType: RealmBlockNumberModel.className()) { oldObject, newObject in
//
//				}
//				migration.enumerateObjects(ofType: RealmBlockNumberModel.className()) { oldObject, newObject in
//
//				}
			}
		})
		
		
        
        
    }
}













