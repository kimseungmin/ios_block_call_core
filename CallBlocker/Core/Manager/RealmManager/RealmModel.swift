
import Foundation
import RealmSwift


class RealmBlockNumberModel: Object{
    @objc dynamic var seq: Int             = 0
    @objc dynamic var blockNumber:String   = ""
	@objc dynamic var callKitBundle:String = ""
	@objc dynamic var localNumber:String   = ""// For UI
    @objc dynamic var middleNumber:String  = ""// For UI
    @objc dynamic var lastNumber:String    = ""// For UI
	@objc dynamic var isDefault:Bool  	   = false // For UI
	@objc dynamic var suffixCount: Int     = 0
	
	
	override static func primaryKey() -> String? {
		return "seq"
	}
	//Incrementa ID
	func incrementSeq() -> Int{
		let realm = try! Realm()
		if let retNext = realm.objects(RealmBlockNumberModel.self).sorted(byKeyPath: "seq").last?.seq {
			return retNext + 1
		}else{
			return 1
		}
	}
}
