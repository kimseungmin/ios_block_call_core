
import Foundation

protocol KeyNamespaceable {
    func namespaced<T : RawRepresentable>(_ key: T) -> String
}

extension KeyNamespaceable {
    func namespaced<T: RawRepresentable>(_ key: T) -> String {
        return "\(Self.self).\(key.rawValue)"
    }
}

protocol StringDefaultSettable : KeyNamespaceable {
    associatedtype StringKey : RawRepresentable
}

extension StringDefaultSettable where StringKey.RawValue == String {
    
    // set
    func set(_ value: Int, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func set(_ value: Float, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func set(_ value: String, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func set(_ value: Bool, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func set(_ value: Any, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    // get
    func int(forKey key: StringKey) -> Int? {
        let keySpaced = namespaced(key)
        guard let _ = self.object(forKey: key) else {
            return nil
        }
        
        return UserDefaults.standard.integer(forKey: keySpaced)
    }
    
    func float(forKey key: StringKey) -> Float? {
        let keySpaced = namespaced(key)
        guard let _ = self.object(forKey: key) else {
            return nil
        }
        
        return UserDefaults.standard.float(forKey: keySpaced)
    }
    
    @discardableResult
    func string(forKey key: StringKey) -> String? {
        let keySpaced = namespaced(key)
        guard let _ = self.object(forKey: key) else {
            return nil
        }
        
        return UserDefaults.standard.string(forKey: keySpaced)
    }
    
    func bool(forKey key: StringKey) -> Bool? {
        let keySpaced = namespaced(key)
        guard let _ = self.object(forKey: key) else {
            return nil
        }
        
        return UserDefaults.standard.bool(forKey: keySpaced)
    }
    
    func object(forKey key: StringKey) -> Any? {
        let keySpaced = namespaced(key)
        return UserDefaults.standard.object(forKey: keySpaced)
    }
}

extension UserDefaults : StringDefaultSettable {
    enum StringKey : String {
		case isFirstLoadConnected		// 콜킷 설정 여부
		case launchedBefore 			// launchedBefore
        case userIdentifier            // apple login userid
		case loginType				   // loginType
		case premium				   // purchased
		case videoQuality			   // videoQuality
		case backgroundPlay			   // backgroundPlay
		case regDate
		//MARK: 1.0.3 버전 인 경우 최초 1회 싱크
		case syncRecentPlayList
		case lastPlayedVideo
    }
}

