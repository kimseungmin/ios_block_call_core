//
//  KeyboardManager.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit

public class KeyboardManager {
	static let shared: KeyboardManager = KeyboardManager()
	
	var measuredSize: CGRect = CGRect.zero

	var addedWindow: UIWindow
	var textfield = UITextField()
	
	var keyboardHeightKnownCallback: () -> Void = {}
	var simulatorTimeout: Timer?

	var duration: Double?
	var curve:UIView.AnimationOptions?
	var startingFrame:CGRect?
	var endingFrame:CGRect?
	
	
	public func setup(_ callback: @escaping () -> Void) {
		guard measuredSize == CGRect.zero else {
			return
		}

		
		self.keyboardHeightKnownCallback = callback
	}
	public func setup() {

		
		guard measuredSize == CGRect.zero else {
			return
		}

		
	}

	private init() {
		addedWindow = UIWindow(frame: UIScreen.main.bounds)
		addedWindow.rootViewController = UIViewController()
		addedWindow.addSubview(textfield)

		self.observeKeyboardNotifications()
		self.observeKeyboard()
	}
	
	private func observeKeyboardNotifications() {
		let center = NotificationCenter.default
//		center.addObserver(self, selector: #selector(self.keyboardWillUp), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
		center.addObserver(self, selector: #selector(self.keyboardChange), name: UIResponder.keyboardDidShowNotification, object: nil)
	}

	private func observeKeyboard() {
		let currentWindow = UIWindow.key

		addedWindow.makeKeyAndVisible()
		textfield.becomeFirstResponder()
		
		currentWindow?.makeKeyAndVisible()

		setupTimeoutForSimulator()
	}
	
//	@objc private func keyboardWillUp(_ notification: Notification) {
//		if MainToolBarManager.shared.getIsEditing() == true{
//			UIView.setAnimationsEnabled(false)
//		}else{
//			UIView.setAnimationsEnabled(true)
//		}
//	}

	@objc private func keyboardChange(_ notification: Notification) {
		textfield.resignFirstResponder()
		textfield.removeFromSuperview()

		guard self.measuredSize == CGRect.zero, let info = notification.userInfo else { return }

		guard let value = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
		guard let duration = info[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
		guard let rawAnimationCurve = (info[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber) else { return }
		let curveValue = rawAnimationCurve.uint32Value << 16
		let curve = UIView.AnimationOptions(rawValue:UInt(curveValue))
		
		let startingFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
		let endingFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

		
		self.saveAnimationOptions(duration: duration, curve: curve, startingFrame: startingFrame, endingFrame: endingFrame)
		saveKeyboardSize(value.cgRectValue)
	}
	
	private func saveAnimationOptions(duration: Double, curve:UIView.AnimationOptions, startingFrame:CGRect, endingFrame:CGRect ){
		
		self.duration = duration
		self.curve = curve
		self.startingFrame = startingFrame
		self.endingFrame = endingFrame
	}
	

	private func saveKeyboardSize(_ size: CGRect) {
		cancelSimulatorTimeout()

		self.measuredSize = size
		keyboardHeightKnownCallback()

	}
	public func getHeight() -> CGFloat {
		return self.measuredSize.height
	}
	
	public func getDuration() -> Double{
		return self.duration ?? 0.25
	}
	
	public func getAnimation() -> UIView.AnimationOptions{
		return self.curve ?? UIView.AnimationOptions.curveLinear
	}
	
	public func getStartingFrame() -> CGRect{
		//MARK: default by iphone x (simulator does not have defualt)
		return self.startingFrame ?? CGRect(x: 0.0, y: 812.0, width: 375.0, height: 291.0)
	}
	
	public func getEndingFrame() -> CGRect{
		//MARK: default by iphone x (simulator does not have defualt)
		return self.endingFrame ?? CGRect(x: 0.0, y: 512.0, width: 375.0, height: 291.0)
	}
	
	
	public func getBottomSafeAreaHeight() -> CGFloat{
		var height:CGFloat = 0.0
		if #available(iOS 11.0, *) {

			let bottomSafeArea = UIApplication.topViewController()?.view.safeAreaInsets.bottom
			height = bottomSafeArea ?? 0.0
			 
		}
		
		return height
	}
	
	private func setupTimeoutForSimulator() {
		#if targetEnvironment(simulator)
		let timeout = 2.0
		simulatorTimeout = Timer.scheduledTimer(withTimeInterval: timeout, repeats: false, block: { (_) in
//			print(" KeyboardSize")
//			print(" .keyboardDidShowNotification did not arrive after \(timeout) seconds.")
//			print(" Please check \"Toogle Software Keyboard\" on the simulator (or press cmd+k in the simulator) and relauch your app.")
//			print(" A keyboard height of 0 will be used by default.")
		  self.saveKeyboardSize(CGRect.zero)
		})
		#endif
	}

	private func cancelSimulatorTimeout() {
		simulatorTimeout?.invalidate()
	}

	deinit {
		print(debug: "", self)
		NotificationCenter.default.removeObserver(self)
	}
	public func isKeyBoardUp() -> Bool{
		return UIApplication.shared.isKeyboardPresented
	}
}


extension UIApplication {
	var isKeyboardPresented: Bool {
		if let keyboardWindowClass = NSClassFromString("UIRemoteKeyboardWindow"), self.windows.contains(where: { $0.isKind(of: keyboardWindowClass) }) {
			return true
		} else {
			return false
		}
	}
}

@nonobjc extension UIView {

	/// Returns first found subview with given class name
	///
	/// - Parameter className: Subview class name
	/// - Returns: First subview with given class name
	func subview(withSuffix className: String) -> UIView? {
		return subviews.first {
			$0.hasClassNameSuffix(className)
		}
	}

	/// Compares suffix of view class name with given name
	///
	/// - Parameter className: Class name that will be compared
	/// - Returns: Comparison result of view class name and given name
	func hasClassNameSuffix(_ className: String) -> Bool {
		return NSStringFromClass(type(of: self)).hasSuffix(className)
	}
}

