//
//  ReachabliltyManager.swift
//  YouTubeSearch
//
//  Created by vincent_kim on 2020/12/31.
//

import Foundation
import Network

enum Reachable {
	case yes, no
}

enum Connection {
	case cellular, loopback, wifi, wiredEthernet, other
}


public class ReachabliltyManager {
	
	private let monitor: NWPathMonitor =  NWPathMonitor()
	private var rechable:Bool = false
	private var status:Connection = .other
	
	static let shared = ReachabliltyManager()
	init() {
		let queue = DispatchQueue.global(qos: .background)
		monitor.start(queue: queue)
	}

}



extension ReachabliltyManager {
	func getCurrentStatus() -> Connection{
		return self.status
	}
	func startMonitoring( callBack: @escaping (_ connection: Connection, _ rechable: Reachable) -> Void ) -> Void {
		monitor.pathUpdateHandler = { path in

			let reachable = (path.status == .unsatisfied || path.status == .requiresConnection)  ? Reachable.no  : Reachable.yes

			if path.availableInterfaces.count == 0 {
				self.rechable = false
				self.status = .other
				return callBack(.other, .no)
				
			} else if path.usesInterfaceType(.wifi) {
				self.rechable = true
				self.status = .wifi
				return callBack(.wifi, reachable)
				
			} else if path.usesInterfaceType(.cellular) {
				self.rechable = true
				self.status = .cellular
				return callBack(.cellular, reachable)
				
			} else if path.usesInterfaceType(.loopback) {
				self.rechable = true
				self.status = .loopback
				return callBack(.loopback, reachable)
				
			} else if path.usesInterfaceType(.wiredEthernet) {
				self.rechable = true
				self.status = .wiredEthernet
				return callBack(.wiredEthernet, reachable)
				
			} else if path.usesInterfaceType(.other) {
				self.rechable = true
				self.status = .other
				return callBack(.other, reachable)
			}
		}
	}
}


extension ReachabliltyManager {
	func cancel() {
		monitor.cancel()
	}
}
