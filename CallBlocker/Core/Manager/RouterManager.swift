//
//  RouterManager.swift
//  CallRecorder
//
//  Created by vincent_kim on 2020/11/09.
//

import Foundation
import UIKit


enum ViewType: Int {
	/**스플래시 화면*/
	case splash
	/**차단목록*/
	case blockList
	/**차단목록 상세*/
	case blockListDetail
	
	/**메인*/
	case main
	/**공통 웹뷰*/
	case commonWebview
	
	/**결제*/
	case inAppPurchase
	
}

final class RouterManager {

	static let shared = RouterManager()
	var window = UIWindow.key//UIApplication.shared.keyWindow
	
	
	private init() {
		
	}

	public func dispatcher(_ type: ViewType, values: [String:Any]? = nil) {
		
		switch type {
			
			case .splash:
				self.moveToSplash()
			case .blockList:
				self.moveToBlockList()
			case .blockListDetail:
				self.moveToBlockNumberDetail(values: values!)
			case .main:
				self.moveToMainTabbar()
			case .commonWebview:
				self.moveToCommonWebview(values: values!)
			case .inAppPurchase:
				self.moveToInAppPurchase()
		}
	}
}

// MARK: move to View Controller
extension RouterManager {
	private func moveToExample(){
		let vc = SplashVC()
//		self.window?.pushVC(vc)
		self.window?.setRootNavVC(vc, options: .init(direction: .fade,
													 style: .linear,
													 duration: .main))
	}
	// splash view
	private func moveToSplash() {
		let vc = SplashVC()
		
		self.window?.rootViewController = vc

	}
	
	private func moveToBlockList(){
		let vc = BlockListVC()
		self.window?.setRootNavVC(vc,setNavHidden: false , options: .init(direction: .fade,
													 style: .linear,
													 duration: .main))
	}
	
	private func moveToMainTabbar(){
		let vc = MainTabBarVC()
		self.window?.setRootNavVC(vc,setNavHidden: true , options: .init(direction: .fade,
													 style: .linear,
													 duration: .main))
//		self.window?.rootViewController = vc
	}
	
	private func moveToCommonWebview(values: [String:Any]){
		let vc = CommonWebViewVC()
		vc.landingUrl = values["landingUrl"] as! String
		vc.vcTitle = values["vcTitle"] as! String
		let navigationController = UINavigationController(rootViewController: vc)
		self.window?.presentVC(navigationController, type: .automatic)
		
	}
	
	
	private func moveToBlockNumberDetail(values: [String:Any]){
		let vc = BlockDetailVC()
		
		vc.vcTitle = values["vcTitle"] as! String
		
		vc.model = values["model"] as? BlockNumberModel
		if let model = vc.model{
			let prefixString = String(repeating: "X", count: model.suffixCount)
			let result = model.localNumber + model.middleNumber + model.lastNumber + prefixString
			vc.expectResult = result
		}
		
		self.window?.pushVC(vc, fromTop: false)
	}
	
	private func moveToInAppPurchase(){
		let vc = PurchaseMainVC()
		let navigationController = UINavigationController(rootViewController: vc)
		self.window?.presentVC(navigationController, type: .automatic)
		
	}
	
}
