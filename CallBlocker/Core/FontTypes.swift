//
//  KE_FontTypes.swift
//  KidsNoteEditor
//
//  Created by vincent_kim on 2020/10/27.
//  Copyright © 2020 dktechin. All rights reserved.
//

import Foundation
import UIKit

//MARK: 폰트 추가하기 1
/// ============================
///
/// 모든 폰트는 enum 에 추가
/// info - title : 버튼에 표시될 제목
/// info - value : 해당 폰트의 고유 이름
///
/// ============================

public enum KE_FontTypes: String{
	/**노토 산스*/
	case fontSans
	/**노토 세리프*/
	case fontSerif
	/**서툰 이야기*/
	case fontPoorStory
	/** 감자 꽃*/
	case fontGamja
	/** 주아*/
	case fontJua
	
	var info: (title: String, value:String){
		switch self {
			case .fontSans:
				return("고딕", "sans")
			case .fontSerif:
				return("명조", "serif")
			case .fontPoorStory:
				return("서툰이야기", "poorstory")
			case .fontGamja:
				return("감자꽃", "gamja")
			case .fontJua:
				return("주아", "jua")
				
		}
	}
}

/// CustomFont
public enum CSFontType: String {
	case GamJaBoldItalic     = "GamjaFlower-BoldItalic"
	case GamJaRegular        = "GamjaFlower-Regular"
	case GamJaBold           = "GamjaFlower-RegularBold"
	case GamJaItalic         = "GamjaFlower-RegularItalic"
	case JuaBoldItalic       = "Jua-BoldItalic"
	case JuaRegular          = "Jua-Regular"
	case JuaBold             = "Jua-RegularBold"
	case JuaItalic           = "Jua-RegularItalic"
	case NotoSansBoldItalic  = "NotoSansKR-BoldItalic"
	case NotoSansRegular     = "NotoSansKR-Regular"
	case NotoSansBold        = "NotoSansKR-Bold"
	case NotoSansItalic      = "NotoSansKR-RegularItalic"
	case NotoSerifBoldItalic = "NotoSerifKR-BoldItalic"
	case NotoSerifRegular    = "NotoSerifKR-Regular"
	case NotoSerifBold       = "NotoSerifKR-Bold"
	case NotoSerifItalic     = "NotoSerifKR-RegularItalic"
	case PoorRegular         = "PoorStory-Regular"
	case PoorBoldItalic      = "PoorStory-BoldItalic"
	case PoorBold            = "PoorStory-RegularBold"
	case PoorItalic          = "PoorStory-RegularItalic"

}

	
//MARK: 폰트 추가하기 2
/// ============================
///
/// 커스텀 폰트의 일반, 볼드, 이탤릭 , 볼드 + 이탤릭 파일 추가 후 해당 family font 를 enum에 추가
///
/// ============================
extension UIFont{
	
	static func CSFont(_ type: CSFontType = .NotoSerifRegular, size: CGFloat = UIFont.systemFontSize) -> UIFont {
		return UIFont(name: "\(type.rawValue)", size: size)!
	}
}
