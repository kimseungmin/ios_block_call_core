//
//  AppDelegate.swift
//  CallBlocker
//
//  Created by vincent_kim on 2021/03/05.
//

import UIKit
import SVProgressHUD
import UserNotifications
import SwiftyStoreKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		
		self.window = UIWindow(frame: UIScreen.main.bounds)
		self.window!.makeKeyAndVisible()
		SystemUtils.shared.setConstraintsLog(false)
		NSSetUncaughtExceptionHandler { exception in
				print("Error Handling:\(exception)")
				print("Error Handling callStackSymbols: \(exception.callStackSymbols)")
		}
		
		self.initLibrary()
		self.initNotification(application: application)
		
		// In App Purchase
		self.initStoreKit()
		
		application.clearLaunchScreenCache()
		
		
		RouterManager.shared.dispatcher(.splash)
		
		do {
		   try FileManager.default.removeItem(atPath: NSHomeDirectory()+"/Library/SplashBoard")
		} catch {
		   print("Failed to delete launch screen cache: \(error)")
		}
		
		return true
	}

	
}


extension AppDelegate{
	private func initLibrary(){
		// initSVProgressHUD
		self.initSVProgressHUD()
		
		// google depend
		self.initGoogle()
	}
}

// MARK: - Google
extension AppDelegate{
	func initGoogle(){
		// Firebase setting
		FirebaseApp.configure()
		
		
		
	}
}

// MARK: - SVProgressHUD
extension AppDelegate{
	func initSVProgressHUD(){
		
		SVProgressHUD.setDefaultStyle(.custom)
		SVProgressHUD.setDefaultMaskType(.black)
		SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
		SVProgressHUD.setForegroundColor(.white)           //Ring Color
		SVProgressHUD.setBackgroundColor(.clear)        //HUD Color
		SVProgressHUD.setBackgroundLayerColor(UIColor.init(hexString: "00000080"))    //Background Color
		
		
	}
	
}

//MARK: - Notification
extension AppDelegate: UNUserNotificationCenterDelegate {
	func initNotification(application: UIApplication){

		let center = UNUserNotificationCenter.current()
		center.delegate = self
		if #available(iOS 10.0, *) {
			// For iOS 10 display notification (sent via APNS)

			center.requestAuthorization(options: [.alert, .sound, .badge]) {
				(granted, error) in
				guard granted else { return }
				DispatchQueue.main.async {
					application.registerForRemoteNotifications()

				}
			}
		}
		/**permission 과 무관하게 token 전송*/
		application.registerForRemoteNotifications()
		
	}
	
	//MARK: Success
	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//        let tokenString = deviceToken.map { String(format: "%02x", $0) }.joined()
		print(debug: "device token: \(tokenString)")
//		APNSManager.shared.setDeviceToken(token: tokenString)
		
		
	}
	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
		print(error)
//        print(debug: error)
	}

	//MARK: recieve message
	func application(_ application: UIApplication,
					 didReceiveRemoteNotification userInfo: [AnyHashable : Any],
					 fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
		
		print("\n"+"didReceiveRemoteNotification = \(userInfo)"+"\n")
		completionHandler(UIBackgroundFetchResult.newData)
	}
	
	//MARK: Foreground Noti event
	func userNotificationCenter(_ center: UNUserNotificationCenter,
								willPresent notification: UNNotification,
								withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		print(debug: "UNUserNotificationCenterDelegate willPresent notification = \(notification.request.content.userInfo)")
		
		
		completionHandler([.alert, .sound])
		
		
	}
	
	//MARK: action
	func userNotificationCenter(_ center: UNUserNotificationCenter,
								didReceive response: UNNotificationResponse,
								withCompletionHandler completionHandler: @escaping () -> Void) {

		
//		APNSManager.shared.userInfo(response.notification.request.content.userInfo)
//		print(debug: APNSManager.shared.phone, self)
//		let phone = APNSManager.shared.phone
//		let name = APNSManager.shared.name
//		let model = UserData(phoneNumber: phone, name: name)
//		SchemeManager.shared.setIsSelectedPhone(user: model)
		
		completionHandler()
		
	}


}

// MARK: - IAP
extension AppDelegate{
	func initStoreKit(){
		
		SwiftyStoreKit.completeTransactions(atomically: true) { purchases in

			for purchase in purchases {
				switch purchase.transaction.transactionState {
				case .purchased, .restored:
					let downloads = purchase.transaction.downloads
					if !downloads.isEmpty {
						SwiftyStoreKit.start(downloads)
					} else if purchase.needsFinishTransaction {
						// Deliver content from server, then:
						
						SwiftyStoreKit.finishTransaction(purchase.transaction)
					}
					print(debug: "\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)", self)
					
					
				case .failed, .purchasing, .deferred:
					break // do nothing
				@unknown default:
					break // do nothing
				}
			}
		}
		
		SwiftyStoreKit.updatedDownloadsHandler = { downloads in

			// contentURL is not nil if downloadState == .finished
			let contentURLs = downloads.compactMap { $0.contentURL }
			if contentURLs.count == downloads.count {
				print(debug: "SwiftyStoreKit.updatedDownloadsHandler Saving: \(contentURLs)", self)
				
				SwiftyStoreKit.finishTransaction(downloads[0].transaction)
			}
		}
		
		
		
	}
}
