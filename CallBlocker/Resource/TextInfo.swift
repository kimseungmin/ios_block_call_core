//
//  TextInfo.swift
//  YouTubeSearch
//
//  Created by vincent_kim on 2020/12/15.
//

import Foundation
import UIKit

////MARK: - Common
struct TextInfo {

}
//
//MARK: - Purchase
extension TextInfo{

//    static let title_service_term         = "Terms Of Use"
    static let purchase_month_btn_status    = "패턴차단 전체 이용권 1 개월"
    static let purchase_month_btn_value     = "500원 / 월 간";
    static let purchase_year_btn_status     = "패턴차단 전체 이용권 1 년";
    static let purchase_year_btn_value      = "5000원 / 연 간";
    static let purchase_restore             = "재 설치하셨나요?\n여기를 눌러 복원하세요";
    static let purchase_info1               = "2달 무료 ";
    static let purchase_info2               = "(약 1000원 절약)";
    static let purchase_info_month          = "첫 2주간은 무료로 사용하세요 !\n";
    static let purchase_main_desc1          = "월 500원으로";
    static let purchase_main_desc2          = "\n070 번호를 차단해보세요";
    static let purchase_sub_desc            = "구독 작동 방식\n• 지불 확인시 iTunes 계정으로 요금이 청구됩니다\n• 자동 갱신이 현재 기간이 종료되기 최소 24 시간 전에 해제되지 않으면 자동으로 갱신됩니다.\n• 계정은 현재 기간이 끝나기 24 시간 이내에 갱신 비용을 청구하고 갱신 비용을 확인합니다.\n• 구독은 사용자가 관리 할 수 ​​있으며 구입 후 사용자의 계정 설정으로 이동하여 자동 갱신을 끌 수 있습니다.\n• 무료 평가판 사용 기간의 미사용 부분은 사용자가 해당 서적에 가입 할 때 해당되는 경우 자격을 상실합니다.";

	/**확인*/
    static let alert_action_confirm         = "확인"
	/**구독 성공*/
    static let purchase_alert_title_success = "구독 성공"
	/**070 차단 기능이 모두 활성화 되었습니다*/
    static let purchase_alert_msg_success   = "패턴 차단기능이 모두 활성화 되었습니다";
	/**구독 실패*/
    static let purchase_alert_title_failed  = "구독 실패";
	/**구독상품 구매에 실패했습니다\n결제정보 및 구매 정보를 확인해주세요*/
    static let purchase_alert_msg_failed    = "구독상품 구매에 실패했습니다\n결제정보 및 구매 정보를 확인해주세요";
	
	
	/**알림*/
    static let alert_title                  = "알림"
	/**취소*/
	static let alert_cancel                 = "취소"

	/**메인 화면 타이틀*/
    static let vc_block_title               = "패턴 차단"
	
	/**더 보기*/
	static let vc_more_title				= "더 보기"
	
	/**번호를 설정해주세요*/
	static let text_need_number				= "번호를 설정해주세요"
}


struct URLInfo{
	
//	static let facebook_page = "https://www.facebook.com/featuredkpop"
	
	static let local_policy_address = "https://smbh.kr/twonum_terms/Privacy.html"
	static let local_terms_of_service_address = "https://smbh.kr/twonum_terms/index.html"
}


struct AppInfo {
	static let appId                = "1556764083"
	static let alert_update_title   = "업데이트 알림";
	static let alert_update_message = "새로운 버전이 출시되었습니다";
	static let alert_ok             = "앱 스토어로 이동하기";
	static let alert_cancel         = "나중에";

}



struct ReportEmailInfo {
	static let alert_send_email_fail = "이 디바이스에서는 메일발송이 불가합니다\n메일 관련 설정을 확인해주세요";
	static let alert_cannotfind_file = "파일을 찾을 수 없습니다"
	static let recipients_address    = "remain-co@naver.com"
	static let recipients_cc_address = ""//"kimtmdals@naver.com"
	static let email_subject         = "[\(Bundle.appName())] Contact Request"
	
}
