//
//  CallDirectoryHandler.swift
//  CallKit-EX
//
//  Created by vincent_kim on 2021/03/05.
//

import Foundation
import CallKit
import UserNotifications

class CallDirectoryHandler: CXCallDirectoryProvider {
	
//	let bundleName:String = CallKitBundles.bundle1.rawValue
    let msg:String   = "기본 070 1번"
    let title:String = "070 번호차단 1번"

	let bundleName:String = "com.remain.CallBlocker.CallKit-EX"
	override func beginRequest(with context: CXCallDirectoryExtensionContext) {
		context.delegate = self

		
		// 적용 확인용
		self.addAllIdentificationPhoneNumbers(to: context)
		self.addAllBlockingPhoneNumbers(to: context)
		context.completeRequest()

	}

	private func addAllBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
//		let userDefaults = UserDefaults(suiteName: CallkitGroupName.groupName)!
		if let userDefaults = UserDefaults(suiteName: CallkitGroupName.groupName){
			let dict = userDefaults.dictionary(forKey: self.bundleName) ?? ["number":1, "suffix":1]
			let baseNumberInt = dict["number"] as! Int
			let suffixInt = dict["suffix"] as! Int
			
			var baseNumber = baseNumberInt
			if baseNumberInt == 1{
				// 초기화 or 최초
				context.addBlockingEntry(withNextSequentialPhoneNumber: CXCallDirectoryPhoneNumber(baseNumber))
//				self.postNotification(msg: self.msg + "의 환경설정이 완료되었습니다")
			}
			else{
				
				var suffixValue = ""
				
				for _ in 1...suffixInt{
					suffixValue.append("9")
				}
				
				if let suffixValueToInt = Int(suffixValue){
					for _ in 1...suffixValueToInt{
						baseNumber += 1
						context.addBlockingEntry(withNextSequentialPhoneNumber: CXCallDirectoryPhoneNumber(baseNumber))
					}
					self.postNotification(msg: self.msg + "의 차단설정이 완료되었습니다\n휴대폰 상태에 따라 차단이 해제될 수 있으며\n차단해제 현상 발생 시 앱 실행 후 재 동기화 부탁드립니다")
					
					
				}
				
			}
			
		}
		else{
			self.postNotification(msg: self.msg + "의 적용이 실패했습니다\n잠시 후 다시 시도 해주세요")
			context.addBlockingEntry(withNextSequentialPhoneNumber: CXCallDirectoryPhoneNumber(1))
			let error = NSError(domain: "CallDirectoryHandler", code: 1, userInfo: nil)
			context.cancelRequest(withError: error)
		}
	}

	private func addOrRemoveIncrementalBlockingPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
		// Retrieve any changes to the set of phone numbers to block from data store. For optimal performance and memory usage when there are many phone numbers,
		// consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
//        let phoneNumbersToAdd: [CXCallDirectoryPhoneNumber] = [ 1_408_555_1234 ]
//        for phoneNumber in phoneNumbersToAdd {
//            context.addBlockingEntry(withNextSequentialPhoneNumber: phoneNumber)
//        }
//
//        let phoneNumbersToRemove: [CXCallDirectoryPhoneNumber] = [ 1_800_555_5555 ]
//        for phoneNumber in phoneNumbersToRemove {
//            context.removeBlockingEntry(withPhoneNumber: phoneNumber)
//        }

		// Record the most-recently loaded set of blocking entries in data store for the next incremental load...
	}

	private func addAllIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
		
		// 최대 1만개까지 되는 듯
		if let userDefaults = UserDefaults(suiteName: CallkitGroupName.groupName){
			let dict = userDefaults.dictionary(forKey: self.bundleName) ?? ["number":1, "suffix":1]
			let baseNumberInt = dict["number"] as? Int ?? 1
			let suffixInt = dict["suffix"] as? Int ?? 6
//			let baseNumberInt = userDefaults.integer(forKey: self.bundleName)
			var baseNumber = baseNumberInt
			if baseNumberInt == 1{
				// 초기화 or 최초
				context.addIdentificationEntry(withNextSequentialPhoneNumber: CXCallDirectoryPhoneNumber(1), label: "")
//				self.postNotification(msg: self.msg + "의 환경설정이 완료되었습니다")
			}
			else{
				var suffixValue = ""
				if suffixInt < 5{
					for _ in 1...suffixInt{
						suffixValue.append("9")
					}
					
					
					if let suffixValueToInt = Int(suffixValue){
						for _ in 1...suffixValueToInt{
							baseNumber += 1
							context.addIdentificationEntry(withNextSequentialPhoneNumber: CXCallDirectoryPhoneNumber(baseNumber), label: "차단된 번호입니다")
						}
	//					self.postNotification(msg: self.msg + "\(baseNumberInt)의 콜킷 설정이 완료되었습니다\nsuffixInt 는 \(suffixInt) 입니다")
					}
				}
				
			}
		}
		else{
			self.postNotification(msg: self.msg + "의 적용이 실패했습니다\n잠시 후 다시 시도 해주세요")
			context.addIdentificationEntry(withNextSequentialPhoneNumber: CXCallDirectoryPhoneNumber(1), label: "1 가 의심 번호입니다")
			let error = NSError(domain: "CallDirectoryHandler", code: 1, userInfo: nil)
			context.cancelRequest(withError: error)
		}

	}

	private func addOrRemoveIncrementalIdentificationPhoneNumbers(to context: CXCallDirectoryExtensionContext) {
		// Retrieve any changes to the set of phone numbers to identify (and their identification labels) from data store. For optimal performance and memory usage when there are many phone numbers,
		// consider only loading a subset of numbers at a given time and using autorelease pool(s) to release objects allocated during each batch of numbers which are loaded.
//        let phoneNumbersToAdd: [CXCallDirectoryPhoneNumber] = [ 1_408_555_5678 ]
//        let labelsToAdd = [ "New local business" ]
//
//        for (phoneNumber, label) in zip(phoneNumbersToAdd, labelsToAdd) {
//            context.addIdentificationEntry(withNextSequentialPhoneNumber: phoneNumber, label: label)
//        }
//
//        let phoneNumbersToRemove: [CXCallDirectoryPhoneNumber] = [ 1_888_555_5555 ]
//
//        for phoneNumber in phoneNumbersToRemove {
//            context.removeIdentificationEntry(withPhoneNumber: phoneNumber)
//        }

		// Record the most-recently loaded set of identification entries in data store for the next incremental load...
	}

}

extension CallDirectoryHandler: CXCallDirectoryExtensionContextDelegate {

	func requestFailed(for extensionContext: CXCallDirectoryExtensionContext, withError error: Error) {
		// An error occurred while adding blocking or identification entries, check the NSError for details.
		// For Call Directory error codes, see the CXErrorCodeCallDirectoryManagerError enum in <CallKit/CXError.h>.
		//
		// This may be used to store the error details in a location accessible by the extension's containing app, so that the
		// app may be notified about errors which occurred while loading data even if the request to load data was initiated by
		// the user in Settings instead of via the app itself.
		self.postNotification(msg: self.msg + "의 적용이 실패했습니다\n잠시 후 다시 시도 해주세요")
		extensionContext.cancelRequest(withError: error)
	}

}
extension CallDirectoryHandler{
	func postNotification(msg:String){
		let userNotificationCenter = UNUserNotificationCenter.current()
		let notificationContent = UNMutableNotificationContent()

		notificationContent.title = self.title
		notificationContent.body = msg
		notificationContent.sound = UNNotificationSound.default
		notificationContent.threadIdentifier = "F39-C521-A7A"

		let userData:[String:String] = ["phoneNumber":"",
										"name":""
										]
		notificationContent.userInfo = ["userInfo": userData]


		let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
		let nowDouble = NSDate().timeIntervalSince1970
		let now = Int(nowDouble*1000)
		let request = UNNotificationRequest(identifier: "\(now)",
											content: notificationContent,
											trigger: trigger)
		userNotificationCenter.removeAllPendingNotificationRequests()
		userNotificationCenter.add(request) { error in
			if let error = error {

				NSLog("Notification Error: \(error)")
			}
			else{
				
				notificationContent.body = "차단 범위가 10,000 건 이하 인 경우 최근 통화내역에서 기존에 왔던 스팸 번호의 차단여부가 표시됩니다.\n단 아이폰 환경에 따라 추 후 차단된 번호는 수신되거나 표시되지 않습니다"
				let secondRequest = UNNotificationRequest(identifier: "\(now+1)",
														  content: notificationContent,
														  trigger: trigger)
				userNotificationCenter.add(secondRequest) { error in
					if let error = error {

						NSLog("Notification Error: \(error)")
					}
				}
			}
		}
		
		
	}
}
